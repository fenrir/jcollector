package org.fenrir.jcollector.ui.worker;

import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.worker.AbstractWorker;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.core.service.IContentSearchService;
import org.fenrir.jcollector.util.CommonUtils;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140530
 */
public class RegenerateThumbnailsWorker extends AbstractWorker<Void, Void> 
{
    private Logger log = LoggerFactory.getLogger(RegenerateThumbnailsWorker.class);
			
    /**
     * Mètode que s'encarregarà de realitzar la tasca d'actualitzar els fitxers del repositori
     * S'executa en un Thread a part.
     */
    @Override	
    protected Void doInBackground() 
    {
        IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
        try{
            // Es neteja el directori de thumbnails			
            CommonUtils.cleanThumbnailsDirectory();
            // Primer, les imatges pendents del repositoris
            List<Repository> repositories = contentSearchService.findAllActiveRepositories();
            for(Repository repository:repositories){
                try{
                    List<Picture> pictures = contentSearchService.findOutstandingPicturesByRepository(repository.getId());
                    for(Picture picture:pictures){
                        try{
                            CommonUtils.createThumbnail(picture);
                        }
                        catch(IOException e){
                            log.error("Error al generar miniatura de la imatge {}: {} ", new Object[]{picture.getRelativePath(), e.getMessage()}, e);
                        }
                    }
                }
                catch(BusinessException e){
                    log.error("Error al processar imatges del repositori {}: {}", new Object[]{repository.getName(), e.getMessage()}, e);
                }
            }
            // Les imatge catalogades definitivament en el seu respectiu grup
            List<PictureGroup> groups = contentSearchService.findAllPictureGroups();
            for(PictureGroup group:groups){
                try{
                    List<Picture> pictures = contentSearchService.findPicturesByGroup(group.getId(), true);
                    for(Picture picture:pictures){
                        try{
                            CommonUtils.createThumbnail(picture);
                        }
                        catch(IOException e){
                            log.error("Error al generar miniatura de la imatge {}: {}", new Object[]{picture.getRelativePath(), e.getMessage()}, e);
                        }
                    }
                }
                catch(BusinessException e){
                    log.error("Error al processar imatges del grup {}: {}", new Object[]{group.getName(), e.getMessage()}, e);
                }
            }
        }
        catch(BusinessException e){
            log.error("Error al regenerar les miniatures de les imatges: ", e.getMessage(), e);
        }
        catch(IOException e){
            log.error("Error al esborrar les miniatures existents: ", e.getMessage(), e);
        }

        return null;
    }			
}
