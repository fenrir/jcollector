package org.fenrir.jcollector.ui.worker;

import javax.swing.SwingWorker;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.event.IWorkerUpdateListener;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.core.service.IContentAdministrationService;
import org.fenrir.jcollector.core.service.IContentSearchService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140128
 */
public class ImportRepositoryContentWorker extends SwingWorker<Void, String>
{
    private final Logger log = LoggerFactory.getLogger(ImportRepositoryContentWorker.class);

    private Repository repository = null;

    private List<IWorkerUpdateListener<String>> vListeners = new ArrayList<IWorkerUpdateListener<String>>();

    public ImportRepositoryContentWorker(Repository repository)
    {
        this.repository = repository;
    }
	
    /**
     * Mètode que s'encarregarà de realitzar la tasca d'actualitzar els fitxers del repositori
     * S'executa en un Thread a part.
     */
    @Override	
    protected Void doInBackground() 
    {	
        IContentAdministrationService contentAdministrationService = (IContentAdministrationService)ApplicationContext.getInstance().getRegisteredComponent(IContentAdministrationService.class);
        IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);

        List<Repository> vRepositories;
        try{
            // Només s'inspeccionará el repositori especificat
            if(repository!=null){
                vRepositories = new ArrayList<Repository>();
                vRepositories.add(repository);
            }
            // Si no s'indica repositori, s'inspeccionen tots
            else{			
                vRepositories = contentSearchService.findAllActiveRepositories();
            }
			
            // S'examinen els repositoris sel.leccionats
            for(Repository elem:vRepositories){
                File folder = new File(elem.getFolderPath());
                if(folder.exists()){
                    /* Fitxers presents a la carpeta */
                    // Es recorre el directori recursivament
                    // TODO v0.2 Posar extensions com a paràmetre o constant
                    @SuppressWarnings(value = {"unchecked"})
                    Collection<File> files = FileUtils.listFiles(folder, new String[]{"jpg", "jpeg", "png", "gif", "bmp"}, true);
                    HashSet<PictureDescriptor> h1 = new HashSet<PictureDescriptor>(); 
                    for(File f:files){
                        String relativePath = StringUtils.substringAfter(f.getAbsolutePath(), folder.getAbsolutePath());
                        if(relativePath.startsWith(File.separator)){
                            relativePath = relativePath.substring(1);
                        }
                        h1.add(new PictureDescriptor(relativePath, repository.getId()));
                    }
					
                    /* Fitxers registrats al repositori */
                    List<Picture> vPictures = contentSearchService.findOutstandingPicturesByRepository(repository.getId()); 						
                    HashSet<PictureDescriptor> h2 = new HashSet<PictureDescriptor>();
                    for(Picture p:vPictures){
                        h2.add(new PictureDescriptor(p.getId(), p.getRelativePath(), repository.getId()));
                    }
					
                    /* Es determinen quins fitxers s'han d'actualitzar a BDD */
                    // Fitxers que s'han d'inserir
                    HashSet<PictureDescriptor> hr1 = new HashSet<PictureDescriptor>();
                    hr1.addAll(h1);
                    hr1.removeAll(h2);				
                    for(PictureDescriptor pd:hr1){
                        String absolutePath = repository.getFolderPath() + File.separator + pd.getPath();
                        try{																				
                            contentAdministrationService.createPicture(pd.getRepository(), new File(absolutePath));

                            publish("Created: " + pd.getPath());
                        }
                        catch(BusinessException e){
                            log.error("Error al crear Picture {}: {}", new Object[]{absolutePath, e.getMessage()}, e);
                            publish("Error: " + e.getMessage());
                        }
                    }
					
                    // Fitxers que s'han d'esborrar
                    HashSet<PictureDescriptor> hr2 = new HashSet<PictureDescriptor>();
                    hr2.addAll(h2);
                    hr2.removeAll(h1);												
                    for(PictureDescriptor pd:hr2){
                        try{
                            contentAdministrationService.deletePicture(pd.getId());

                            publish("Deleted: " + pd.getPath());
                        }
                        catch(BusinessException e){
                            log.error("Error al esborrar picture de la BDD ID={}: {}", new Object[]{pd.getId(), e.getMessage()}, e);
                            publish("Error deleting: " + pd.getPath());
                        }
                    }
                }
            }
        }
        /* En cas de no poder recuperar la llista de repositoris a examinar o hagi fallat algun rollback,
         * s'acaba l'execució
         */
        catch(BusinessException e){
            log.error("Error al importar contingut del repositoris: ", e.getMessage(), e);
        }
		
        return null;
    }
	
    @Override
    protected void process(List<String> chunks) 
    {		
        super.process(chunks);

        for(IWorkerUpdateListener<String> listener:vListeners){
            listener.process(chunks);
        }
    }

    /**
     * Mètode que s'executarà al finalitzar la tasca en el Thread d'event de la UI. Serà l'encarregat
     * d'actualitzar la UI de la finestra de diàleg amb el resultat final.
     */
    @Override
    protected void done() 
    {		
        super.done();

        for(IWorkerUpdateListener<String> listener:vListeners){
            listener.done();
        }
    }
	
    public void addProcessListener(IWorkerUpdateListener<String> listener)
    {
        vListeners.add(listener);
    }

    public void removeProcessListener(IWorkerUpdateListener<String> listener)
    {
        vListeners.remove(listener);
    }

    public void removeAllProcessListeners()
    {
        vListeners.clear();
    }
	
    private class PictureDescriptor
    {
        private Long id;
        private String path;
        private Long repository;

        public PictureDescriptor(String path, Long repository)
        {
            this.path = path;
            this.repository = repository;
        }

        public PictureDescriptor(Long id, String path, Long repository)
        {
            this(path, repository);

            this.id = id;
        }

        public Long getId()
        {
            return id;
        }

        public String getPath() 
        {
            return path;
        }

        public Long getRepository() 
        {
            return repository;
        }

        @Override
        public int hashCode() 
        {
            final int prime = 31;
            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + ((path == null) ? 0 : path.hashCode());
            result = prime * result	+ ((repository == null) ? 0 : repository.hashCode());

            return result;
        }

        @Override
        public boolean equals(Object obj) 
        {
            if(this == obj){
                return true;
            }
            if(obj == null){
                return false;
            }
            if(getClass() != obj.getClass()){
                return false;
            }
            PictureDescriptor other = (PictureDescriptor)obj;
            if(!getOuterType().equals(other.getOuterType())){
                return false;
            }
            if(path == null) {
                if (other.path != null){
                    return false;
                }
            } 
            else if(!path.equals(other.path)){
                return false;
            }
            if(repository == null) {
                if (other.repository != null){
                    return false;
                }
            } 
            else if(!repository.equals(other.repository)){
                return false;
            }

            return true;
        }

        /*
         * Permet accedir a inner classes
         */
        private ImportRepositoryContentWorker getOuterType() 
        {
            return ImportRepositoryContentWorker.this;
        }			
    }
}
