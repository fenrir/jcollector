package org.fenrir.jcollector.ui.module;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import org.fenrir.jcollector.ui.model.ApplicationModel;
import org.fenrir.jcollector.ui.view.TagExplorerView;
import org.fenrir.jcollector.ui.controller.ExplorerController;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140128
 */
public class UIModule extends AbstractModule
{
    @Override
    protected void configure()
    {        
        /* Models */
        bind(ApplicationModel.class).in(Singleton.class);
        /* Controladors */
        bind(ExplorerController.class).in(Singleton.class);
        /* Vistes */        
        bind(TagExplorerView.class).in(Singleton.class);        
    }
}
