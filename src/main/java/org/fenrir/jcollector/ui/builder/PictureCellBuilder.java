package org.fenrir.jcollector.ui.builder;

import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.ui.dto.ICell;
import org.fenrir.jcollector.ui.dto.PictureCell;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20110524
 */
public class PictureCellBuilder 
{
	private Picture picture;
	
	public PictureCellBuilder setPicture(Picture picture)
	{
		this.picture = picture;
		
		return this;
	}
	
	public ICell createCell()
	{
		ICell cell = new PictureCell();
		cell.setElement(picture);
		
		return cell;
	}
}
