package org.fenrir.jcollector.ui.builder;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.ui.dto.ICell;
import org.fenrir.jcollector.ui.dto.PictureGroupCell;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20110322
 */
public class PictureGroupCellBuilder 
{
	private PictureGroup group;
	
	public PictureGroupCellBuilder setGroup(PictureGroup group)
	{
		this.group = group;
		
		return this;
	}
	
	public ICell createCell() throws IOException
	{
		BufferedImage image = ImageIO.read(new File("data/icons/folder_128.png"));				
		ICell cell = new PictureGroupCell();
		cell.setElement(group);
		cell.setImage(image);
		
		return cell;
	}
}
