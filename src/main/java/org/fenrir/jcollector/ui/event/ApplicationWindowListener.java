package org.fenrir.jcollector.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import javax.swing.SwingUtilities;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.action.SwitchWorkspaceAction;
import org.fenrir.yggdrasil.ui.event.IWindowListener;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.service.IWorkspaceAdministrationService;
import org.fenrir.jcollector.Application;
import org.fenrir.jcollector.ui.controller.ExplorerController;
import org.fenrir.jcollector.core.event.IDatabaseEventListener;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140610
 */
public class ApplicationWindowListener implements IWindowListener 
{
    private Application applicationInstance;
	
    public void setApplicationInstance(Application application)
    {
        this.applicationInstance = application;
    }
	
    public void preWindowOpen()
    {

    }
		
    @Override
    public void windowOpened(WindowEvent event) 
    {
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        IWorkspaceAdministrationService workspaceService = (IWorkspaceAdministrationService)applicationContext.getRegisteredComponent(IWorkspaceAdministrationService.class);        
        // Si no hi ha un workspace per defecte a la configuració, es mostra la finestra de dialeg per triar-lo    
        if(!workspaceService.isWorkspaceLoaded()){
            // TODO v0.2 Canviar la manera de cridar les Actions
            ActionEvent actionEvent = new ActionEvent(event.getSource(), event.getID(), null);
            new SwitchWorkspaceAction().actionPerformed(actionEvent); 	
        }
        
        /* Una vegada carregat el workspace es pot mostrar el wizard per importar contingut (si cal)
         * Si el workspace està carregat en aquest moment voldrà dir que la inicialització de l'aplicació
         * ha trobat un workspace per defecte i l'ha carregat abans d'obrir la finestra principal
         */
        if(workspaceService.isWorkspaceLoaded()){     
            // En aquest punt la connexió a base de dades ja estarà establerta
            applicationInstance.performDataUpdates();
            loadViews();
        }
        /* En cas contrari es registra un listener per tal de carregar les vistes
         * quan la base de dades estigui disponible
         */
        else{
            IEventNotificationService eventNotificationService = (IEventNotificationService)applicationContext.getRegisteredComponent(IEventNotificationService.class);
            eventNotificationService.addListener(IDatabaseEventListener.class, new WorkspaceDatabaseEventListener());
        }
    }
	
    @Override
    public void windowActivated(WindowEvent event) 
    {

    }

    @Override
    public void windowClosed(WindowEvent event) 
    {	
        
    }

    @Override
    public void windowClosing(WindowEvent event) 
    {	

    }

    @Override
    public void windowDeactivated(WindowEvent event) 
    {	

    }

    @Override
    public void windowDeiconified(WindowEvent event) 
    {	

    }

    @Override
    public void windowIconified(WindowEvent event) 
    {	

    }

    private void loadViews()
    {
        // Es carreguen totes les vistes
        // L'actualització de la UI s'ha de fer en un Thread a part per no bloquejar l'obertura de la finestra
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
            	ApplicationWindowManager.getInstance().switchToDefaultPerspective();
                ExplorerController explorerController = (ExplorerController)ApplicationContext.getInstance().getRegisteredComponent(ExplorerController.class);
                // TODO Carregar el filtre carregat a la sessió anterior
                explorerController.loadExplorerViews();
            }
        });
    }

    @EventListener(definitions=IDatabaseEventListener.class)
    private class WorkspaceDatabaseEventListener implements IDatabaseEventListener
    {
        @Override
        public void databaseConnectionEstablished() throws ApplicationException
        {
            applicationInstance.performDataUpdates();
            loadViews();            
        }

        @Override
        public void databaseConnectionReleased() throws ApplicationException
        {
            // Res a fer
        }
    }        
}