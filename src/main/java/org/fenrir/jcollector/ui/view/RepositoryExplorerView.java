package org.fenrir.jcollector.ui.view;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.List;
import org.fenrir.jcollector.ui.controller.ExplorerController;
import org.fenrir.jcollector.ui.widget.RepositoryListCellRenderer;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.yggdrasil.ui.mvc.AbstractView;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20110830
 */
@SuppressWarnings("serial")
public class RepositoryExplorerView extends AbstractView<ExplorerController>
{
	public static final String ID = "org.fenrir.jcollector.ui.view.repositoryExplorerView";
	
	private JList repositoryList;
	private boolean fireChangeEvent = true;
	
	@Override
    public String getId()
    {
    	return ID;
    }
	
	@Override
	protected JComponent createViewContents()
	{
		JScrollPane scrollPanel = new JScrollPane();
		scrollPanel.getVerticalScrollBar().setUnitIncrement(10);		
		repositoryList = new JList(new DefaultListModel());
		repositoryList.setCellRenderer(new RepositoryListCellRenderer());
		repositoryList.addListSelectionListener(new ListSelectionListener() 
        {			
			@Override
			public void valueChanged(ListSelectionEvent event) 
			{
				if(fireChangeEvent){
					Repository selectedRepository = (Repository)repositoryList.getSelectedValue();
					controller.setSelectedRepository(selectedRepository);
				}
			}
        });
		scrollPanel.setViewportView(repositoryList);
		
		return scrollPanel;
	}

	public void updateRepositoryList(List<Repository> repositories)
	{		
		fireChangeEvent = false;
		
		((DefaultListModel)repositoryList.getModel()).removeAllElements();
		for(Repository tag:repositories){
			((DefaultListModel)repositoryList.getModel()).addElement(tag);
		}
		
		fireChangeEvent = true;
	}
	
	public void clearSelection()
	{
		fireChangeEvent = false;
		repositoryList.clearSelection();
		fireChangeEvent = true;
	}
}
