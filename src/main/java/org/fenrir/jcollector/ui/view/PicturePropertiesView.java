package org.fenrir.jcollector.ui.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.fenrir.jcollector.ui.controller.SelectedPictureController;
import org.fenrir.jcollector.ui.widget.PictureGroupSearchComboBoxModel;
import org.fenrir.jcollector.ui.widget.PictureGroupSearchField;
import org.fenrir.jcollector.ui.widget.RepositorySearchField;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.yggdrasil.ui.mvc.AbstractView;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.2.20120811
 */
@SuppressWarnings("serial")
public class PicturePropertiesView extends AbstractView<SelectedPictureController>
{
	public static final String ID = "org.fenrir.jcollector.ui.view.picturePropertiesView";
	
	private RepositorySearchField inputRepository;
	private JComboBox comboCollections;
	private PictureGroupSearchField inputGroup;
	private JTextField inputInclusionDate;
	private JCheckBox checkCollected;
	private JButton bSavePicture;

	public PicturePropertiesView()
	{
		super();

		setFieldsEnabled(false);
	}

	@Override
    public String getId()
    {
    	return ID;
    }
	
	@Override
	protected final JComponent createViewContents()
	{
		JPanel pContents = new JPanel();

		JLabel lRepository = new JLabel("Repositori");
		inputRepository = new RepositorySearchField();
		JLabel lCollection = new JLabel("Col.lecció");
		DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();
		comboCollections = new JComboBox(comboBoxModel);
		JLabel lGroup = new JLabel("Grup");
		PictureGroupSearchComboBoxModel groupSearchModel = new PictureGroupSearchComboBoxModel(comboBoxModel);
		comboCollections.addActionListener(groupSearchModel);
		inputGroup = new PictureGroupSearchField(groupSearchModel);
		JLabel lInclusionDate = new JLabel("Data inclusió");
		inputInclusionDate = new JTextField();
		checkCollected = new JCheckBox("Definitiu");
		checkCollected.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				if(checkCollected.isSelected() && StringUtils.isBlank(inputInclusionDate.getText())){
					String strTodayDate = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
					inputInclusionDate.setText(strTodayDate);
				}
			}
		});
		bSavePicture = new JButton("Guardar");
		bSavePicture.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				Collection selectedCollection = (Collection)((DefaultComboBoxModel)comboCollections.getModel()).getSelectedItem();
				boolean isCollected = checkCollected.isSelected();
				String collectionDate = inputInclusionDate.getText();
				controller.savePicture(inputRepository.getSelectedRepository(), selectedCollection, inputGroup.getSelectedGroup(), isCollected, collectionDate);
			}
		});

		GroupLayout layout = new GroupLayout(pContents);
		/* Grup horitzontal */
		pContents.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                	.addComponent(lRepository, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                	.addComponent(lCollection, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                	.addComponent(lGroup, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                	.addComponent(lInclusionDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                	.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                		.addComponent(inputRepository, 0, 172, Short.MAX_VALUE)
                		.addComponent(inputGroup, 0, 172, Short.MAX_VALUE)
	                	.addComponent(comboCollections, 0, 172, Short.MAX_VALUE)
	                	.addComponent(inputInclusionDate, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
	                	.addComponent(checkCollected)
	                )
	                .addComponent(bSavePicture, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)
                )
            )
        );

        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                    .addComponent(lRepository)
                    .addComponent(inputRepository, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lCollection)
                    .addComponent(comboCollections, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                    .addComponent(lGroup)
                    .addComponent(inputGroup, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lInclusionDate)
                    .addComponent(inputInclusionDate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(checkCollected)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 73, Short.MAX_VALUE)
                .addComponent(bSavePicture)
                .addContainerGap()
            )
        );

        return pContents;
	}

	public void setFieldsEnabled(boolean enabled)
	{
		if(!enabled){
			inputRepository.setText(null);
			inputRepository.setEnabled(false);
			((DefaultComboBoxModel)comboCollections.getModel()).removeAllElements();
			comboCollections.setEnabled(false);
			inputGroup.setText(null);
			inputGroup.setEnabled(false);
			inputInclusionDate.setText(null);
			inputInclusionDate.setEnabled(false);
			checkCollected.setSelected(false);
			checkCollected.setEnabled(false);
			bSavePicture.setEnabled(false);
		}
		else{
			inputRepository.setEnabled(true);
			comboCollections.setEnabled(true);
			inputGroup.setEnabled(true);
			inputInclusionDate.setEnabled(true);
			checkCollected.setEnabled(true);
			bSavePicture.setEnabled(true);
		}
	}

	public void updateFields(Repository repository,
			Collection collection,
			PictureGroup group,
			Picture picture) throws BusinessException
	{
		// Es carrega el repositori de la imatge a partir de l'objecte picture rebut
		inputRepository.setSelectedRepository(repository);
		DefaultComboBoxModel comboModel = (DefaultComboBoxModel)comboCollections.getModel();
		// Es neteja el combo per refrescar els continguts
		comboModel.removeAllElements();
		// Opció en blanc
		comboModel.addElement(null);
		// Col.leccions
		List<Collection> vCollections = controller.getAllCollections();
		Collection selectedCollection = null;
		for(Collection elem:vCollections){
			comboModel.addElement(elem);
			if(elem.getId().equals(picture.getCollection())){
				selectedCollection = elem;
			}
		}
		comboModel.setSelectedItem(selectedCollection);
		// Es carrega el group de la imatge a partir de l'objecte picture rebut
		if(picture.getPictureGroup()!=null){
			inputGroup.setSelectedGroup(group);
		}
		else{
			inputGroup.setSelectedGroup(null);
		}
		// Es carrega la data de catalogació
		if(picture.getCollectionDate()!=null){
			String strCollectionDate = new SimpleDateFormat("dd/MM/yyyy").format(picture.getCollectionDate());
			inputInclusionDate.setText(strCollectionDate);
		}
		else{
			inputInclusionDate.setText(null);
		}
		// S'indica si ha estat catalogat definitivament
		checkCollected.setSelected(picture.isCollected());
	}

	public void updatePictureGroupFields(Collection collection, PictureGroup group)
	{
		((DefaultComboBoxModel)comboCollections.getModel()).setSelectedItem(collection);
		inputGroup.setSelectedGroup(group);
	}
}
