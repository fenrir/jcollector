package org.fenrir.jcollector.ui.view;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fenrir.jcollector.ui.controller.ExplorerController;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.yggdrasil.ui.mvc.AbstractView;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20120428
 */
@SuppressWarnings("serial")
public class CollectionExplorerView extends AbstractView<ExplorerController>
{
	public static final String ID = "org.fenrir.jcollector.ui.view.collectionExplorerView";
	
	private JComboBox comboCollections;
	private JList groupList;
	private boolean fireCollectionChangeEvent = true;
	private boolean fireGroupChangeEvent = true;

	@Override
    public String getId()
    {
    	return ID;
    }
	
	@Override
	protected JComponent createViewContents()
	{
		JPanel viewPanel = new JPanel();
		viewPanel.setLayout(new BorderLayout());
		/* Sel.lecció de col.lecció */
		DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();
		comboCollections = new JComboBox(comboBoxModel);
		comboCollections.addItemListener(new ItemListener()
		{
			/**
			 * Mètode cridat en sel.leccionar o canviar la sel.lecció de la combo box de les col.leccions.
			 * En el cas que inicialment hi hagués jauna col.lecció sel.leccionada, es cridarà 2 cops
			 * 1 per la desel.lecció de l'item inicial i 1 altre per la sel.lecció del nou
			 * @param event ItemEvent - Event generat per la sel.lecció / desel.lecció d'una col.lecció del combo
			 */
			@Override
			public void itemStateChanged(ItemEvent event)
			{
				// Sel.lecció d'una nova col.lecció
				if(event.getStateChange()==ItemEvent.SELECTED && fireCollectionChangeEvent){
					Collection selectedCollection = (Collection)event.getItem();
					controller.setSelectedCollection(selectedCollection);
				}
			}
		});
		viewPanel.add(comboCollections, BorderLayout.NORTH);
		/* Grups principals de la col.lecció sel.leccionada */
		JScrollPane scrollGroups = new JScrollPane();
		scrollGroups.getVerticalScrollBar().setUnitIncrement(10);
		groupList = new JList(new DefaultListModel());
		groupList.addListSelectionListener(new ListSelectionListener()
        {
			@Override
			public void valueChanged(ListSelectionEvent event)
			{
				if(fireGroupChangeEvent){
					PictureGroup selectedGroup = (PictureGroup)groupList.getSelectedValue();
					controller.setSelectedGroup(selectedGroup);
				}
			}
        });
		scrollGroups.setViewportView(groupList);
		viewPanel.add(scrollGroups, BorderLayout.CENTER);

		return viewPanel;
	}

	public void updateCollections(List<Collection> collections)
	{
		fireCollectionChangeEvent = false;

		DefaultComboBoxModel comboModel = (DefaultComboBoxModel)comboCollections.getModel();
		Collection selectedCollection = (Collection)comboModel.getSelectedItem();
		comboModel.removeAllElements();
		for(Collection collection:collections){
			comboModel.addElement(collection);
		}

		fireCollectionChangeEvent = true;

		if(collections.size()>0){
			int index = comboModel.getIndexOf(selectedCollection);
			if(index>=0){
				comboCollections.setSelectedIndex(index);
			}
			else{
				comboCollections.setSelectedIndex(0);
				controller.setSelectedCollection((Collection)comboModel.getSelectedItem());
			}
		}
	}

	/**
	 * @param collection Collection -
	 * @param groups List<Picturegroup> -
	 */
	public void updateCollectionGroups(Collection collection, List<PictureGroup> groups)
	{
		fireGroupChangeEvent = false;
		fireCollectionChangeEvent = false;

		Collection selectedCollection = (Collection)comboCollections.getSelectedItem();
		if(selectedCollection!=null && selectedCollection.equals(collection)){
			DefaultListModel listModel = (DefaultListModel)groupList.getModel();
			listModel.removeAllElements();
			for(PictureGroup group:groups){
				listModel.addElement(group);
			}
		}

		fireCollectionChangeEvent = true;
		fireGroupChangeEvent = true;
	}

	public void clearSelection()
	{
		fireGroupChangeEvent = false;
		groupList.clearSelection();
		fireGroupChangeEvent = true;
	}
}
