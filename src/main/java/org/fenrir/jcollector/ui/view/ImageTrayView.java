package org.fenrir.jcollector.ui.view;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.fenrir.jcollector.ui.controller.ImageTrayController;
import org.fenrir.jcollector.ui.dto.TrayElement;
import org.fenrir.jcollector.util.CommonUtils;
import org.fenrir.yggdrasil.ui.mvc.AbstractView;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20120514
 */
@SuppressWarnings("serial")
public class ImageTrayView extends AbstractView<ImageTrayController> 
{
	public static final String ID = "org.fenrir.jcollector.ui.view.imageTrayView";
	
	private JPanel pThumbs;	
	private List<TrayPlaceHolder> placeHolders = new ArrayList<TrayPlaceHolder>();
	
	@Override
    public String getId()
    {
    	return ID;
    }
	
	@Override
	protected JComponent createViewContents()
	{
		pThumbs = new JPanel();
        pThumbs.setBackground(new Color(234, 231, 226));
        pThumbs.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 5));                                
        JScrollPane scrollPane = new JScrollPane(pThumbs);		
            
        return scrollPane;
	}
		
	public void clearTray()
	{
		pThumbs.removeAll();
		pThumbs.repaint();
	}
	
	public void updateTray(List<TrayElement> elements) throws IOException
	{
		// S'han de treure tots els elements del panell per tal que no hi hagi problemes al repintar-lo, ja que es quedaven els elements antics
		pThumbs.removeAll();
		int index = 0;
		for(TrayElement elem:elements){
			TrayPlaceHolder placeHolder = null;
			if(index<placeHolders.size()){
				placeHolder = placeHolders.get(index);
				placeHolder.setTrayElement(elem);
				placeHolder.setPinned(elem.isPinned());
			}
			else{
				placeHolder = new TrayPlaceHolder(elem, elem.isPinned());
				placeHolders.add(placeHolder);				
			}
			pThumbs.add(placeHolder);
			index++;
		}
		// S'eliminen els placeholders que no es necessiten per evitar creuament de dades
		for(int i=index; i<placeHolders.size(); i++){
			placeHolders.remove(i);
		}

		pThumbs.repaint();
	}
	
	private class TrayPlaceHolder extends JPanel implements ActionListener
	{
		private TrayElement element;				
		private JLabel label;		
		private JToggleButton togglePinned;
		
		private boolean fireChangeEvent = true;
		
		public TrayPlaceHolder(TrayElement element, boolean pinned) throws FileNotFoundException, IOException
		{
			this.element = element;			
			
			this.createContents();
			
			BufferedImage image = CommonUtils.loadThumbnail(element.getPicture());
			element.setImage(image);
			label.setIcon(new ImageIcon(image));
			String pictureName = CommonUtils.getPictureBasename(element.getPicture());
			label.setToolTipText(pictureName);
			setPinned(pinned);
		}				
		
		private void createContents()
		{
			label = new JLabel();
			label.addMouseListener(new MouseAdapter() 
			{
				@Override
				public void mouseClicked(MouseEvent event) 
				{
					controller.setSelectedPicture(element.getPicture());
				}				
			});
			togglePinned = new JToggleButton(new ImageIcon("data/icons/locked_16.png"));
			togglePinned.addActionListener(this); 			
			
			GroupLayout layout = new GroupLayout(this);						
	        this.setLayout(layout);
	        /* Grup horitzontal */
	        layout.setHorizontalGroup(
	            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addComponent(label, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
	                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(togglePinned, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
	                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	            )
	        );
	        /* Grup vertical */
	        layout.setVerticalGroup(
	            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	            .addComponent(label, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
	            .addComponent(togglePinned)
	        );
		}
		
		public void setTrayElement(TrayElement element) throws FileNotFoundException, IOException
		{
			this.element = element;
			
			if(element.getImage()==null){
				BufferedImage image = CommonUtils.loadThumbnail(element.getPicture());
				element.setImage(image);
			}
			label.setIcon(new ImageIcon(element.getImage()));
			String pictureName = CommonUtils.getPictureBasename(element.getPicture());
			label.setToolTipText(pictureName);
		}
		
		public void setPinned(boolean pinned)
		{
			fireChangeEvent = false;
			togglePinned.setSelected(pinned);
			fireChangeEvent = true;
		}
					
		@Override
		public void actionPerformed(ActionEvent event) 
		{
			if(fireChangeEvent){
				JToggleButton source = (JToggleButton)event.getSource();			
				controller.setPicturePinned(this.element.getPicture(), source.isSelected());	
			}
		}						
	}
}
