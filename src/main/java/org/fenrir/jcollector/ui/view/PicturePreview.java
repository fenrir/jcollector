package org.fenrir.jcollector.ui.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.ui.controller.SelectedPictureController;
import org.fenrir.jcollector.util.CommonUtils;
import org.fenrir.yggdrasil.ui.mvc.AbstractView;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20120108
 */
@SuppressWarnings("serial")
public class PicturePreview extends AbstractView<SelectedPictureController> 
{
	public static final String ID = "org.fenrir.jcollector.ui.view.picturePreview";
	
	private JLabel lPicturePreview;
	private JLabel lPictureName;
	private JLabel lPictureResolution;
	
	public PicturePreview()
	{
		super();
		createViewToolBar(TOOLBAR_LOCATION_EAST);
	}
	
	@Override
    public String getId()
    {
    	return ID;
    }
	
	@Override
	protected final JComponent createViewContents()
	{
		JPanel pContents = new JPanel();
		pContents.setLayout(new BoxLayout(pContents, BoxLayout.Y_AXIS));				
				
		// Marge superior
		pContents.add(Box.createRigidArea(new Dimension(180,5)));
		// Previsualització de la imatge sel.leccionada
		lPicturePreview = new JLabel(null, null, JLabel.CENTER);		
		lPicturePreview.setMinimumSize(new Dimension(160, 160));
		lPicturePreview.setPreferredSize(new Dimension(160, 160));
		lPicturePreview.setMaximumSize(new Dimension(160, 160));
		lPicturePreview.setAlignmentX(JPanel.CENTER_ALIGNMENT);
		lPicturePreview.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		pContents.add(lPicturePreview);
		// Nom de la imatge
		lPictureName = new JLabel();					
		lPictureName.setAlignmentX(JPanel.CENTER_ALIGNMENT);
		lPictureName.setPreferredSize(new Dimension(160, 20));
		pContents.add(lPictureName);
		// Resolució de la imatge
		lPictureResolution = new JLabel();
		lPictureResolution.setAlignmentX(JPanel.CENTER_ALIGNMENT);
		lPictureResolution.setPreferredSize(new Dimension(160, 20));
		pContents.add(lPictureResolution);
		// Marge inferior
		pContents.add(Box.createVerticalGlue());
		
		return pContents;
	}
	
	public void updatePicturePreview(BufferedImage image)
	{
		lPicturePreview.setIcon(new ImageIcon(image));
	}
	
	public void updatePictureFields(Picture picture)
	{
		String pictureName = CommonUtils.getPictureFilename(picture);
		lPictureName.setText(pictureName);
		lPictureName.setToolTipText(pictureName);
		lPictureResolution.setText(picture.getWidth() + " x " + picture.getHeight() + " px");
	}
	
	public void clearFields()
	{
		lPicturePreview.setIcon(null);
		lPictureName.setText(null);
		lPictureName.setToolTipText(null);
		lPictureResolution.setText(null);		
	}
}
