package org.fenrir.jcollector.ui.view;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.List;
import org.fenrir.yggdrasil.ui.mvc.AbstractView;
import org.fenrir.jcollector.ui.widget.TagListCellRenderer;
import org.fenrir.jcollector.ui.controller.ExplorerController;
import org.fenrir.jcollector.core.entity.Tag;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140522
 */
@SuppressWarnings("serial")
public class TagExplorerView extends AbstractView<ExplorerController> 
{
	public static final String ID = "org.fenrir.jcollector.ui.view.tagExplorerView";
	
	private JList listTags;
	private boolean fireChangeEvent = true;
	
	@Override
    public String getId()
    {
    	return ID;
    }
	
	@Override
	protected JComponent createViewContents()
	{
		JScrollPane scrollTags = new JScrollPane();
		scrollTags.getVerticalScrollBar().setUnitIncrement(10);		
		listTags = new JList(new DefaultListModel());
		listTags.setCellRenderer(new TagListCellRenderer());
		listTags.addListSelectionListener(new ListSelectionListener() 
        {			
			@Override
			public void valueChanged(ListSelectionEvent event) 
			{
				if(fireChangeEvent){
					Tag selectedTag = (Tag)listTags.getSelectedValue();
					controller.setSelectedTag(selectedTag);
				}
			}
        });
		scrollTags.setViewportView(listTags);
		
		return scrollTags;
	}

	public void updateTagList(List<Tag> tags)
	{
		fireChangeEvent = false;		
		((DefaultListModel)listTags.getModel()).removeAllElements();
		for(Tag tag:tags){
			((DefaultListModel)listTags.getModel()).addElement(tag);
		}
		fireChangeEvent = true;
	}		
	
	public void clearSelection()
	{
		fireChangeEvent = false;
		listTags.clearSelection();
		fireChangeEvent = true;
	}
}
