package org.fenrir.jcollector.ui.view;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumn;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.fenrir.jcollector.core.entity.Tag;
import org.fenrir.jcollector.ui.controller.SelectedPictureController;
import org.fenrir.jcollector.ui.widget.PictureTagTableModel;
import org.fenrir.yggdrasil.ui.mvc.AbstractView;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20111022
 */
@SuppressWarnings("serial")
public class PictureTagsView extends AbstractView<SelectedPictureController> 
{
	public static final String ID = "org.fenrir.jcollector.ui.view.pictureTagsView";
	
	private JTextField inputTag;
	private JToggleButton togglePreferredTags;
	private JTable tableTags; 
	
	public PictureTagsView()
	{
		super();
		
		setFieldsEnabled(false);
	}
	
	@Override
    public String getId()
    {
    	return ID;
    }
	
	@Override
	protected JComponent createViewContents()
	{
		JPanel pTags = new JPanel();
		
		GroupLayout layout = new GroupLayout(pTags);
        pTags.setLayout(layout);
		
        inputTag = new JTextField();
        inputTag.addKeyListener(new KeyAdapter() 
        {
			@Override
			public void keyReleased(KeyEvent event) 
			{
				int keyPressed = event.getKeyCode();
				String tagName = inputTag.getText();
				if(StringUtils.isNotBlank(tagName)){
					/* Si la tecla premuda es ENTER:
					 * 1) Es crea el tag si el nombre si no hi ha cap coincidència exacte (Case insensitive)
					 * 2) S'associa si hi ha una coincidència exacte (Case insensitive)
					 */
					if(keyPressed==KeyEvent.VK_ENTER){														
						if(StringUtils.isNotBlank(tagName)){																
							controller.tagPicture(tagName);																
							
							// S'esborra el contingut de l'input
							inputTag.setText(null);							
						}
					}
					/* Si la tecla premuda es una altra, es realitza la cerca 
					 * depenent de l'estat de l'indicador de favorits
					 */
					else if(togglePreferredTags.isSelected()){
						controller.searchPreferredTags(tagName);
					}
					else{
						controller.searchTags(tagName);											
					}
				}
				/* Si el filtre es buit però l'indicador de favorits està activat, 
				 * es retornen tots el favorits
				 */
				else if(togglePreferredTags.isSelected()){
					controller.searchPreferredTags();
				}
			}        	
		});
        togglePreferredTags = new JToggleButton(new AbstractAction(null, new ImageIcon("data/icons/favourite_16.png")) 
        {			
			@Override
			public void actionPerformed(ActionEvent event) 
			{
				PictureTagTableModel tableModel = (PictureTagTableModel)tableTags.getModel();
				tableModel.removeAllUncheckedTags();
				
				if(togglePreferredTags.isSelected()){
					tableModel.removeAllUncheckedTags();
					controller.searchPreferredTags(inputTag.getText());
				}				
			}
		});
        
        tableTags = new JTable(new PictureTagTableModel())
        {
        	/**
        	 * Implementació dels tooltips de les descripcions de la taula de tags associats a la imatge
        	 */
            public String getToolTipText(MouseEvent event) 
            {
            	String tip = null;
                Point p = event.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                int realColumnIndex = convertColumnIndexToModel(colIndex);

                /* Columna descripció. S'ha de comprobar que a fila estigui dins del rang 0 - getColumns-1.
                 * En cas contrari rowIndex = -1 
                 */
                if(realColumnIndex==1 && rowIndex!=-1){
                    tip = (String)getValueAt(rowIndex, colIndex);

                } 
                /* La resta de columnes. Aquesy pas es podria ometre ja que no hi ha mes renderers
                 * que proporcionin tooltips per aquesta taula, però d'aquesta manera és més general
                 */
                else{                     
                    tip = super.getToolTipText(event);
                }
                return tip;
            }
        };
        tableTags.setShowGrid(false);
        // Es redimensiona la columna del check        
        tableTags.setFillsViewportHeight(true);
        TableColumn col = tableTags.getColumnModel().getColumn(0);        
        col.setMaxWidth(20);
        // Al marcar el check s'associarà el tag a la imatge
        tableTags.getModel().addTableModelListener(new TableModelListener() 
        {			
			@Override
			public void tableChanged(TableModelEvent event) 
			{
				if(event.getType()==TableModelEvent.UPDATE && event.getColumn()==0){
					int row = event.getFirstRow();
					Tag tag = ((PictureTagTableModel)tableTags.getModel()).getTagAt(row);
					boolean checked = ((PictureTagTableModel)tableTags.getModel()).isTagChecked(row); 						
					// Si el check està marcat s'associa el tag a la imatge
					if(checked){
						controller.tagPicture(tag);														
					}
					// en cas contrari s'elimina l'associació
					else{
						controller.deletePictureTag(tag);														
					}					
				}				
			}
		});
        JScrollPane scrollTags = new JScrollPane(tableTags);        
        
		/* Grup horitzontal */
        layout.setHorizontalGroup(
        	layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(inputTag, GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(togglePreferredTags, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                    )
                    .addComponent(scrollTags, GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
        	layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                    .addComponent(inputTag, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(togglePreferredTags)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollTags, GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                .addContainerGap()
            )
        );
        
        return pTags;
	}
	
	/**
	 * Mètode de refresc de la llista de tags de la imatge
	 * Afegeix un nou tag associat a la imatge (checked)
	 * @param tag Tag - Nou tag associat a la imatge 
	 */
	public void addPictureTag(Tag tag)
	{									
		((PictureTagTableModel)tableTags.getModel()).addTag(tag, true);
	}
	
	/**
	 * Inicialitza les dades de la vista amb els tags passats per paràmetre.
	 * A més, si el sel.lector de favorits està sel.leccionat, els mostra 
	 * @param tags List<Tag> - Tags de la imatge sel.leccionada
	 */
	public void setPictureTags(List<Tag> tags)
	{
		// Es neteja el filtre cada cop que es sel.lecciona una imatge nova
		inputTag.setText(null);
		
		PictureTagTableModel tableModel = (PictureTagTableModel)tableTags.getModel();
		tableModel.removeAllTags();
		tableModel.addTags(tags, true);
		
		// S'afegiran els tags favorits als propis de la imatge si el botó està marcat
		if(togglePreferredTags.isSelected()){
			controller.searchPreferredTags();
		}
	}
	
	public void addTagSuggestions(List<Tag> tags)
	{
		PictureTagTableModel tableModel = (PictureTagTableModel)tableTags.getModel();
		tableModel.removeAllUncheckedTags();
		tableModel.addTags(tags, false);
	}	
	
	public void setFieldsEnabled(boolean enabled)
	{
		if(enabled){
			inputTag.setEnabled(true);
			togglePreferredTags.setEnabled(true);
		}
		else{
			inputTag.setText(null);
			inputTag.setEnabled(false);
			togglePreferredTags.setEnabled(false);
			
			PictureTagTableModel tableModel = (PictureTagTableModel)tableTags.getModel();
			tableModel.removeAllTags();
		}
	}
}
