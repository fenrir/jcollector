package org.fenrir.jcollector.ui.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.LayoutStyle;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.MouseInputAdapter;
import java.util.Collections;
import java.util.List;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.ui.controller.ThumbsViewerController;
import org.fenrir.jcollector.ui.dto.ICell;
import org.fenrir.jcollector.ui.widget.Canvas;
import org.fenrir.yggdrasil.ui.mvc.AbstractView;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20120514
 */
@SuppressWarnings("serial")
public class ThumbnailsView extends AbstractView<ThumbsViewerController>
{
	public static final String ID = "org.fenrir.jcollector.ui.view.thumbnailsView";
	
	private Canvas canvas;
	private JScrollBar scrollBar;
	private JLabel lTitleIcon;
	private JLabel lTitleName;
	private JPanel pBreadcrumb;		
	
	private boolean fireScrollbarStateChange = true;	
		
	@Override
    public String getId()
    {
    	return ID;
    }
	
	@Override
	protected final JComponent createViewContents()
	{
		JPanel pContents = new JPanel();
		pContents.setLayout(new BorderLayout(0, 5));
		
		/* Títol */
		JComponent pTitle = createTitlePanel();		
		pContents.add(pTitle, BorderLayout.NORTH);
		/* Canvas */		
		canvas = new Canvas();
		canvas.addMouseWheelListener(new MouseInputAdapter() 
		{
			@Override
			public void mouseWheelMoved(MouseWheelEvent event) 
			{
				// Amunt
				int amount = event.getWheelRotation(); 
				int index = canvas.getFirstRow();
				int totalRows = canvas.getTotalRows();
				int visibleRows = canvas.getVisibleRows();					
				if(amount<0 && index+amount>=0){
					updateScroll(index + amount);					
				}
				// Avall
				else if(amount>0){
					if(index+amount+visibleRows<=totalRows){
						updateScroll(index + amount);
					}						
				}																																	
			}						
		});
		
		canvas.addMouseMotionListener(new MouseInputAdapter() 
		{
			@Override
			public void mouseExited(MouseEvent event)
			{
				canvas.setToolTipText(null);
			}			
			
			@Override
			public void mouseMoved(MouseEvent event) 
			{								
				ICell selectedCell = canvas.getSelectedCell(event.getX(), event.getY());				
				if(selectedCell!=null){					
					canvas.setToolTipText(selectedCell.getText());
				}
				else{
					canvas.setToolTipText(null);
				}
			}
		});
		
		canvas.addMouseListener(new MouseInputAdapter()
		{			 			
			@Override
			public void mouseClicked(MouseEvent event) 
			{
				ICell selectedCell = canvas.getSelectedCell(event.getX(), event.getY());
				// Si selectedCell==null, es deseleccionarà la imatge actual
				if(selectedCell!=null && !selectedCell.isContainerCell()){
					controller.setSelectedPicture((Picture)selectedCell.getElement());					
				}
				// Si doble click en un subgrup, es carrega la vista amb les seves miniatures
				else{
					controller.setSelectedPicture(null);					
					if(selectedCell!=null && selectedCell.isContainerCell() && event.getClickCount()>1){
						controller.setSelectedGroup((PictureGroup)selectedCell.getElement());						
					}
				}
			}											
		});				
		pContents.add(canvas, BorderLayout.CENTER);
		/* Scrollbar */
		scrollBar = new JScrollBar();
		scrollBar.getModel().addChangeListener(new ChangeListener()
		{				
			@Override
 			public void stateChanged(ChangeEvent event) 
 			{					
				if(fireScrollbarStateChange){
					updateScroll(scrollBar.getValue());
				}
 			}
		});	
		pContents.add(scrollBar, BorderLayout.EAST);
		
		return pContents;
	}
	
	private JComponent createTitlePanel()
	{
		JPanel pTitle = new JPanel();
		pTitle.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(200, 200, 200)));
		lTitleIcon = new JLabel();
		lTitleName = new JLabel();
		pBreadcrumb = new JPanel();
		pBreadcrumb.setLayout(new FlowLayout(FlowLayout.LEFT, 3, 0)); 
		
		GroupLayout layout = new GroupLayout(pTitle);
        pTitle.setLayout(layout);
        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lTitleIcon, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(pBreadcrumb, GroupLayout.DEFAULT_SIZE, 482, Short.MAX_VALUE)
                    .addComponent(lTitleName, GroupLayout.DEFAULT_SIZE, 482, Short.MAX_VALUE)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lTitleName)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pBreadcrumb)
                    )
                    .addComponent(lTitleIcon, GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                )
                .addContainerGap()
            )
        );
        
        return pTitle;
	}		
	
	private void updateScroll()
	{
		updateScroll(canvas.getFirstRow());		
	}
	
	private void updateScroll(int row)
	{
		fireScrollbarStateChange = false;
		scrollBar.setValue(row);
		scrollBar.setMaximum(canvas.getTotalRows());		
		scrollBar.setVisibleAmount(canvas.getVisibleRows());
		fireScrollbarStateChange = true;				
		
		canvas.updateContents(row);						
	}
	
	@SuppressWarnings("unchecked")
	public void updateView(String title, Icon icon, List<ICell> cells, boolean resetPosition)
	{	
		updateView(title, icon, Collections.EMPTY_LIST, cells, resetPosition);
	}
	
	public void updateView(String title, Icon icon, List<Object[]> ancestors, List<ICell> cells, boolean resetPosition)
	{
		// Es configura la breadcrumb
		pBreadcrumb.removeAll();
		for(Object[] ancestor:ancestors){
			final Long id = (Long)ancestor[0];
			String name = (String)ancestor[1];
			JButton button = new JButton();
			button.setAction(new AbstractAction(name) 
			{				
				@Override
				public void actionPerformed(ActionEvent event) 
				{
					controller.visualizeAncestor(id);
				}
			});
			pBreadcrumb.add(button);
		}		
		pBreadcrumb.repaint();
		
		lTitleName.setText(title);
		lTitleIcon.setIcon(icon);
		
		canvas.clearCells();
		canvas.addCells(cells);
				
		if(resetPosition){
			updateScroll(0);
		}
		else{
			updateScroll();
		}				
	}
	
	public void setViewFetchLimit(int fetchLimit)
	{
		canvas.setFetchLimit(fetchLimit);
	}
}
