package org.fenrir.jcollector.ui.controller;

import javax.swing.JOptionPane;
import java.beans.PropertyChangeEvent;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.mvc.AbstractController;
import org.fenrir.jcollector.ui.model.ApplicationModel;
import org.fenrir.jcollector.ui.view.CollectionExplorerView;
import org.fenrir.jcollector.ui.view.RepositoryExplorerView;
import org.fenrir.jcollector.ui.view.TagExplorerView;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.entity.Tag;
import org.fenrir.jcollector.core.event.ApplicationEvent;
import org.fenrir.jcollector.core.event.IApplicationEventListener;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.core.service.IContentSearchService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140530
 */
public class ExplorerController extends AbstractController<ApplicationModel> implements IApplicationEventListener
{
	public static final String TAG_LIST_MODEL = "TAG_LIST_MODEL";
	public static final String SELECTED_TAG = "SELECTED_TAG";

	private Logger log = LoggerFactory.getLogger(ExplorerController.class);

    @Inject
	private CollectionExplorerView collectionExplorerView;
    
    @Inject
	private RepositoryExplorerView repositoryExplorerView;
    
    @Inject
	private TagExplorerView tagExplorerView;

    @Inject
	private IContentSearchService contentSearchService;

	@Inject
    public void setApplicationModel(ApplicationModel model)
    {
        setModel(model);
    }

    public void setCollectionExplorerView(CollectionExplorerView view)
	{
        this.collectionExplorerView = view;
		attachView(view);		
	}

	public void setRepositoryExplorerView(RepositoryExplorerView view)
	{
        this.repositoryExplorerView = view;
		attachView(view);		
	}

	public void setTagExplorerView(TagExplorerView view)
	{
        this.tagExplorerView = view;
		attachView(view);		
	}

	public void setContentSearchService(IContentSearchService contentSearchService)
	{
		this.contentSearchService = contentSearchService;
	}

	/**
	 * Mètode del controlador encarregat d'especificar al model de l'aplicació la col.lecció sel.leccionada.
	 * En el cas de les col.leccions, la sel.lecció no implica que la resta d'elements del model
	 * (grups, repositoris i tags) s'hagin de desel.leccionar ja que les col.leccions en cap cas
	 * es mostren a la vista de continguts (ThumbnailsView)
	 * ATENCIÓ: Es pot donar el cas que la col.lecció sel.leccionada no correspongui amb el grup
	 * si s'ha sel.leccionat una altra després de fer la sel.lecció d'un grup.
	 * @param collection Collection - Col.lecció a sel.leccionar al model de l'aplicació
	 */
	public void setSelectedCollection(Collection collection)
	{
		model.setSelectedCollection(collection);
	}

	/**
	 * Mètode del controlador encarregat d'especificar al model de l'aplicació el grup i la
	 * col.lecció corresponent sel.leccionats.
	 * Al contrari que en el cas de les col.leccions, la sel.lecció d'un grup implica la deselecció
	 * de la resta d'elements del model de l'aplicació tret de la col.lecció, es a dir, repositoris i tags,
	 * ja que es tracta d'un element visualitzable a la vista de contiguts de l'aplicació (ThumbnailsView)
	 * @param group PictureGroup - Grup a sel.leccionar al model de l'aplicació
	 */
	public void setSelectedGroup(PictureGroup group)
	{
		if(group==null){
			model.setSelectedCollection(null, false);
			model.setSelectedGroup(null);
		}
		else{
			Collection collection = contentSearchService.findCollectionById(group.getCollection().getId());
			model.setSelectedCollection(collection, false);
			model.setSelectedGroup(group);
			model.setSelectedRepository(null, false);
			model.setSelectedTag(null, false);
		}

		repositoryExplorerView.clearSelection();
		tagExplorerView.clearSelection();
	}

	/**
	 * Mètode del controlador encarregat d'especificar al model de l'aplicació el repositori sel.leccionat.
	 * Al contrari que en el cas de les col.leccions, la sel.lecció d'un repositori implica la deselecció
	 * de la resta d'elements del model de l'aplicació (col.leccions, grups i tags),
	 * ja que es tracta d'un element visualitzable a la vista de contiguts de l'aplicació (ThumbnailsView)
	 * @param repository Repository - Repositori a sel.leccionar al model de l'aplicació
	 */
	public void setSelectedRepository(Repository repository)
	{
		model.setSelectedRepository(repository);
		model.setSelectedGroup(null, false);
		model.setSelectedTag(null, false);

		collectionExplorerView.clearSelection();
		tagExplorerView.clearSelection();
	}

	/**
	 * Mètode del controlador encarregat d'especificar al model de l'aplicació el tag sel.leccionat.
	 * Al contrari que en el cas de les col.leccions, la sel.lecció d'un tag implica la deselecció
	 * de la resta d'elements del model de l'aplicació (col.leccions, grups i repositoris),
	 * ja que es tracta d'un element visualitzable a la vista de contiguts de l'aplicació (ThumbnailsView)
	 * @param tag Tag - Tag a sel.leccionar al model de l'aplicació
	 */
	public void setSelectedTag(Tag tag)
	{
		model.setSelectedTag(tag);
		model.setSelectedRepository(null, false);
		model.setSelectedGroup(null, false);

		collectionExplorerView.clearSelection();
		repositoryExplorerView.clearSelection();
	}

	@Override
	public void propertyChange(PropertyChangeEvent event)
	{
		if(ApplicationModel.PROPERTY_SELECTED_COLLECTION.equals(event.getPropertyName())){
			Collection collection = (Collection)event.getNewValue();
			if(collection!=null){
				try{
					// Es visualitzen els grups de la col.lecció
					List<PictureGroup> groups = contentSearchService.findCollectionPictureGroups(collection.getId());
					collectionExplorerView.updateCollectionGroups(collection, groups);
				}
				catch(Exception e){
					log.error("Error al recarregar la vista per la col.lecció {}: {}", new Object[]{collection.getId(), e.getMessage()}, e);
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recarregar la vista per la col.lecció "  + collection.getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	@Override
	public void applicationEventPerformed(ApplicationEvent event)
	{
		if(ApplicationEvent.EVENT_WORKSPACE_LOADED.equals(event.getType())){
			updateCollectionExplorerView();
			updateRepositoryExplorerView();
			updateTagExplorerView();
		}
		else if(ApplicationEvent.EVENT_COLLECTION_CREATED.equals(event.getType())
				|| ApplicationEvent.EVENT_COLLECTION_MODIFIED.equals(event.getType())
				|| ApplicationEvent.EVENT_COLLECTION_DELETED.equals(event.getType())){
			updateCollectionExplorerView();
		}
		else if(ApplicationEvent.EVENT_GROUP_CREATED.equals(event.getType())
				|| ApplicationEvent.EVENT_GROUP_MODIFIED.equals(event.getType())){
			PictureGroup group = (PictureGroup)event.getNewValue();
			updateCollectionExplorerView(group.getCollection().getId());
		}
		else if(ApplicationEvent.EVENT_GROUP_CONTENT_UPDATED.equals(event.getType())){
			PictureGroup group = (PictureGroup)event.getOldValue();
			updateCollectionExplorerView(group.getCollection().getId());
		}
		else if(ApplicationEvent.EVENT_REPOSITORY_CREATED.equals(event.getType())
				|| ApplicationEvent.EVENT_REPOSITORY_MODIFIED.equals(event.getType())
				|| ApplicationEvent.EVENT_REPOSITORY_DELETED.equals(event.getType())
				|| ApplicationEvent.EVENT_REPOSITORY_CONTENT_UPDATED.equals(event.getType())){
			updateRepositoryExplorerView();
		}
		else if(ApplicationEvent.EVENT_TAG_CREATED.equals(event.getType())
				|| ApplicationEvent.EVENT_TAG_MODIFIED.equals(event.getType())
				|| ApplicationEvent.EVENT_TAG_DELETED.equals(event.getType())
				|| ApplicationEvent.EVENT_TAG_CONTENT_UPDATED.equals(event.getType())){
			updateTagExplorerView();
		}
		else if(ApplicationEvent.EVENT_PICTURE_DELETED.equals(event.getType())){
			Picture oldPicture = (Picture)event.getOldValue();
			if(oldPicture.getPictureGroup()!=null || oldPicture.getCollection()!=null){
				updateCollectionExplorerView();
			}
			if(oldPicture.getRepository()!=null){
				updateRepositoryExplorerView();
			}
			updateTagExplorerView();
		}
	}

	public void loadExplorerViews()
	{
		updateCollectionExplorerView();
		updateRepositoryExplorerView();
		updateTagExplorerView();
	}
	
	private void updateCollectionExplorerView()
	{
		List<Collection> collections = contentSearchService.findAllCollections();
		collectionExplorerView.updateCollections(collections);
	}

	private void updateCollectionExplorerView(Long collectionId)
	{
		try{
			Collection collection = contentSearchService.findCollectionById(collectionId);
			List<PictureGroup> groups = contentSearchService.findCollectionPictureGroups(collectionId);
			collectionExplorerView.updateCollectionGroups(collection, groups);
		}
		catch(BusinessException e){
			log.error("Error en actualitzar la vista d'exploració de les col.leccions: ", e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error en actualitzar la vista d'exploració de les col.leccions", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void updateRepositoryExplorerView()
	{
		List<Repository> repositories = contentSearchService.findAllActiveRepositories();
		repositoryExplorerView.updateRepositoryList(repositories);
	}

	private void updateTagExplorerView()
	{
		try{
			List<Tag> tags = contentSearchService.findAllTags();
			tagExplorerView.updateTagList(tags);
		}
		catch(BusinessException e){
			log.error("Error en actualitzar la vista d'exploració de tags: ", e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error en actualitzar la vista d'exploració de tags", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}
