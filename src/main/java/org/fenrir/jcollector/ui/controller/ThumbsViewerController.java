package org.fenrir.jcollector.ui.controller;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import java.beans.PropertyChangeEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.mvc.AbstractController;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.jcollector.ui.builder.PictureCellBuilder;
import org.fenrir.jcollector.ui.builder.PictureGroupCellBuilder;
import org.fenrir.jcollector.ui.dto.ICell;
import org.fenrir.jcollector.ui.model.ApplicationModel;
import org.fenrir.jcollector.ui.view.ThumbnailsView;
import org.fenrir.jcollector.core.CorePreferenceConstants;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.entity.Tag;
import org.fenrir.jcollector.core.event.ApplicationEvent;
import org.fenrir.jcollector.core.event.IApplicationEventListener;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.core.service.IContentSearchService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20140601
 */
public class ThumbsViewerController extends AbstractController<ApplicationModel> implements IApplicationEventListener
{
	private int CURRENT_VIEW_NONE = 0;
	private int CURRENT_VIEW_GROUP = 1;
	private int CURRENT_VIEW_REPOSITORY = 2;
	private int CURRENT_VIEW_TAG = 3;

	private Logger log = LoggerFactory.getLogger(ThumbsViewerController.class);

	private ThumbnailsView view;

	private int currentView = CURRENT_VIEW_NONE;

	private IContentSearchService contentSearchService;
	private IPreferenceService preferenceService;

	public ThumbsViewerController(ApplicationModel model)
	{
		super(model);
	}

	public void setThumbnailsView(ThumbnailsView view)
	{
		attachView(view);
		this.view = view;
	}

	public void setContentSearchService(IContentSearchService contentSearchService)
	{
		this.contentSearchService = contentSearchService;
	}

	public void setPreferenceService(IPreferenceService preferenceService)
	{
		this.preferenceService = preferenceService;
	}

	public void setSelectedGroup(PictureGroup group)
	{
		model.setSelectedGroup(group);
	}

	public void setSelectedPicture(Picture picture)
	{
		model.setSelectedPicture(picture);
	}

	@Override
	public void propertyChange(PropertyChangeEvent event)
	{
		if(ApplicationModel.PROPERTY_SELECTED_GROUP.equals(event.getPropertyName())){
			PictureGroup group = (PictureGroup)event.getNewValue();
			if(group!=null){
				try{
					// Es visualitza el nou grup sel.leccionat i reposicionant la vista al principi
					visualizeGroup(group, true);
				}
				catch(Exception e){
					log.error("Error al recarregar la vista pel grup " + group.getId() + ": " + e.getMessage(), e);
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recarregar la vista pel grup "  + group.getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
				currentView = CURRENT_VIEW_GROUP;
			}
		}
		else if(ApplicationModel.PROPERTY_SELECTED_REPOSITORY.equals(event.getPropertyName())){
			Repository repository = (Repository)event.getNewValue();
			if(repository!=null){
				try{
					// Es visualitza el nou repositori sel.leccionat i reposicionant la vista al principi
					visualizeRepository(repository, true);
				}
				catch(Exception e){
					log.error("Error al recarregar la vista pel repositori " + repository.getId() + ": " + e.getMessage(), e);
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recarregar la vista pel repositori "  + repository.getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
				currentView = CURRENT_VIEW_REPOSITORY;
			}
		}
		else if(ApplicationModel.PROPERTY_SELECTED_TAG.equals(event.getPropertyName())){
			Tag tag = (Tag)event.getNewValue();
			if(tag!=null){
				try{
					// Es visualitza el nou tag sel.leccionat i reposicionant la vista al principi
					visualizeTag(tag, true);
				}
				catch(Exception e){
					log.error("Error al recarregar la vista pel tag " + tag.getId() + ": " + e.getMessage(), e);
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recarregar la vista pel tag "  + tag.getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
				currentView = CURRENT_VIEW_TAG;
			}
		}
	}

	@Override
	public void applicationEventPerformed(ApplicationEvent event)
	{
		if(ApplicationEvent.EVENT_WORKSPACE_LOADED.equals(event.getType())){
			int fetchLimit = 0;
			String value = preferenceService.getProperty(CorePreferenceConstants.THUMBS_FETCH_COUNT);
			if(value!=null){
				try{
					fetchLimit = Integer.parseInt(value);
				}
				catch(NumberFormatException e){
					log.warn("La propietat fetch-limit indicada per la vista no contñe un valor vàlid");
				}
			}

			view.setViewFetchLimit(fetchLimit);
		}
		else if(currentView==CURRENT_VIEW_GROUP
				&& (ApplicationEvent.EVENT_GROUP_MODIFIED.equals(event.getType())
					|| ApplicationEvent.EVENT_GROUP_CREATED.equals(event.getType())
					|| ApplicationEvent.EVENT_GROUP_CONTENT_UPDATED.equals(event.getType())
				)){
			PictureGroup oldGroup = (PictureGroup)event.getOldValue();
			// En l'event ApplicationEvent.EVENT_GROUP_CONTENT_UPDATED el valor que arriba en newValue és null
			PictureGroup newGroup = (PictureGroup)event.getNewValue();
			// Cas d'actualització del contingut del grup actualment sel.leccionat
			if((oldGroup!=null && oldGroup.getId().equals(model.getSelectedGroup().getId()))
					// Cas que s'hagi modificat el grup actualment sel.leccionat
					|| (newGroup!=null && newGroup.getId().equals(model.getSelectedGroup().getId()))
					// Cas que s'hagi creat un grup fill del grup actualment sel.leccionat
					|| (newGroup!=null && newGroup.getParent()!=null && newGroup.getParent().equals(model.getSelectedGroup().getId()))){
				try{
					// Es refresca el grup actual però es manté la posició de visualització
					visualizeGroup(model.getSelectedGroup(), false);
				}
				catch(Exception e){
					log.error("Error al recarregar la vista pel grup " + model.getSelectedGroup().getId() + ": " + e.getMessage(), e);
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recarregar la vista pel grup "  + model.getSelectedGroup().getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		else if(currentView==CURRENT_VIEW_REPOSITORY
				&&(ApplicationEvent.EVENT_REPOSITORY_MODIFIED.equals(event.getType())
					|| ApplicationEvent.EVENT_REPOSITORY_CONTENT_UPDATED.equals(event.getType())
				)){
			Repository oldRepository = (Repository)event.getOldValue();
			// En l'event ApplicationEvent.EVENT_REPOSITORY_CONTENT_UPDATED el valor que arriba és null
			Repository newRepository = (Repository)event.getNewValue();
			if((oldRepository!=null && oldRepository.getId().equals(model.getSelectedRepository().getId()))
					|| (newRepository!=null && newRepository.getId().equals(model.getSelectedRepository().getId()))){
				try{
					// Es refresca el repositori actual però es manté la posició de visualització
					visualizeRepository(model.getSelectedRepository(), false);
				}
				catch(Exception e){
					log.error("Error al recarregar la vista pel repositori " + model.getSelectedRepository().getId() + ": " + e.getMessage(), e);
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recarregar la vista pel repositori "  + model.getSelectedRepository().getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		else if(currentView==CURRENT_VIEW_TAG
				&& (ApplicationEvent.EVENT_TAG_MODIFIED.equals(event.getType())
					|| ApplicationEvent.EVENT_TAG_CONTENT_UPDATED.equals(event.getType())
				)){
			try{
				// Es refresca el tag actual però es manté la posició de visualització
				visualizeTag(model.getSelectedTag(), false);
			}
			catch(Exception e){
				log.error("Error al recarregar la vista pel tag " + model.getSelectedTag().getId() + ": " + e.getMessage(), e);
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recarregar la vista pel tag "  + model.getSelectedTag().getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		else if(ApplicationEvent.EVENT_PICTURE_MODIFIED.equals(event.getType())){
			Picture oldPicture = (Picture)event.getOldValue();
			Picture newPicture = (Picture)event.getNewValue();
			PictureGroup selectedGroup = model.getSelectedGroup();
			Repository selectedRepository = model.getSelectedRepository();
			if(currentView==CURRENT_VIEW_GROUP
					&& (selectedGroup.getId().equals(oldPicture.getPictureGroup()) || selectedGroup.getId().equals(newPicture.getPictureGroup()))){
				try{
					// Es refresca el grup actual però es manté la posició de visualització
					visualizeGroup(model.getSelectedGroup(), false);
				}
				catch(Exception e){
					log.error("Error al recarregar la vista pel grup " + model.getSelectedGroup().getId() + ": " + e.getMessage(), e);
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recarregar la vista pel grup "  + model.getSelectedGroup().getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
			else if(currentView==CURRENT_VIEW_REPOSITORY
					&& (selectedRepository.getId().equals(oldPicture.getRepository()) || selectedRepository.getId().equals(newPicture.getRepository()))){
				try{
					// Es refresca el repositori actual però es manté la posició de visualització
					visualizeRepository(selectedRepository, false);
				}
				catch(Exception e){
					log.error("Error al recarregar la vista pel repositori " + model.getSelectedRepository().getId() + ": " + e.getMessage(), e);
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recarregar la vista pel repositori "  + model.getSelectedRepository().getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
			else if(currentView==CURRENT_VIEW_TAG){
				try{
					Tag selectedTag = model.getSelectedTag();
					Collection<Tag> pictureTags = oldPicture.getTags();
					// Si la imatge modificada conté el tag que s'està visualitzant es refresca la vista
					if(pictureTags.contains(selectedTag)){
						visualizeTag(selectedTag, false);
					}
				}
				catch(Exception e){
					log.error("Error al recarregar la vista pel repositori " + model.getSelectedRepository().getId() + ": " + e.getMessage(), e);
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recarregar la vista pel repositori "  + model.getSelectedRepository().getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		else if(ApplicationEvent.EVENT_PICTURE_DELETED.equals(event.getType())){
			Picture oldPicture = (Picture)event.getOldValue();
			PictureGroup selectedGroup = model.getSelectedGroup();
			Repository selectedRepository = model.getSelectedRepository();
			if(currentView==CURRENT_VIEW_GROUP && selectedGroup.getId().equals(oldPicture.getPictureGroup())){
				try{
					// Es refresca el grup actual però es manté la posició de visualització
					visualizeGroup(selectedGroup, false);
				}
				catch(Exception e){
					log.error("Error al recarregar la vista pel grup " + selectedGroup.getId() + ": " + e.getMessage(), e);
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recarregar la vista pel grup "  + selectedGroup.getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
			else if(currentView==CURRENT_VIEW_REPOSITORY && selectedRepository.getId().equals(oldPicture.getRepository())){
				try{
					// Es refresca el repositori actual però es manté la posició de visualització
					visualizeRepository(selectedRepository, false);
				}
				catch(Exception e){
					log.error("Error al recarregar la vista pel repositori " + selectedRepository.getId() + ": " + e.getMessage(), e);
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recarregar la vista pel repositori "  + selectedRepository.getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
			else if(currentView==CURRENT_VIEW_TAG){
				Tag selectedTag = model.getSelectedTag();
				try{
					// Es refresca el tag actual però es manté la posició de visualització
					visualizeTag(selectedTag, false);
				}
				catch(Exception e){
					log.error("Error al recarregar la vista pel tag " + selectedTag.getId() + ": " + e.getMessage(), e);
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recarregar la vista pel tag "  + selectedTag.getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	public void visualizeAncestor(Long id)
	{
		try{
			PictureGroup group = contentSearchService.findPictureGroupById(id);
			// Canviant el grup sel.leccionat es llança la visualització del grup
			model.setSelectedGroup(group);
		}
		catch(BusinessException e){
			log.error("Error al carregar grup "  + id + ": " + e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recarregar grup "  + id + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void visualizeGroup(PictureGroup group, boolean resetPosition) throws BusinessException, IOException
	{
		LinkedList<Object[]> ancestors = new LinkedList<Object[]>();
		// Es calculen els antecessors
		PictureGroup aux = group;
		while(aux.getParent()!=null){
			aux = contentSearchService.findPictureGroupById(aux.getParent().getId());
			Object[] ancestor = new Object[]{aux.getId(), aux.getName()};
			ancestors.addFirst(ancestor);
		}
		// Es construeixen les cel.les
		List<ICell> vCells = new ArrayList<ICell>();
		List<PictureGroup> vGroups = contentSearchService.findPictureGroupsByParent(group.getId());
		PictureGroupCellBuilder groupCellBuilder = new PictureGroupCellBuilder();
		for(PictureGroup elem:vGroups){
			vCells.add(groupCellBuilder.setGroup(elem).createCell());
		}
		List<Picture> vPictures = contentSearchService.findPicturesByGroup(group.getId(), true);
		PictureCellBuilder pictureCellBuilder = new PictureCellBuilder();
		for(Picture elem:vPictures){
			vCells.add(pictureCellBuilder.setPicture(elem).createCell());
		}

		long groupCount = contentSearchService.countPicturesByGroup(group.getId(), true);
		String groupTitle = group.getName() + " (" + groupCount + ")";
		// TODO v0.2 Posar icona pels grups
		view.updateView(groupTitle, null, ancestors, vCells, resetPosition);
	}

	private void visualizeRepository(Repository repository, boolean resetPosition) throws BusinessException
	{
		List<ICell> vCells = new ArrayList<ICell>();
		List<Picture> vPictures = contentSearchService.findOutstandingPicturesByRepository(repository.getId());
		PictureCellBuilder pictureCellBuilder = new PictureCellBuilder();
		for(Picture elem:vPictures){
			vCells.add(pictureCellBuilder.setPicture(elem).createCell());
		}

		long repositoryCount = contentSearchService.countOutstandingPicturesByRepository(repository.getId());
		String repositoryTitle = repository.getName() + " (" + repositoryCount + ")";
		view.updateView(repositoryTitle, new ImageIcon("data/icons/repository_48.png"), vCells, resetPosition);
	}

	private void visualizeTag(Tag tag, boolean resetPosition) throws BusinessException
	{
		List<ICell> vCells = new ArrayList<ICell>();
		List<Picture> vPictures = contentSearchService.findTaggedPictures(tag.getId());
		PictureCellBuilder pictureCellBuilder = new PictureCellBuilder();
		for(Picture elem:vPictures){
			vCells.add(pictureCellBuilder.setPicture(elem).createCell());
		}

		long tagCount = contentSearchService.countTaggedPictures(tag.getId());
		String tagTitle = tag.getName() + " (" + tagCount + ")";
		view.updateView(tagTitle, new ImageIcon("data/icons/tag_48.png"), vCells, resetPosition);
	}
}
