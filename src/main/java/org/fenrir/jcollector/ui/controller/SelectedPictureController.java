package org.fenrir.jcollector.ui.controller;

import java.awt.image.BufferedImage;
import javax.swing.JOptionPane;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.mvc.AbstractController;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.jcollector.ui.dialog.PictureRenameDialog;
import org.fenrir.jcollector.ui.model.ApplicationModel;
import org.fenrir.jcollector.ui.view.PicturePreview;
import org.fenrir.jcollector.ui.view.PicturePropertiesView;
import org.fenrir.jcollector.ui.view.PictureTagsView;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.entity.Tag;
import org.fenrir.jcollector.core.event.ApplicationEvent;
import org.fenrir.jcollector.core.event.IApplicationEventListener;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.core.service.IContentAdministrationService;
import org.fenrir.jcollector.core.service.IContentSearchService;
import org.fenrir.jcollector.util.CommonUtils;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.4.20140601
 */
public class SelectedPictureController extends AbstractController<ApplicationModel>
{
	private Logger log = LoggerFactory.getLogger(SelectedPictureController.class);

	private PicturePreview picturePreview;
	private PicturePropertiesView propertiesView;
	private PictureTagsView pictureTagsView;

	private IEventNotificationService eventNotificationService;
	private IContentSearchService contentSearchService;
	private IContentAdministrationService contentAdministrationService;

	public SelectedPictureController(ApplicationModel model)
	{
		super(model);
	}

	public void setPicturePreview(PicturePreview view)
	{
		attachView(view);
		this.picturePreview = view;
	}

	public void setPicturePorpertiesView(PicturePropertiesView view)
	{
		attachView(view);
		this.propertiesView = view;
	}

	public void setPictureTagsView(PictureTagsView view)
	{
		attachView(view);
		this.pictureTagsView = view;
	}

	public void setEventNotificationService(IEventNotificationService eventNotificationService)
	{
		this.eventNotificationService = eventNotificationService;
	}

	public void setContentSearchService(IContentSearchService contentSearchService)
	{
		this.contentSearchService = contentSearchService;
	}

	public void setContentAdministrationService(IContentAdministrationService contentAdministrationService)
	{
		this.contentAdministrationService = contentAdministrationService;
	}

	public List<Collection> getAllCollections() throws BusinessException
	{
		return contentSearchService.findAllCollections();
	}

	public void savePicture(Repository selectedRepository,
			Collection selectedCollection,
			PictureGroup selectedGroup,
			boolean isCollected,
			String collectionDate)
	{
		String picturePath = null;
		String destinationPath = null;

		try{
			// Es crea una còpia de la imatge sel.leccionada per tal de fer-hi els canvis
			Picture oldPicture = model.getSelectedPicture();
			Picture newPicture = new Picture(oldPicture);

			// Si s'ha marcat com a imatge definitiva han d'estar completes totes les dades
			if(isCollected){
				if(selectedCollection==null){
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Ha de sel.leccionar una col.lecció o desmarcar el canvi com a definitiu", "Advertencia", JOptionPane.WARNING_MESSAGE);
					return;
				}
				if(selectedGroup==null){
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Ha de sel.leccionar un grup o desmarcar el canvi com a definitiu", "Advertencia", JOptionPane.WARNING_MESSAGE);
					return;
				}
				if(StringUtils.isBlank(collectionDate)){
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Ha d'introduir una data o desmarcar el canvi com a definitiu", "Advertencia", JOptionPane.WARNING_MESSAGE);
					return;
				}
			}

			// Es recupera la ruta per moure la imatge en cas de que sigui definitiva o s'hagi canviat de repositori
			picturePath = CommonUtils.getPictureAbsolutePath(newPicture);

			// Es modifiquen les dades de la imatge
			Repository pictureRepository = selectedRepository!=null ? selectedRepository : null;
			newPicture.setRepository(pictureRepository);
			Collection pictureCollection = selectedCollection!=null ? selectedCollection : null;
			newPicture.setCollection(pictureCollection);
			PictureGroup pictureGroup = selectedGroup!=null ? selectedGroup : null;
			newPicture.setPictureGroup(pictureGroup);
			if(StringUtils.isNotBlank(collectionDate)){
				newPicture.setCollectionDate(new SimpleDateFormat("dd/MM/yyyy").parse(collectionDate));
			}
			newPicture.setCollected(isCollected);

			/* Si es definitiu i no ho era abans, no tenia grup assignat o aquest s'ha cambiat
			 * es mou la imatge al directori del grup
			 */
			boolean moved = false;
			if(isCollected && !oldPicture.isCollected()
					|| isCollected && !selectedGroup.getId().equals(oldPicture.getPictureGroup())){
				String extension = FilenameUtils.getExtension(picturePath);
				String namePattern = selectedGroup.getPattern();
				String groupPath = selectedGroup.getFolderPath();
				String pictureName = CommonUtils.getPictureBasename(newPicture);
				// TODO Canviar al implementar la funcionalitat de renombrar
//				String nameField = inputImageName.getText();
				String nameField = pictureName;
				String newName = null;
				// Si s'ha modificat el nom de la imatge a mà es respecta
				if(StringUtils.isNotBlank(pictureName) && nameField.equals(pictureName)){
					/* TODO v0.2 Millorar la manera en que es calcula l'índex de la imatges dins del grup
					 * Es determina el comptador que li tocaria a la imatge dins del grup
					 */
					long index = contentSearchService.countPicturesByGroup(selectedGroup.getId(), true) + 1;
					String strIndex = StringUtils.leftPad(Long.toString(index), 2, "0");
					// Es reemplaça l'índex
					namePattern = namePattern.replace("##", strIndex);
					// Es reemplacen els paràmetres Width i Height
					namePattern = namePattern.replace("[W]", newPicture.getWidth().toString());
					newName = namePattern.replace("[H]", newPicture.getHeight().toString());
				}
				else{
					newName = nameField;
				}

				// S'obre la finestra de verificació del nom introduit
				PictureRenameDialog renameDialog = new PictureRenameDialog(ApplicationWindowManager.getInstance().getMainWindow(), newPicture, newName);
				if(PictureRenameDialog.DIALOG_OK==renameDialog.open()){
					newName = renameDialog.getPictureName();
				}

				destinationPath = groupPath + File.separator + newName + "." + extension;
				File renamedFile = new File(destinationPath);
				if(renamedFile.exists()){
					// Es reseteja a null per tal de no esborrar la imatge amb la que colisiona per accident
					destinationPath = null;
					throw new Exception("El nom de fitxer ja existeix: " + newName);
				}

				if(!picturePath.equals(destinationPath)){
					FileUtils.copyFile(new File(picturePath), renamedFile);
					moved = true;
				}
				newPicture.setRelativePath(newName + "." + extension);
			}
			/* Es mou la imatge de repositori en cas que no sigui definitiva i s'hagi canviat el repositori
			 * o s'hagi desmarcat el check de definitiu.
			 */
			else if(!isCollected && oldPicture.isCollected()
					|| !isCollected && selectedRepository!=null	&& !selectedRepository.getId().equals(oldPicture.getRepository())){
				destinationPath = selectedRepository.getFolderPath() + File.separator + newPicture.getRelativePath();
				FileUtils.copyFile(new File(picturePath), new File(destinationPath));
				moved = true;
			}

			contentAdministrationService.updatePicture(newPicture.getId(),
					newPicture.getCollection()!=null ? newPicture.getCollection().getId() : null,
					newPicture.getRepository()!=null ? newPicture.getRepository().getId() : null,
					newPicture.getPictureGroup()!=null ? newPicture.getPictureGroup().getId() : null,
					newPicture.getRelativePath(),
					newPicture.isCollected(),
					newPicture.getCollectionDate());

			// Si tot ha anat bé s'esborra la imatge antiga per completar el moviment
			if(moved){
				FileUtils.deleteQuietly(new File(picturePath));
			}

			// Es llança event de refresc per les vistes
			ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_PICTURE_MODIFIED);
			event.setOldValue(oldPicture);
			event.setNewValue(newPicture);
			eventNotificationService.notifyEvent(IApplicationEventListener.class, event);
		}
		catch(Exception e){
			log.error("Error al guardar imatge: " + e.getMessage(), e);
			// Si hi ha hagut un error, s'ha de revertir el moviment de fitxer
			if(destinationPath!=null){
				File srcFile = new File(picturePath);
				File destinationFile = new File(destinationPath);
				/* Primer ens assegurem que el fitxer origen existeixi al final del procés.
				 * Això pot succeir si un listener falla després d'haver esborrat el fitxer original
				 */
				try{
					if(!srcFile.exists()){
						FileUtils.copyFile(destinationFile, srcFile);
					}

					if(destinationFile.exists()){
						FileUtils.deleteQuietly(new File(destinationPath));
						log.warn("Esborrat fitxer " + destinationPath + " a causa de l'error!");
					}
				}
				catch(IOException e2){
					log.error("Error al revertir la imatge " + picturePath + ": " + e.getMessage(), e);
				}
			}
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al guardar imatge: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void searchTags(String tagNamePrefix)
	{
		try{
			List<Tag> tags = contentSearchService.findTagsNameLike(tagNamePrefix);
			pictureTagsView.addTagSuggestions(tags);
		}
		catch(BusinessException e){
			log.error("Error al buscar tags de la imatge: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al buscar tags de la imatge: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void searchPreferredTags()
	{
		searchPreferredTags(null);
	}

	public void searchPreferredTags(String tagNamePrefix)
	{
		try{
			List<Tag> tags = null;
			if(StringUtils.isNotBlank(tagNamePrefix)){
				tags = contentSearchService.findPreferredTagsNameLike(tagNamePrefix);
			}
			else{
				tags = contentSearchService.findPreferredTags();
			}
			pictureTagsView.addTagSuggestions(tags);
		}
		catch(BusinessException e){
			log.error("Error al recuperar tags favorits: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al recuperar tags favorits: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void tagPicture(String tagName)
	{
		try{
			boolean createTag = false;
			List<Tag> candidateTags = contentSearchService.findTagsNameLike(tagName);
			// Si la llista de candidats es buida es crea el tag i s'associa a la imatge
			if(candidateTags.isEmpty()){
				createTag = true;
			}
			// En el cas que existeixin candidats...
			else{
				// Si n'hi ha algún que coincideix completament, s'associa directament en cas que no ho estigui
				Iterator<Tag> iterator = candidateTags.iterator();
				boolean tagged = false;
				while(iterator.hasNext() && !tagged){
					Tag tag = iterator.next();
					if(tag.getName().equalsIgnoreCase(tagName)){
						tagPicture(tag);
						tagged = true;

						// Es refresca la vista amb el nou tag sel.leccionat
						pictureTagsView.addPictureTag(tag);
					}
				}

				// En cas que no n'hi hagi cap, es crea un nou
				if(!tagged){
					createTag = true;
				}
			}

			// Es crea el nou tag en cas que sigui necessari
			if(createTag){
				// Es demana confirmació
				int option = JOptionPane.showConfirmDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Crear tag amb el nom introduit?");
				if(option==JOptionPane.OK_OPTION){
					Tag tag = contentAdministrationService.tagPicture(tagName, model.getSelectedPicture().getId());
					log.info("Afegit tag " + tag.getId() + " a la imatge " + model.getSelectedPicture().getId());
					pictureTagsView.addPictureTag(tag);

					// Es llança event de refresc per les vistes d'exploració de tags
					ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_TAG_CREATED);
					event.setNewValue(tag);
					eventNotificationService.notifyEvent(IApplicationEventListener.class, event);
				}
			}
		}
		catch(Exception e){
			log.error("Error al associar tag: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al associar tag: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void tagPicture(Tag tag)
	{
		try{
			contentAdministrationService.tagPicture(tag.getId(), model.getSelectedPicture().getId());
			log.info("Afegit tag " + tag.getId() + " a la imatge " + model.getSelectedPicture().getId());
			// Es llança event de refresc per les vistes
			ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_TAG_CONTENT_UPDATED);
			event.setOldValue(tag);
			event.setNewValue(tag);
			eventNotificationService.notifyEvent(IApplicationEventListener.class, event);
		}
		catch(Exception e){
			log.error("Error associant etiqueta: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al associar tag: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void deletePictureTag(Tag tag)
	{
		try{
			contentAdministrationService.deletePictureTag(tag.getId(), model.getSelectedPicture().getId());
			log.info("Esborrat tag " + tag.getId() + " a la imatge " + model.getSelectedPicture().getId());
			// Es llança event de refresc per les vistes
			ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_TAG_CONTENT_UPDATED);
			event.setOldValue(tag);
			eventNotificationService.notifyEvent(IApplicationEventListener.class, event);

			// Es llança event de refresc per totes les vistes que visualitzin dades de la imatge sel.leccionada
			event = new ApplicationEvent(ApplicationEvent.EVENT_PICTURE_MODIFIED);
			// Com que les dades de la imatge no han sigut modificades, oldValue i newValue seran iguals
			event.setOldValue(model.getSelectedPicture());
			event.setNewValue(model.getSelectedPicture());
			eventNotificationService.notifyEvent(IApplicationEventListener.class, event);
		}
		catch(Exception e){
			log.error("Error desassociant etiqueta: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error desassociant etiqueta: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent event)
	{
		if(ApplicationModel.PROPERTY_SELECTED_PICTURE.equals(event.getPropertyName())){
			Picture picture = (Picture)event.getNewValue();
			if(picture!=null){
				// S'actualitza la vista prèvia
				BufferedImage thumbnail = CommonUtils.createAndLoadThumbnail(picture);
				picturePreview.updatePicturePreview(thumbnail);
				picturePreview.updatePictureFields(picture);
				// S'actualitza la vista de propietats
				propertiesView.setFieldsEnabled(true);
				try{
					Collection collection = picture.getCollection();
					PictureGroup group = picture.getPictureGroup();
					Repository repository = picture.getRepository();
					propertiesView.updateFields(repository, collection, group, picture);
				}
				catch(BusinessException e){
					log.error("Error al actualitzar la vista de propietats de la imatge sel.leccionada: " + e.getMessage(), e);
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al actualitzar la vista de propietats de la imatge sel.leccionada: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
				// S'actualitza la vista de tags
				try{
					pictureTagsView.setFieldsEnabled(true);
					List<Tag> tags = contentSearchService.findAllPictureTags(model.getSelectedPicture().getId());
					pictureTagsView.setPictureTags(tags);
				}
				catch(BusinessException e){
					log.error("Error al actualitzar la vista de tags de la imatge sel.leccionada: " + e.getMessage(), e);
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al actualitzar la vista de tags de la imatge sel.leccionada: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
			else{
				// S'actualitza la vista prèvia
				picturePreview.clearFields();
				// S'actualitza la vista de propietats
				propertiesView.setFieldsEnabled(false);
				// S'actualitza la vista de tags
				pictureTagsView.setFieldsEnabled(false);
			}
		}
	}
}
