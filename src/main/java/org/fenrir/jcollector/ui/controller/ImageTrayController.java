package org.fenrir.jcollector.ui.controller;

import javax.swing.JOptionPane;
import java.beans.PropertyChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.mvc.AbstractController;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.event.ApplicationEvent;
import org.fenrir.jcollector.core.event.IApplicationEventListener;
import org.fenrir.jcollector.ui.model.ImageTrayModel;
import org.fenrir.jcollector.ui.view.ImageTrayView;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20110830
 */
public class ImageTrayController extends AbstractController<ImageTrayModel> implements IApplicationEventListener
{
	private final Logger log = LoggerFactory.getLogger(ImageTrayController.class);
	
	private ImageTrayView view;
	
	public ImageTrayController(ImageTrayModel model)
	{
		super(model);
	}
	
	public void setImageTrayView(ImageTrayView view)
	{
		attachView(view);
		this.view = view;
	}
	
	public void setSelectedPicture(Picture picture)
	{
		model.setSelectedPicture(picture);
	}
	
	public void setPicturePinned(Picture picture, boolean pinned)
	{
		model.setPicturePinned(picture, pinned);
	}
	
	public void removePicture(Picture picture)
	{
		model.removePicture(picture);
	}		
	
	@Override
	public void propertyChange(PropertyChangeEvent event) 
	{	
		if(ImageTrayModel.PROPERTY_SELECTED_PICTURE.equals(event.getPropertyName())){
			Picture picture = (Picture)event.getNewValue();
			model.removeAllUnpinnedTrayElements();
			if(picture!=null){											
				model.addUnpinnedPicture(picture);					
			}			
			
			try{										
				view.updateTray(model.getTrayElements());				
			}
			catch(Exception e){
				log.error("Error al afegir picture a la llista " + picture.getId() + ": " + e.getMessage(), e);
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al afegir picture a la llista " + picture.getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}						
		}
	}
	
	@Override
	public void applicationEventPerformed(ApplicationEvent event) 
	{
		if(ApplicationEvent.EVENT_PICTURE_MODIFIED.equals(event.getType())){
			Picture oldPicture = (Picture)event.getOldValue();
			Picture newPicture = (Picture)event.getNewValue();			
			model.replacePicture(oldPicture, newPicture);			
		}
		else if(ApplicationEvent.EVENT_PICTURE_DELETED.equals(event.getType())){
			Picture oldPicture = (Picture)event.getOldValue();			
			model.removePicture(oldPicture);				
		}
		
		try{										
			view.updateTray(model.getTrayElements());				
		}
		catch(Exception e){
			log.error("Error al actualitzar la llista d'imatges sel.leccionades: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al actualitzar la llista d'imatges sel.leccionades: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}	
	}
}
