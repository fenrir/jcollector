package org.fenrir.jcollector.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.jcollector.ui.widget.ImportGroupContentTableModel;
import org.fenrir.jcollector.ui.widget.ImportGroupContentTableModel.ModelData;
import org.fenrir.jcollector.ui.widget.RepositoryTableCellEditor;
import org.fenrir.jcollector.core.service.IContentAdministrationService;
import org.fenrir.jcollector.core.service.IContentSearchService;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.core.event.ApplicationEvent;
import org.fenrir.jcollector.core.event.IApplicationEventListener;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.4.20140601
 */
@SuppressWarnings("serial")
public class ImportGroupContentDialog extends AbstractDialog 
{
	private final Logger log = LoggerFactory.getLogger(ImportGroupContentDialog.class);
	
	private JTextField inputGroup;
	private JTextField inputFolder;
	private JTable table;
	private JButton bImport;
	
	private PictureGroup selectedGroup;
	
	public ImportGroupContentDialog()
	{
		super(ApplicationWindowManager.getInstance().getMainWindow(), "Importar contingut grup", new Dimension(800, 600), true);

		setIconAsResource("data/icons/repository_add_48.png");
	}
	
	@Override
    protected JComponent createContents()
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        /* Definició dels components */
        JLabel lFolder = new JLabel("Directori:", SwingConstants.RIGHT);
        inputFolder = new JTextField();
        JButton bSearchFolder = new JButton(new ImageIcon("data/icons/search_16.png"));
        bSearchFolder.addActionListener(new ActionListener() 
        {		
			@Override
			public void actionPerformed(ActionEvent event) 
			{
				loadFolderContent();
			}
		});
        bImport = new JButton("Importar");
        bImport.addActionListener(new ActionListener() 
        {			
			@Override
			public void actionPerformed(ActionEvent event) 
			{			
				String strFolderPath = inputFolder.getText();
				if(StringUtils.isBlank(strFolderPath)){
					JOptionPane.showMessageDialog(ImportGroupContentDialog.this, "S'ha d'indicar el directori a importar", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				File folder = new File(strFolderPath);
				if(!folder.exists()){
					JOptionPane.showMessageDialog(ImportGroupContentDialog.this, "S'ha d'indicar un directori a importar que existeixi", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				if(selectedGroup==null){
					JOptionPane.showMessageDialog(ImportGroupContentDialog.this, "S'ha d'indicar el grup", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				// Es deshabiliten els botons
				setCloseButtonEnabled(false);
				bImport.setEnabled(false);
				
				new ImportWorker().execute();
			}
		});
        JLabel lGroupName = new JLabel("Grup:", SwingConstants.RIGHT);
        inputGroup = new JTextField();
        inputGroup.addKeyListener(new KeyAdapter() 
        {
        	@Override
			public void keyReleased(KeyEvent event) 
			{
        		int keyPressed = event.getKeyCode();
        		if(keyPressed==KeyEvent.VK_ENTER){		
        			try{
    					openGroupSelection();
    				}
    				catch(Exception e){
    					log.error("Error al obrir la finestra de sel.lecció de grup: " + e.getMessage(), e);
    				}
        		}
        		else if(selectedGroup!=null){
        			selectedGroup = null;
        		}
			}
		});
        JButton bSearchGroup = new JButton(new ImageIcon("data/icons/search_16.png"));
        bSearchGroup.addActionListener(new ActionListener() 
        {		
			@Override
			public void actionPerformed(ActionEvent event) 
			{			
				try{
					openGroupSelection();
				}
				catch(Exception e){
					log.error("Error al obrir la finestra de sel.lecció de grup: " + e.getMessage(), e);
				}
			}
		});
        table = new JTable(new ImportGroupContentTableModel());
        // Es redimensiona la columna del check        
        table.setFillsViewportHeight(true);
        TableColumn col = table.getColumnModel().getColumn(0);        
        col.setMaxWidth(20);        
        // S'especifica un cellEditor per podr introduir els repositoris
        col = table.getColumnModel().getColumn(3);
        col.setCellEditor(new RepositoryTableCellEditor());        
        JScrollPane scrollTable = new JScrollPane(table);
        JButton bSelectAll = new JButton("Tots");
        bSelectAll.addActionListener(new ActionListener() 
        {			
			@Override
			public void actionPerformed(ActionEvent event) 
			{
				((ImportGroupContentTableModel)table.getModel()).selectAll();
			}
		});
        JButton bSelectNone = new JButton("Cap");
        bSelectNone.addActionListener(new ActionListener() 
        {			
			@Override
			public void actionPerformed(ActionEvent event) 
			{
				((ImportGroupContentTableModel)table.getModel()).selectNone();
			}
		});
        
        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(scrollTable, GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(bSelectAll)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bSelectNone)
                    )
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lGroupName, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lFolder, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        )
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(inputGroup, GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE)
                            .addComponent(inputFolder, GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE)
                        )
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addComponent(bSearchGroup, GroupLayout.PREFERRED_SIZE, 26, Short.MAX_VALUE)
                            .addComponent(bSearchFolder, GroupLayout.PREFERRED_SIZE, 26, Short.MAX_VALUE)
                        )
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bImport, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
                    )
                )
                .addGap(12, 12, 12)
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                    .addComponent(lFolder)
                    .addComponent(inputFolder, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(bImport)
                    .addComponent(bSearchFolder, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                    .addComponent(lGroupName)
                    .addComponent(inputGroup, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(bSearchGroup, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollTable, GroupLayout.PREFERRED_SIZE, 218, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(bSelectAll)
                    .addComponent(bSelectNone)
                )
                .addContainerGap()
            )
        );
        
        return pContents;
    }
	
	private void loadFolderContent()
	{
		JFileChooser fileChooser = new JFileChooser(inputFolder.getText());
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int retValue = fileChooser.showOpenDialog(ImportGroupContentDialog.this);
        if(retValue==JFileChooser.APPROVE_OPTION){
            inputFolder.setText(fileChooser.getSelectedFile().getAbsolutePath());
            File folder = new File(inputFolder.getText());            
            try{
	            IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
	            List<PictureGroup> folderGroups = contentSearchService.findPictureGroupsByFolderPath(folder.getAbsolutePath());
	            // Es recuperen els noms de les imatges ja registrades per poder comparar
	            Set<String> filenames = new HashSet<String>();
	            for(PictureGroup group:folderGroups){
	            	List<Picture> groupPictures = contentSearchService.findPicturesByGroup(group.getId(), true);
	            	for(Picture picture:groupPictures){
	            		// Si la imatge ja ha estat catalogada al grup, el seu path relatiu serà el mateix nom del fitxer
	            		filenames.add(picture.getRelativePath());
	            	}
	            }
	            
	            /* Si el directori indicat conté un fitxer de definició (index.txt) 
				 * es llegeix per associar dates i repositoris a les imatges que s'hi indiqui
				 */
	            Map<String, String[]> index = new HashMap<String, String[]>();
	            File indexFile = new File(folder.getAbsolutePath() + File.separator + "index.txt");
	            if(indexFile.exists()){
	            	try{
	            		List<String> lines = FileUtils.readLines(indexFile);
	            		/* S'interpreta el contingut de les linies del fitxer i es guarda per associar-lo
	            		 * després a les imatges llegides del directori
	            		 * La primera part serà el nom (amb extensió o sense)
	            		 * La segona part serà la data d'inclusió
	            		 * La tercera part serà el repositori associat
	            		 */
	            		for(String line:lines){
	            			if(StringUtils.isNotBlank(line)){
	            				String[] parts = line.trim().split(";");	            		
	            				index.put(parts[0], parts);
	            			}
	            		}
	            	}
	            	catch(IOException e){
	            		log.error("Error al llegir fitxer d'index " + indexFile.getAbsolutePath() + ": " + e.getMessage(), e);
	            	}
	            }
	            
	            // TODO Parametritzar formats de fitxer permesos
	            Collection<File> files = FileUtils.listFiles(folder, new String[]{"jpg", "jpeg", "png", "gif", "bmp"}, false);
	    		for(File file:files){	
	    			// Es comprova que el fitxer no estigui ja registrat al grup en el cas que es processi el mateix directori d'aquest
	    			if(!filenames.contains(file.getName())){
	    				ModelData elem = new ModelData(file);
	    				// Si el registre es troba al fitxer d'index, s'intenta llegir les dades associades	    				
	    				String name = FilenameUtils.getBaseName(file.getAbsolutePath());
	    				// Primer es busca a l'index per nom + extensió
	    				String[] parts = index.get(name);
	    				// Si no es troba es possible que només estigui registrat per nom 
	    				if(parts==null){
	    					String basename = FilenameUtils.getName(file.getAbsolutePath());
	    					parts = index.get(basename);
	    				}
	    				// Si s'han pogut llegir les dades de l'index, s'asocien a la imatge corresponent
	    				if(parts!=null){
	    					try{	    						
	    						// Es comprova que sigui una data vàlida
	    						new SimpleDateFormat("dd/MM/yyyy").parse(parts[1]);
	    						elem.setDate(parts[1]);
	    						
	    						// Es recupera el repositori
	    						Repository repository = contentSearchService.findRepositoryBySource(parts[2]);
	    						elem.setRepository(repository);
	    					}
	    					catch(ParseException e){
	    						log.warn("Error al llegir registre del fitxer d'index. La data no es vàlida: " + e.getMessage());
	    					}
	    					catch(BusinessException e){
	    						log.warn("Error al llegir registre del fitxer d'index. El repositori no es vàlid: " + e.getMessage());
	    					}
	    					catch(Exception e){
	    						log.warn("Error al llegir registre del fitxer d'index: " + e.getMessage());
	    					}
	    				}
		    			((ImportGroupContentTableModel)table.getModel()).addData(elem);
	    			}	    				    			
	    		}
            }
            catch(BusinessException e){
            	log.error("Error al recuperar els grups relatius al directori " + folder.getAbsolutePath() + ": " + e.getMessage(), e);
            	JOptionPane.showMessageDialog(ImportGroupContentDialog.this, "Error al recuperar els grups relatius al directori " + folder.getAbsolutePath(), "Error", JOptionPane.ERROR_MESSAGE);            	            	
            }            
        }                
	}
	
	private void openGroupSelection() throws Exception
	{
		GroupSelectionDialog dialog = null;				
		String groupTerm = inputGroup.getText();
		if(StringUtils.isNotBlank(groupTerm)){
			dialog = new GroupSelectionDialog(null, groupTerm);
		}
		else{
			dialog = new GroupSelectionDialog(null);
		}
		if(dialog.open()==GroupSelectionDialog.DIALOG_OK){
			selectedGroup = dialog.getSelectedGroup();
			inputGroup.setText(selectedGroup.getName());				
		}		
	}
	
	@Override
	public boolean onClose()
	{
		return true;
	}
	
	private class ImportWorker extends SwingWorker<Void, ModelData>
	{
		/**
		 * Mètode que s'encarregarà de realitzar la tasca d'actualitzar els fitxers del repositori
		 * S'executa en un Thread a part.
		 */
		@Override	
		protected Void doInBackground() 
		{
			Iterator<ImportGroupContentTableModel.ModelData> iterator = ((ImportGroupContentTableModel)table.getModel()).getData().iterator();
			while(iterator.hasNext()){
				ImportGroupContentTableModel.ModelData data = iterator.next();				
				if(data.isChecked()){					
					// Es reinicia l'estat del registre per si anteriorment ha estat erroni
					if(data.hasError()){
						data.clearMessage();
					}
					
					Long collection = selectedGroup.getCollection().getId();
					Long repository = data.getRepository()!=null ? data.getRepository().getId() : null;
					Long pictureGroup = selectedGroup.getId();
					Date collectedDate = null;
					try{
						collectedDate = new SimpleDateFormat("dd/MM/yyyy").parse(data.getDate());						
					}
					catch(ParseException e){
						log.error("Error al parsejar data: " + e.getMessage(), e);
						data.setErrorMessage("Data no vàlida");
					}
					
					if(!data.hasError()){
						// Es mou la imatge al directori del grup si l'actual no ho és 
						File destFile = null;											
						if(!FilenameUtils.equals(selectedGroup.getFolderPath(), data.getFile().getParent())){
							String strDestFilename = data.getFilename();
							destFile = new File(selectedGroup.getFolderPath() + File.separator + strDestFilename);					
							if(destFile.exists()){
								data.setErrorMessage("El fitxer destí ja existeix");
							}	
							else{
								try{
									FileUtils.moveFile(data.getFile(), destFile);									
								}
								catch(IOException e){
									log.error("Error al moure fitxer: " + e.getMessage(), e);
									data.setErrorMessage("Error al moure fitxers");
								}
							}
						}									
						else{
							destFile = data.getFile();
						}
						
						if(!data.hasError()){
							try{
								IContentAdministrationService contentAdministrationService = (IContentAdministrationService)ApplicationContext.getInstance().getRegisteredComponent(IContentAdministrationService.class);
								contentAdministrationService.createPicture(collection, repository, pictureGroup, destFile, true, collectedDate);
								
								// Com que la imatge ja ha estat processada, es dehabilita el check de sel.lecció
								data.setInfoMessage("Imatge importada amb èxit!");
								data.setImported(true);
							}
							catch(BusinessException e){							
								log.error("Error al crear imatge: " + e.getMessage(), e);
								data.setErrorMessage("Error al crear imatge");
								// Si el fitxer original s'ha copiat al directori del grup, aquest últim s'esborra
								if(!data.getFile().equals(destFile)){
									log.info("Esborrant fitxer " + destFile.getAbsolutePath());
									FileUtils.deleteQuietly(destFile);
								}							
							}
						}
					}
					
					publish(data);
				}
			}
			
			return null;
		}
		
		@Override
		protected void process(List<ModelData> chunks) 
		{		
			((AbstractTableModel)table.getModel()).fireTableDataChanged();			
		}
		
		/**
		 * Mètode que s'executarà al finalitzar la tasca en el Thread d'event de la UI. Serà l'encarregat
		 * d'actualitzar la UI de la finestra de diàleg amb el resultat final.
		 */
		@Override
		protected void done() 
		{		
			setCloseButtonEnabled(true);
			bImport.setEnabled(true);
			
			try{
				// Es llança event de modificació del grup afectat			
				IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
				ApplicationEvent appEvent = new ApplicationEvent(ApplicationEvent.EVENT_GROUP_CONTENT_UPDATED);
				appEvent.setOldValue(selectedGroup);
				eventNotificationService.notifyEvent(IApplicationEventListener.class, appEvent);
			}
			catch(Exception e){
				log.error("Error notificant event de contingut del grup actualitzat: {}", e.getMessage(), e);
				ApplicationWindowManager.getInstance().displayErrorMessage("Error notificant event de contingut del grup actualitzat: " + e.getMessage(), e);
			}
		}	
	}
}
