package org.fenrir.jcollector.ui.dialog;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.LayoutStyle;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.core.service.IContentSearchService;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.2.20120428
 */
@SuppressWarnings("serial")
public class GroupSelectionDialog extends AbstractConfirmDialog
{
	private final Logger log = LoggerFactory.getLogger(GroupSelectionDialog.class);

	private JTextField inputGroup;
	private JTree treeGroups;

	private Collection selectedCollection;
	private PictureGroup selectedGroup;

	public GroupSelectionDialog(Frame parent)
	{
		super(parent, "Sel.leccionar grup", new Dimension(450, 600), true);
	}

	public GroupSelectionDialog(Frame parent, Collection collection)
	{
		this(parent);

		selectedCollection = collection;
	}

	public GroupSelectionDialog(Frame parent, String term)
	{
		this(parent);

		inputGroup.setText(term);
		search(term);
	}

	public GroupSelectionDialog(Frame parent, Collection collection, String term)
	{
		this(parent);

		inputGroup.setText(term);
		selectedCollection = collection;
		search(term);
	}

	@Override
    protected JComponent createContents()
    {
		JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        inputGroup = new JTextField();
        inputGroup.addKeyListener(new KeyAdapter()
        {
			@Override
			public void keyReleased(KeyEvent event)
			{
				String term = inputGroup.getText();
				search(term);
			}
        });
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode();
        DefaultTreeModel treeModel = new DefaultTreeModel(rootNode);
        treeGroups = new JTree(treeModel);
        // El node arrel no serà visible per tal de poder posar tots els elements sota el mateix arbre
		treeGroups.setRootVisible(false);
		treeGroups.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		JScrollPane scrollTags = new JScrollPane(treeGroups);

		/* Grup horitzontal */
        layout.setHorizontalGroup(
        	layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                	.addComponent(inputGroup, GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
                    .addComponent(scrollTags, GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
        	layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(inputGroup, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollTags, GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                .addContainerGap()
            )
        );

        return pContents;
    }

	@Override
	protected boolean onAccept()
	{
		DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)treeGroups.getLastSelectedPathComponent();
		if(selectedNode==null){
			setMessage("S'ha de sel.leccionar un grup", MSG_TYPE_ERROR);

			return false;
		}
		else{
			Object selectedObj = selectedNode.getUserObject();
			if(!(selectedObj instanceof PictureGroup)){
				setMessage("S'ha de sel.leccionar un grup", MSG_TYPE_ERROR);

				return false;
			}
			selectedGroup = (PictureGroup)selectedObj;
		}

		return true;
	}

	@Override
	protected boolean onCancel()
	{
		return true;
	}

	public PictureGroup getSelectedGroup()
	{
		return selectedGroup;
	}

	private void search(String term)
	{
		DefaultTreeModel treeModel = (DefaultTreeModel)treeGroups.getModel();
		DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode)treeModel.getRoot();

		if(StringUtils.isNotBlank(term)){
			collectPictureGroups(term);
			// S'expandeixen els nodes de l'arbre
			expandTreePath(new TreePath(rootNode));
		}
		else{
			rootNode.removeAllChildren();
			treeModel.reload();
		}
	}

	private void collectPictureGroups(String groupName)
	{
		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
		if(StringUtils.isNotBlank(groupName)){
			try{
				Map<Object, DefaultMutableTreeNode> mapNodes = new HashMap<Object, DefaultMutableTreeNode>();
				List<DefaultMutableTreeNode> vCollectionNodes = new ArrayList<DefaultMutableTreeNode>();
				List<PictureGroup> vGroups = contentSearchService.findPictureGroupsNameLike(groupName);
				for(PictureGroup group:vGroups){
					boolean rootGroup = false;
					while(!rootGroup){
						DefaultMutableTreeNode treeNode;
						if(mapNodes.containsKey(group)){
							treeNode = mapNodes.get(group);
						}
						else{
							treeNode = new DefaultMutableTreeNode(group);
							// Si és un grup pare d'algun altre que ja ha estat afegit, s'enllacen
							for(DefaultMutableTreeNode node:mapNodes.values()){
								Object userObject = node.getUserObject();
								if(userObject instanceof PictureGroup && group.getId().equals(((PictureGroup) userObject).getParent())){
									treeNode.add(node);
								}
							}
							// S'afegeix el node de l'arbre al mapa d'objectes tractats
							mapNodes.put(group, treeNode);
						}

						if(group.getParent()!=null){
							PictureGroup objParent = contentSearchService.findPictureGroupById(group.getParent().getId());
							if(mapNodes.containsKey(objParent)){
								mapNodes.get(objParent).add(treeNode);
							}
							group = objParent;
						}
						else{
							rootGroup = true;
						}
					}
					if(rootGroup){
						Collection collection = contentSearchService.findCollectionById(group.getCollection().getId());
						DefaultMutableTreeNode collectionNode;
						if(mapNodes.containsKey(collection)){
							collectionNode = mapNodes.get(collection);
						}
						else{
							collectionNode = new DefaultMutableTreeNode(collection);
							mapNodes.put(collection, collectionNode);
						}
						DefaultMutableTreeNode treeNode = mapNodes.get(group);
						collectionNode.add(treeNode);
						vCollectionNodes.add(collectionNode);
					}
				}
				DefaultTreeModel treeModel = (DefaultTreeModel)treeGroups.getModel();
				DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode)treeModel.getRoot();
				rootNode.removeAllChildren();
				for(DefaultMutableTreeNode collectionNode:vCollectionNodes){
					/* TODO Refer el filtratge per col.lecció per tal que tiri de query */
					if(selectedCollection==null || selectedCollection.equals(collectionNode.getUserObject())){
						rootNode.add(collectionNode);
					}
				}

				treeModel.reload();
			}
			catch(BusinessException e){
				log.error("Error buscant grups: " + e.getMessage(), e);
				JOptionPane.showMessageDialog(this, "Error buscant grups: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void expandTreePath(TreePath parent)
	{
		TreeNode node = (TreeNode)parent.getLastPathComponent();
	    if(node.getChildCount()>=0){
	    	@SuppressWarnings("unchecked")
	    	Enumeration<TreeNode> children = node.children();
	        while(children.hasMoreElements()){
	            TreeNode n = (TreeNode)children.nextElement();
	            TreePath path = parent.pathByAddingChild(n);
	            expandTreePath(path);
	        }
	    }
	    treeGroups.expandPath(parent);
	}
}
