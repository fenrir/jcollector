package org.fenrir.jcollector.ui.dialog;

import java.util.List;
import java.util.Map;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.jcollector.core.event.ApplicationEvent;
import org.fenrir.jcollector.core.event.IApplicationEventListener;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.core.entity.Tag;
import org.fenrir.jcollector.core.service.IContentAdministrationService;
import org.fenrir.jcollector.core.service.IContentSearchService;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.4.20140610
 */
public class TagManagementDialog extends AbstractEntityManagementDialog<Tag>
{
	private static final long serialVersionUID = -1228031281138827446L;
	
	private static final String PARAMETER_NAME = "TAG";
	
    public TagManagementDialog() throws Exception
    {
        super("Administrar Tags", 450, 600);
        
        setIconAsResource("/org/fenrir/jcollector/ui/icons/tag_48.png");
    }

    @Override
    protected void loadData() throws BusinessException
    {
    	IContentSearchService searchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);

        List<Tag> tags = searchService.findAllTags();
        addElements(tags);
    }

    @Override
    protected boolean openAddDialog(Map<String, Object> parameters) 
    {
        TagDialog dialog = new TagDialog();
        boolean returnValue = dialog.open()==TagDialog.DIALOG_OK;
        Tag tag = dialog.getFormData();
        parameters.put(PARAMETER_NAME, tag);
        
        return returnValue;
    }
    
    @Override
    protected void doAdd(Map<String, Object> parameters) throws Exception
    {        
    	IContentAdministrationService administrationService = (IContentAdministrationService)ApplicationContext.getInstance().getRegisteredComponent(IContentAdministrationService.class);
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	Tag tag = (Tag)parameters.get(PARAMETER_NAME);
        administrationService.createTag(tag.getName(), tag.isPreferred());
        
        ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_TAG_CREATED);
        event.setNewValue(tag);
        eventNotificationService.notifyEvent(IApplicationEventListener.class, event);
    }

    @Override
    protected boolean openModifyDialog(Map<String, Object> parameters) 
    {
    	Tag selectedTag = (Tag)listEntities.getSelectedValue();
        TagDialog dialog = new TagDialog(selectedTag);
        
        boolean returnValue = dialog.open()==TagDialog.DIALOG_OK;
        Tag tag = dialog.getFormData();
        parameters.put(PARAMETER_NAME, tag);
        
        return returnValue;
    }
    
    @Override
    protected void doModify(Map<String, Object> parameters) throws Exception
    {        
    	IContentAdministrationService administrationService = (IContentAdministrationService)ApplicationContext.getInstance().getRegisteredComponent(IContentAdministrationService.class);
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	Tag oldTag = (Tag)parameters.get(PARAMETER_NAME);
        Tag newTag = administrationService.updateTag(oldTag.getId(), 
        		oldTag.getName(), 
        		oldTag.isPreferred());       
        
        ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_TAG_MODIFIED);
        event.setOldValue(oldTag);
        event.setNewValue(newTag);
        eventNotificationService.notifyEvent(IApplicationEventListener.class, event);
    }

    @Override
    protected void doDelete(Map<String, Object> parameters) throws Exception
    {
    	Tag selectedTag = (Tag)listEntities.getSelectedValue();
    	IContentAdministrationService administrationService = (IContentAdministrationService)ApplicationContext.getInstance().getRegisteredComponent(IContentAdministrationService.class);
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
        administrationService.deleteTag(selectedTag.getId());
        
        ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_TAG_DELETED);
        event.setOldValue(selectedTag);
        eventNotificationService.notifyEvent(IApplicationEventListener.class, event);
    }
}