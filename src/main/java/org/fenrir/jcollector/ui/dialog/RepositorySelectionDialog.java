package org.fenrir.jcollector.ui.dialog;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.jcollector.core.service.IContentSearchService;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.exception.BusinessException;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.1.20110724
 */
@SuppressWarnings("serial")
public class RepositorySelectionDialog extends AbstractConfirmDialog 
{	
	private final Logger log = LoggerFactory.getLogger(RepositorySelectionDialog.class);
	
	private JTextField inputRepository;
	private JList listRepositories;	
	
	private Repository selectedRepository;
	
	public RepositorySelectionDialog(Frame parent)
	{
		super(parent, "Sel.leccionar repositori", new Dimension(450, 600), true);
		search();
	}
	
	public RepositorySelectionDialog(Frame parent, String term)
	{
		this(parent);
		
		inputRepository.setText(term);
		search();
	}
	
	@Override
    protected JComponent createContents()
    {
		JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        
        inputRepository = new JTextField();
        inputRepository.addKeyListener(new KeyAdapter() 
        {
			@Override
			public void keyReleased(KeyEvent event) 
			{
				try{
					search();
				}
				catch(Exception e){
					log.error("Error al buscar coincidències: " + e.getMessage(), e);
					JOptionPane.showMessageDialog(RepositorySelectionDialog.this, "Error al buscar coincidències: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
        });
               
        DefaultListModel listModel = new DefaultListModel();
        listRepositories = new JList(listModel);    	                                        
		JScrollPane scrollTags = new JScrollPane(listRepositories);
		
		/* Grup horitzontal */
        layout.setHorizontalGroup(
        	layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                	.addComponent(inputRepository, GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)                	
                    .addComponent(scrollTags, GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
        	layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(inputRepository, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)                
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollTags, GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                .addContainerGap()
            )
        );
        
        return pContents;
    }

	@Override
	protected boolean onAccept() 
	{				
		if(listRepositories.getSelectedIndex()<0){			
			setMessage("S'ha de sel.leccionar un repositori", MSG_TYPE_ERROR);			
			return false;
		}
		else{
			selectedRepository = (Repository)listRepositories.getSelectedValue();
		}
		return true;
	}	
	
	@Override
	protected boolean onCancel()
	{
		return true;
	}
	
	public Repository getSelectedRepository()
	{
		return selectedRepository;
	}
	
	private void search()
	{
		try{
			IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
			
			DefaultListModel listModel = (DefaultListModel)listRepositories.getModel();
			listModel.removeAllElements();
			
			String term = inputRepository.getText();
			List<Repository> repositories;
			if(StringUtils.isNotBlank(term)){
				repositories = contentSearchService.findRepositoriesNameLike(term);			
			}
			else{			
				repositories = contentSearchService.findAllRepositories();												
			}
			for(Repository repository:repositories){
				listModel.addElement(repository);
			}
		}
		catch(BusinessException e){
			log.error("Error buscant repositoris: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(this, "Error buscant repositoris: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}		
}
