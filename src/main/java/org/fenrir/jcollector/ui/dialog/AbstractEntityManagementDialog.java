package org.fenrir.jcollector.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractDialog;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.2.20130416
 */
public abstract class AbstractEntityManagementDialog<T> extends AbstractDialog
{
    private static final long serialVersionUID = 1L;
    
    private static final int ACTION_MODE_ADD = 1;
    private static final int ACTION_MODE_MODIFY = 2;
    private static final int ACTION_MODE_DELETE = 3;

    private final Logger log = LoggerFactory.getLogger(AbstractEntityManagementDialog.class);

    protected JList listEntities;
    private JButton bModify;
    private JButton bDelete;

    public AbstractEntityManagementDialog(String title, int width, int height) throws Exception
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), title, new Dimension(width, height), true);

        loadData();
    }

    @Override
    @SuppressWarnings("serial")
    protected JComponent createContents()
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        listEntities = new JList(new DefaultListModel());
        listEntities.addListSelectionListener(new ListSelectionListener()
        {
            @Override
            public void valueChanged(ListSelectionEvent event)
            {
                Object object = listEntities.getSelectedValue();
                if(object!=null){
                    bModify.setEnabled(true);
                    bDelete.setEnabled(true);
                }
                else{
                    bModify.setEnabled(false);
                    bDelete.setEnabled(false);
                }
            }
        });
        JScrollPane scrollPane = new JScrollPane(listEntities);
        JButton bAdd = new JButton();
        bAdd.setAction(new AbstractAction("Afegir")
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
                try{
                    Map<String, Object> parameters = new HashMap<String, Object>();
                    if(openAddDialog(parameters)){
                        layerUI.setLocked(true);
                        new ManagementWorker(ACTION_MODE_ADD, parameters).execute();
                    }
                }
                catch(Exception e){
                    log.error("Error al modificar entitat: {}", e.getMessage(), e);
                    JOptionPane.showMessageDialog(AbstractEntityManagementDialog.this, "Error al crear entitat: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        bModify = new JButton();
        bModify.setAction(new AbstractAction("Modificar")
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
                try{
                    Map<String, Object> parameters = new HashMap<String, Object>();
                    if(openModifyDialog(parameters)){
                        layerUI.setLocked(true);
                        new ManagementWorker(ACTION_MODE_MODIFY, parameters).execute();
                    }
                }
                catch(Exception e){
                    log.error("Error al modificar entitat: {}", e.getMessage(), e);
                    JOptionPane.showMessageDialog(AbstractEntityManagementDialog.this, "Error al modificar entitat: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        bDelete = new JButton();
        bDelete.setAction(new AbstractAction("Eliminar")
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
                try{
                    Map<String, Object> parameters = new HashMap<String, Object>();
                    if(openDeleteDialog(parameters)){
                        layerUI.setLocked(true);
                        new ManagementWorker(ACTION_MODE_DELETE, parameters).execute();
                    }
                }
                catch(Exception e){
                    log.error("Error al eliminar entitat: {}", e.getMessage(), e);
                    JOptionPane.showMessageDialog(AbstractEntityManagementDialog.this, "Error al eliminat entitat: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        bModify.setEnabled(false);
        bDelete.setEnabled(false);

        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 248, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(bDelete, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bModify, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bAdd, GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(bAdd)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bModify)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bDelete)
                    )
                    .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                )
                .addContainerGap()
            )
        );

        return pContents;
    }

    @Override
    protected boolean onClose()
    {
        return true;
    }

    protected abstract void loadData() throws Exception;
    protected abstract boolean openAddDialog(Map<String, Object> parameters);
    protected abstract void doAdd(Map<String, Object> parameters) throws Exception;
    protected abstract boolean openModifyDialog(Map<String, Object> parameters);
    protected abstract void doModify(Map<String, Object> parameters) throws Exception;
    protected boolean openDeleteDialog(Map<String, Object> parameters)
    {
        return JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(AbstractEntityManagementDialog.this, 
                "Voleu eliminar el registre sel.leccionat?", 
                "Confirmar eliminació", 
                JOptionPane.OK_CANCEL_OPTION, 
                JOptionPane.QUESTION_MESSAGE);       
    }
    protected abstract void doDelete(Map<String, Object> parameters) throws Exception;

    protected void addElements(List<T> elements)
    {
        DefaultListModel model = (DefaultListModel)listEntities.getModel();
        model.removeAllElements();
        for(T elem:elements){
            model.addElement(elem);
        }
    }
    
    private class ManagementWorker extends SwingWorker<Void, String>
    {
        private int mode;
        private Map<String, Object> parameters;
        
        public ManagementWorker(int mode, Map<String, Object> parameters)
        {
            this.mode = mode;
            this.parameters = parameters;
        }
        
        @Override
        protected Void doInBackground() throws Exception 
        {
            if(mode==ACTION_MODE_ADD){
                doAdd(parameters);
            }
            else if(mode==ACTION_MODE_MODIFY){
                doModify(parameters);
            }
            else if(mode==ACTION_MODE_DELETE){
                doDelete(parameters);
            }
            loadData();
            
            return null;
        } 

        @Override
        protected void done() 
        {
            layerUI.setLocked(false);
        }        
    }
}