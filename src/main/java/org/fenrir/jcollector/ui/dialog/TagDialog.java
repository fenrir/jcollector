package org.fenrir.jcollector.ui.dialog;

import java.awt.Dimension;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import org.apache.commons.lang.StringUtils;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.jcollector.core.entity.Tag;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.2.20130416
 */
public class TagDialog extends AbstractConfirmDialog 
{
	private static final long serialVersionUID = 1L; 
	
	/* Components del formulari */
    private JTextField inputName;
    private JCheckBox checkPreferred;

    // Valor de retorn
    private Tag tag;
    // Valor original en cas de modificació
    private Tag originalTag;
    
    public TagDialog()
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), "Crear tag", new Dimension(530, 220), true);

//      setIconAsResource("data/icons/tag_add_48.png");        
    }
    
    public TagDialog(Tag tag)
    {
    	super(ApplicationWindowManager.getInstance().getMainWindow(), "Modificar tag", new Dimension(530, 220), true);

//    	setIconAsResource("data/icons/tag_48.png");
    	
    	this.originalTag = tag;
    	inputName.setText(tag.getName());
    	checkPreferred.setSelected(tag.isPreferred());    	
    }

    @Override
    protected JComponent createContents()
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        /* Definició dels components */
        JLabel lName = new JLabel("Nom:", SwingConstants.RIGHT);
        inputName = new JTextField();                
        checkPreferred = new JCheckBox("Favorit");
        checkPreferred.setHorizontalAlignment(JCheckBox.RIGHT);

        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lName, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(checkPreferred)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    )
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(inputName, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)                        
                    )
                )
                .addGap(17, 17, 17)
            )
        );

        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lName, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                    .addComponent(inputName, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)                    
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(checkPreferred)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            )
        );

        return pContents;
    }

    @Override
    protected boolean onAccept()
    {
    	String strName = inputName.getText().trim();
        if(StringUtils.isNotBlank(strName)){
            // S'especifica el valor de retorn
            tag = new Tag();
            // En cas de moficiació també es retorna la ID
            if(originalTag!=null){
            	tag.setId(originalTag.getId());
            }
            tag.setName(strName);
            tag.setPreferred(checkPreferred.isSelected());

            return true;
        }
        else{
            setMessage("El camp NOM no pot ser buit", MSG_TYPE_ERROR);
            
            return false;
        }
    }

    @Override
    protected boolean onCancel()
    {
    	tag = null;
    	
        return true;
    }
    
    public Tag getFormData()
    {
    	return tag;
    }
}
