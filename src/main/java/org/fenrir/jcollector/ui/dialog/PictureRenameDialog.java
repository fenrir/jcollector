package org.fenrir.jcollector.ui.dialog;

import java.awt.Dimension;
import java.awt.Frame;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.jcollector.core.service.IContentSearchService;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.util.CommonUtils;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.4.20140530
 */
@SuppressWarnings("serial")
public class PictureRenameDialog extends AbstractConfirmDialog 
{
	private final Logger log = LoggerFactory.getLogger(PictureRenameDialog.class);
	
	private JTextField inputName;
	private JList list;
	
	private String pictureName;
	
	public PictureRenameDialog(Frame parent, Picture picture, String newName)
	{
		super(parent, "Renombrar imatge", new Dimension(450, 500), true);
				        
        inputName.setText(newName);
        try{
        	loadMatches(picture);
        }
        catch(Exception e){
        	log.error("Error al obtenir coincidencies del grup: " + e.getMessage(), e);
        	JOptionPane.showMessageDialog(PictureRenameDialog.this, "Error al obtenir coincidencies del grup: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }        
	}
	
	protected JComponent createContents()
	{
		JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        JLabel label = new JLabel("Registres del grup");
        inputName = new JTextField();
        list = new JList();	
        list.setModel(new DefaultListModel());
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(list);        
        
        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(label, GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                    .addComponent(inputName, GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE)                    
                    .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)                
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(label)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane)
                .addContainerGap()
            )
        );
        
        return pContents;
	}		
	
	@Override
	protected boolean onAccept()
	{
		if(StringUtils.isBlank(inputName.getText())){
			setMessage("Ha d'indicar un nom per la imatge", MSG_TYPE_ERROR);			
			return false;
		}
		pictureName = inputName.getText();
		
		return true;
	}
	
	@Override
	protected boolean onCancel()
	{
		return true;
	}
	
	public String getPictureName()
	{
		return pictureName;
	}
	
	private void loadMatches(Picture picture) throws BusinessException
	{
		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
		
		DefaultListModel listModel = (DefaultListModel)list.getModel();
		List<Picture> vPictures = contentSearchService.findPicturesByGroup(picture.getPictureGroup().getId(), true);		
		Collections.sort(vPictures, new Comparator<Picture>() 
		{
			public int compare(Picture p1, Picture p2)
			{
				String name1 = CommonUtils.getPictureBasename(p1);
				String name2 = CommonUtils.getPictureBasename(p2);
				
				return name1.compareTo(name2);
			}
		});
		for(Picture elem:vPictures){
			String pictureName = CommonUtils.getPictureBasename(elem);
			listModel.addElement(pictureName);
		}
	}
}
