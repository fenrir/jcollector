/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fenrir.jcollector.ui.widget;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.ext.LockableUI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.2.20130414
 */
public class InProcessLayerUI extends LockableUI
{
    private final Logger log = LoggerFactory.getLogger(InProcessLayerUI.class);
    
    protected String inProcessMessage;    
    // Una vegada carregada la imatge es guarda per no haver de fer-ho més mentres es mostra la finestra
    private Image inProcessImage;                

    @Override  
    protected void paintLayer(Graphics2D g2, JXLayer layer) 
    {
        // Es pinta la capa tal qual és
        super.paintLayer(g2, layer);    
        // Es pinta l'efecte de "en Procés"
        if(isLocked()){
            // Color de la capa
            g2.setColor(new Color(0, 0, 0, 100));
            g2.fillRect(0, 0, layer.getWidth(), layer.getHeight());
            // Image del rellotge
            if(inProcessImage==null){
                try{
                    inProcessImage = ImageIO.read(getClass().getResource("/org/fenrir/dragonfly/ui/icons/in_process_96.png"));
                }
                catch(IOException e){
                    log.error("No s'a pogut carregar la imatge in_process_96.png: {}", e.getMessage(), e);
                }
            }
            if(inProcessImage!=null){
                int posX = layer.getWidth() / 2 - 48;
                int posY = layer.getHeight() / 2 - 48;
                g2.drawImage(inProcessImage, posX, posY, 96, 96, layer);
            }
            // Missatge
        }
    }

    @Override
    protected void processMouseEvent(MouseEvent event, JXLayer<? extends JComponent> layer) 
    {
        if(isLocked()){
            event.consume();
        }
        else{
            super.processMouseEvent(event, layer);
        }
    }

    @Override
    protected void processKeyEvent(KeyEvent event, JXLayer<? extends JComponent> layer) 
    {
        if(isLocked()){
            event.consume();
        }
        else{
            super.processKeyEvent(event, layer);
        }        
    }
    
    public void setMessage(String inProcessMessage)
    {
        this.inProcessMessage = inProcessMessage;
    }
}
