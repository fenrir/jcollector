package org.fenrir.jcollector.ui.widget;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import org.fenrir.jcollector.ui.widget.PictureGroupSearchField.AbstractPictureGroupSearchModel;
import org.fenrir.jcollector.core.entity.Collection;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20120811
 */
public class PictureGroupSearchComboBoxModel extends AbstractPictureGroupSearchModel implements ActionListener
{
	private DefaultComboBoxModel comboBoxModel;

	public PictureGroupSearchComboBoxModel(DefaultComboBoxModel comboBoxModel)
	{
		this.comboBoxModel = comboBoxModel;
	}

	@Override
	public Collection getSelectedCollection()
	{
		return (Collection)comboBoxModel.getSelectedItem();
	}

	@Override
	public void setSelectedCollection(Collection collection)
	{
		comboBoxModel.setSelectedItem(collection);
	}

	@Override
	public void actionPerformed(ActionEvent event)
	{
		JComboBox comboBox = (JComboBox)event.getSource();
		Collection collection = (Collection)comboBox.getSelectedItem();
		fireSelectedCollectionChanged(collection);
	}
}
