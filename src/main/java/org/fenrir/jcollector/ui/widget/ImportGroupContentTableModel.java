package org.fenrir.jcollector.ui.widget;

import javax.swing.table.AbstractTableModel;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.fenrir.jcollector.core.entity.Repository;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.1.20111121
 */
public class ImportGroupContentTableModel extends AbstractTableModel
{
	private static final long serialVersionUID = 1L;
	
	public static final int COLUMN_SELECTOR = 0;
	public static final int COLUMN_FILE = 1;
	public static final int COLUMN_DATE = 2;
	public static final int COLUMN_REPOSITORY = 3;
	public static final int COLUMN_MESSAGE = 4;
	
	private String[] columnNames = new String[]{"", "Fitxer", "Data", "Repositori", "Info"}; 
	private List<ModelData> data;	
	
	public ImportGroupContentTableModel()
	{
		data = new ArrayList<ModelData>();
	}
	
	public ImportGroupContentTableModel(List<ModelData> data)
	{
		this.data = data;
	}
	
	@Override
	public String getColumnName(int col) 
	{
        return columnNames[col];
    }
	
	@Override
	public int getColumnCount() 
	{		
		return columnNames.length;
	}

	@Override
	public int getRowCount() 
	{
		return data.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) 
	{
		if(columnIndex==COLUMN_SELECTOR){
			return data.get(rowIndex).isChecked();
		}
		else if(columnIndex==COLUMN_FILE){
			return data.get(rowIndex).getFilename();
		}		
		else if(columnIndex==COLUMN_DATE){
			return data.get(rowIndex).getDate();
		}
		else if(columnIndex==COLUMN_REPOSITORY){
			Repository repository = data.get(rowIndex).getRepository();
			if(repository!=null){
				return repository.getName();
			}
			else{
				return "";
			}
		}
		else{
			return data.get(rowIndex).getMessage();
		}
	}
	
	/**
	 * Es necessari implementar aquest mètode perquè les dades de la taula poden canviar 
     */
    public void setValueAt(Object value, int row, int col) 
	{
		if(col==COLUMN_SELECTOR){
			data.get(row).setChecked((Boolean)value);
		}
		else if(col==COLUMN_FILE){
			data.get(row).setFilename((String)value);
		}
		// TODO Comprobar format de la data
		else if(col==COLUMN_DATE){			
			data.get(row).setDate((String)value);
		}
		else if(col==COLUMN_REPOSITORY){
			data.get(row).setRepository((Repository)value);
		}		
				
        fireTableCellUpdated(row, col);
    }
    
    @Override
	public boolean isCellEditable(int row, int column) 
	{
    	// El check només serà editable si la imatge no ha sigut ja importada abans
    	if(column==0){
    		if(data.get(row).isImported()){
    			return false;
    		}
    		
    		return true;
    	}
    	
    	// La columna missatge no és editable. La resta si.
		return column!=4;
	}
		
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Class getColumnClass(int column) 
	{
		if(column==0){
			return Boolean.class;
		}
		else{
			return String.class;
		}		
	}
	
	public List<ModelData> getData()
	{
		return data;
	}
	
	public void addData(ModelData data)
	{
		this.data.add(data);
		fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
	}
	
	public void clearData()
	{
		data.clear();
		fireTableDataChanged();
	}
	
	public void selectAll()
	{
		for(ModelData elem:data){
			if(!elem.isImported()){
				elem.setChecked(true);
			}
		}
		fireTableDataChanged();
	}
	
	public void selectNone()
	{
		for(ModelData elem:data){
			elem.setChecked(false);
		}
		fireTableDataChanged();
	}
	
	public static class ModelData
	{
		private Boolean checked;
		private File file;
		private String filename;
		private String date;
		private Repository repository;
		private Boolean imported;
		private Boolean error;
		private String message;
		
		public ModelData(File file)
		{
			this.file = file;
			this.filename = file.getName();
			this.date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
			this.checked = true;
			this.imported = false;
			this.error = false;		
		}

		public Boolean isChecked() 
		{
			return checked;
		}

		/**
		 * Indica si el registre corresponent de la taula serà importat o no. 
		 * Especificant null es deshabilita l'edició del camp
		 * @param checked Boolean - 
		 */
		public void setChecked(Boolean checked) 
		{
			this.checked = checked;
		}
		
		public File getFile() 
		{
			return file;
		}
		
		public String getFilename()
		{
			return filename;
		}
		
		public void setFilename(String filename)
		{
			this.filename = filename;
		}

		public String getDate()
		{
			return date;
		}
		
		public void setDate(String date)
		{
			this.date = date;
		}
		
		public Repository getRepository()
		{
			return repository;
		}
		
		public void setRepository(Repository repository)
		{
			this.repository = repository;
		}
		
		public Boolean isImported()
		{
			return imported;
		}
		
		public void setImported(Boolean imported)
		{
			this.checked = !imported;
			this.imported = imported;
		}
		
		public String getMessage()
		{
			return message;
		}	
		
		public void setInfoMessage(String message)
		{
			this.message = message;
			this.error = false;
		}
		
		public void setErrorMessage(String message)
		{
			this.message = message;
			this.error = true;
		}
		
		public void clearMessage()
		{
			message = null;
			error = false;
		}
		
		public Boolean hasError()
		{
			return error;
		}

		@Override
		public int hashCode() 
		{
			final int prime = 31;
			
			int result = 1;
			result = prime * result + ((file == null) ? 0 : file.hashCode());
			
			return result;
		}

		@Override
		public boolean equals(Object obj) 
		{
			if(this==obj){
				return true;
			}
			if (obj==null){
				return false;
			}
			if(!(obj instanceof ModelData)){
				return false;
			}
			
			ModelData other = (ModelData)obj;
			if(file==null){
				if(other.file!=null){
					return false;
				}
			} 
			else if(!file.equals(other.file)){
				return false;
			}
			
			return true;
		}					
	}
}
