package org.fenrir.jcollector.ui.widget;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import org.apache.commons.lang.StringUtils;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.widget.AdvancedTextField;
import org.fenrir.jcollector.ui.dialog.GroupSelectionDialog;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.service.IContentSearchService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140530
 */
@SuppressWarnings("serial")
public class PictureGroupSearchField extends AdvancedTextField
{
	private AbstractPictureGroupSearchModel model;

	public PictureGroupSearchField()
	{
		this(new DefaultPictureGroupSearchModel());
	}

	public PictureGroupSearchField(AbstractPictureGroupSearchModel fieldModel)
	{
		super("data/icons/search_16.png");

		setModel(fieldModel);

		// Inicialment el color del text indicarà que no s'ha sel.leccionat cap repositori
		inputText.setForeground(Color.RED);

		addKeyListener(new KeyAdapter()
    	{
			@Override
			public void keyPressed(KeyEvent event)
			{
				int keyPressed = event.getKeyCode();
        		if(keyPressed==KeyEvent.VK_ENTER){
        			openGroupSelection(model.getSelectedCollection(), getText());
        		}
        		else if(keyPressed!=KeyEvent.VK_TAB){
					// En el moment que s'insereix text, el repositori sel.lecciónat queda invalidat
        			if(isFieldValid()){
        				setSelectedGroup(null);
        			}
				}
			}
    	});

		addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				openGroupSelection(model.getSelectedCollection(), getText());
			}
		});
	}

	public AbstractPictureGroupSearchModel getModel()
	{
		return model;
	}

	public void setModel(AbstractPictureGroupSearchModel model)
	{
		this.model = model;
		model.setGroupSearchField(this);
	}

	public Collection getSelectedCollection()
	{
		return model.getSelectedCollection();
	}

	public void setSelectedCollection(Collection selectedCollection)
	{
		model.setSelectedCollection(selectedCollection);
		// Reset del grup si canvia la col.lecció
		if(selectedCollection==null
				|| model.getSelectedGroup()!=null && !model.getSelectedGroup().getCollection().equals(selectedCollection.getId())){
			setSelectedGroup(null);
			setText(null);
			setToolTipText(null);
		}
	}

	public PictureGroup getSelectedGroup()
	{
		return model.getSelectedGroup();
	}

	public void setSelectedGroup(PictureGroup selectedGroup)
	{
		model.setSelectedGroup(selectedGroup);
		updateFieldUI();
	}

	public boolean isFieldValid()
	{
		return model.getSelectedGroup()!=null;
	}

	private void updateFieldUI()
	{
		if(model.getSelectedGroup()!=null){
			setText(model.getSelectedGroup().getName());
			setToolTipText(model.getSelectedGroup().getName());
			inputText.setForeground(Color.BLACK);
		}
		else{
			inputText.setForeground(Color.RED);
		}
	}

	private void openGroupSelection(Collection selectedCollection, String text)
	{
		GroupSelectionDialog dialog = null;
		if(StringUtils.isNotBlank(text) && selectedCollection!=null){
			dialog = new GroupSelectionDialog(ApplicationWindowManager.getInstance().getMainWindow(), selectedCollection, text);
		}
		else if(StringUtils.isNotBlank(text)){
			dialog = new GroupSelectionDialog(ApplicationWindowManager.getInstance().getMainWindow(), text);
		}
		else{
			dialog = new GroupSelectionDialog(ApplicationWindowManager.getInstance().getMainWindow());
		}
		if(dialog.open()==GroupSelectionDialog.DIALOG_OK){
			// S'indica que hi ha repositori sel.leccionat
			inputText.setForeground(Color.BLACK);
			PictureGroup selectedGroup = dialog.getSelectedGroup();
			model.setSelectedGroup(selectedGroup);
			IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
			Collection collection = contentSearchService.findCollectionById(selectedGroup.getCollection().getId());
			model.setSelectedCollection(collection);
			// S'actualitzen els camps
			setText(selectedGroup.getName());
			setToolTipText(selectedGroup.getName());
		}
	}

	public static abstract class AbstractPictureGroupSearchModel
	{
		private PictureGroupSearchField field;
		protected PictureGroup selectedGroup;

		private void setGroupSearchField(PictureGroupSearchField field)
		{
			this.field = field;
		}

		public abstract Collection getSelectedCollection();
		public abstract void setSelectedCollection(Collection collection);

		public PictureGroup getSelectedGroup()
		{
			return selectedGroup;
		}

		public void setSelectedGroup(PictureGroup selectedGroup)
		{
			this.selectedGroup = selectedGroup;
		}

		public void fireSelectedCollectionChanged(Collection collection)
		{
			field.setSelectedCollection(collection);
		}
	}

	public static class DefaultPictureGroupSearchModel extends AbstractPictureGroupSearchModel
	{
		private Collection selectedCollection;

		@Override
		public Collection getSelectedCollection()
		{
			return selectedCollection;
		}

		@Override
		public void setSelectedCollection(Collection selectedCollection)
		{
			this.selectedCollection = selectedCollection;
		}
	}
}
