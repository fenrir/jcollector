package org.fenrir.jcollector.ui.widget;

import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import org.pushingpixels.substance.api.renderers.SubstanceDefaultListCellRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.service.IContentSearchService;
import org.fenrir.yggdrasil.core.ApplicationContext;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140311
 */
public class RepositoryListCellRenderer extends	SubstanceDefaultListCellRenderer
{
	private static final long serialVersionUID = -5750715872987292003L;
	
	private final Logger log = LoggerFactory.getLogger(RepositoryListCellRenderer.class);
	
	public RepositoryListCellRenderer()
	{
		 setOpaque(true);
	}
	
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) 
	{
		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
		
		long picturesCount = 0;
		try{
			picturesCount = contentSearchService.countOutstandingPicturesByRepository(((Repository)value).getId());
		}
		catch(Exception e){
			log.error("Error al recuperar dades del repositori {}: {}", new Object[]{((Repository)value).getId(), e.getMessage(), e});
		}

		/* Com que la classe SubstanceDefaultListCellRenderer exten de DefaultListCellRenderer i aquesta de JLabel, 
		 * es pot posar directament el text i la icona 
		 */
		JLabel label = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		label.setText(value.toString() + " (" + picturesCount + ")");
		label.setIcon(new ImageIcon("data/icons/repository_16.png"));

        return label;
	}
}
