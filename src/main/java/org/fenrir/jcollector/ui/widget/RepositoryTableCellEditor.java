package org.fenrir.jcollector.ui.widget;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;
import javax.swing.table.TableCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.ui.dialog.GroupSelectionDialog;
import org.fenrir.jcollector.ui.dialog.RepositorySelectionDialog;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20110804
 */
@SuppressWarnings("serial")
public class RepositoryTableCellEditor extends AbstractCellEditor implements TableCellEditor 
{
	private final Logger log = LoggerFactory.getLogger(RepositoryTableCellEditor.class);
	
	private JTextField input;
	
	private Repository repository;

    public RepositoryTableCellEditor() 
    {    	    	
    	input = new JTextField();
    	input.setBorder(BorderFactory.createLineBorder(Color.BLACK));

    	input.addKeyListener(new KeyAdapter() 
    	{
			@Override
			public void keyPressed(KeyEvent event) 
			{
				int keyPressed = event.getKeyCode();
        		if(keyPressed==KeyEvent.VK_ENTER){
        			RepositorySelectionDialog dialog;
        			String strInput = input.getText();
        			try{
        				if(StringUtils.isNotBlank(strInput)){
        					dialog = new RepositorySelectionDialog(null, strInput);
        				}
        				else{
        					dialog = new RepositorySelectionDialog(null);
        				}
        				if(dialog.open()==GroupSelectionDialog.DIALOG_OK){
        					repository = dialog.getSelectedRepository();        					        				
        				}
        				else{
        					repository = null;
        				}
        				fireEditingStopped();
        			}
        			catch(Exception e){
        				log.error("Error al obrir finestra de cerca: " + e.getMessage(), e);
        				JOptionPane.showMessageDialog(null, "Error al obrir la finestra de cerca", "Error", JOptionPane.ERROR_MESSAGE);        				
        			}
        		}
			}    		
		});    	        
    }

    //Implement the one CellEditor method that AbstractCellEditor doesn't.
    @Override
    public Object getCellEditorValue() 
    {
    	return repository;
    }

    //Implement the one method defined by TableCellEditor.
    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) 
    {
        input.setText((String)value);
        return input;
    }

    @Override
    public boolean isCellEditable(EventObject evt) 
    {
        if(evt instanceof MouseEvent){
            return ((MouseEvent)evt).getClickCount() >= 2;
        }
        return true;
    }
}

