package org.fenrir.jcollector.ui.widget;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import org.apache.commons.lang.StringUtils;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.widget.AdvancedTextField;
import org.fenrir.jcollector.ui.dialog.GroupSelectionDialog;
import org.fenrir.jcollector.ui.dialog.RepositorySelectionDialog;
import org.fenrir.jcollector.core.entity.Repository;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20120905
 */
@SuppressWarnings("serial")
public class RepositorySearchField extends AdvancedTextField
{
	private Repository selectedRepository;

	public RepositorySearchField()
	{
		super("data/icons/search_16.png");

		// Inicialment el color del text indicarà que no s'ha sel.leccionat cap repositori
		inputText.setForeground(Color.RED);

		addKeyListener(new KeyAdapter()
    	{
			@Override
			public void keyPressed(KeyEvent event)
			{
				int keyPressed = event.getKeyCode();
        		if(keyPressed==KeyEvent.VK_ENTER){
        			openRepositorySelection(getText());
        		}
        		else if(keyPressed!=KeyEvent.VK_TAB){
					// En el moment que s'insereix text, el repositori sel.lecciónat queda invalidat
					selectedRepository = null;
					inputText.setForeground(Color.RED);
				}
			}
    	});

		addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				openRepositorySelection(getText());
			}
		});
	}

	public Repository getSelectedRepository()
	{
		return selectedRepository;
	}

	public void setSelectedRepository(Repository selectedRepository)
	{
		this.selectedRepository = selectedRepository;
		if(selectedRepository!=null){
			setText(selectedRepository.getName());
			setToolTipText(selectedRepository.getName());
			inputText.setForeground(Color.BLACK);
		}
		else{
			setText(null);
			setToolTipText(null);
			inputText.setForeground(Color.RED);
		}
	}

	public boolean isFieldValid()
	{
		return selectedRepository!=null;
	}

	private void openRepositorySelection(String text)
	{
		RepositorySelectionDialog dialog;
		if(StringUtils.isNotBlank(text)){
			dialog = new RepositorySelectionDialog(ApplicationWindowManager.getInstance().getMainWindow(), text);
		}
		else{
			dialog = new RepositorySelectionDialog(null);
		}
		if(dialog.open()==GroupSelectionDialog.DIALOG_OK){
			selectedRepository = dialog.getSelectedRepository();
			// S'indica que hi ha repositori sel.leccionat
			inputText.setForeground(Color.BLACK);

			setText(selectedRepository.getName());
			setToolTipText(selectedRepository.getName());
		}
	}
}
