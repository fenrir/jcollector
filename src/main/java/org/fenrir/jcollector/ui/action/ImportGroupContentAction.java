package org.fenrir.jcollector.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.fenrir.jcollector.ui.dialog.ImportGroupContentDialog;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.1.20110821
 */
@SuppressWarnings("serial")
public class ImportGroupContentAction extends AbstractAction 
{	
	public ImportGroupContentAction()
	{
		super("Importar contingut grup");
	}
	
	@Override
	public void actionPerformed(ActionEvent event) 
	{		
		ImportGroupContentDialog dialog = new ImportGroupContentDialog();
		dialog.open();							
	}	
}
