package org.fenrir.jcollector.ui.action;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.WizardDialog;
import org.fenrir.jcollector.ui.model.ApplicationModel;
import org.fenrir.jcollector.ui.wizard.CollectionManagementWizard;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.4.20140601
 */
@SuppressWarnings("serial")
public class CollectionManagementAction extends AbstractAction
{
	public static final int MODE_CREATE = CollectionManagementWizard.MODE_CREATE;
	public static final int MODE_MODIFY = CollectionManagementWizard.MODE_MODIFY;
	public static final int MODE_DELETE = CollectionManagementWizard.MODE_DELETE;
	
	private Logger log = LoggerFactory.getLogger(CollectionManagementAction.class);
	
	private int mode = 0;
	
	public CollectionManagementAction(int mode)
	{
		if(mode==MODE_CREATE){
			putValue(AbstractAction.NAME, "Crear col.lecció");
		}		
		else if(mode==MODE_MODIFY){
			putValue(AbstractAction.NAME, "Modificar col.lecció");
		}
		else if(mode==MODE_DELETE){
			putValue(AbstractAction.NAME, "Eliminar col.lecció");
		}
		else{
			throw new IllegalArgumentException("Mode no vàlid per l'acció de gestió de col.leccions");
		}
		
		this.mode = mode;
	}
			
	@Override
	public void actionPerformed(ActionEvent event) 
	{				
		try{	
			String wizardTitle = null;
			CollectionManagementWizard wizard = null;
			ApplicationModel model = (ApplicationModel)ApplicationContext.getInstance().getRegisteredComponent(ApplicationModel.class);
			if(mode==MODE_CREATE){
				wizardTitle = "Crear col.lecció";				
				wizard = new CollectionManagementWizard(mode);
			}
			else if(mode==MODE_MODIFY){
				wizardTitle = "Modificar col.lecció";
				wizard = new CollectionManagementWizard(model.getSelectedCollection(), mode);
			}
			else{
				wizardTitle = "Eliminar col.lecció";
				wizard = new CollectionManagementWizard(model.getSelectedCollection(), mode);
			}
			new WizardDialog<CollectionManagementWizard>(ApplicationWindowManager.getInstance().getMainWindow(), wizardTitle, new Dimension(500, 250), wizard).open();
		}
		catch(Exception e){
			log.error("Error al crear Wizard de gestió de col.leccions: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al crear wizard de gestió de col.leccions: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} 
	}
}
