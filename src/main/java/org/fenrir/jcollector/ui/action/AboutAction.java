package org.fenrir.jcollector.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.fenrir.jcollector.ui.dialog.AboutDialog;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.2.20130416
 */
public class AboutAction extends AbstractAction
{
    private static final long serialVersionUID = 1L;

    public AboutAction()
    {
        super("Informació sobre l'aplicació");
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
        AboutDialog dialog = new AboutDialog(ApplicationWindowManager.getInstance().getMainWindow());
        dialog.open();        
    }
}
