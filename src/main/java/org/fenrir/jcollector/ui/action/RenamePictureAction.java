package org.fenrir.jcollector.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.jcollector.ui.model.ApplicationModel;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.event.ApplicationEvent;
import org.fenrir.jcollector.core.event.IApplicationEventListener;
import org.fenrir.jcollector.core.service.IContentAdministrationService;
import org.fenrir.jcollector.util.CommonUtils;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.4.20140601
 */
@SuppressWarnings("serial")
public class RenamePictureAction extends AbstractAction implements PropertyChangeListener 
{
	private final Logger log = LoggerFactory.getLogger(RenamePictureAction.class);
	
	private IContentAdministrationService contentAdministrationService;
	private IEventNotificationService eventNotificationService;
	
	private ApplicationModel model;	
	
	public RenamePictureAction(ApplicationModel model)
	{
		super();
		putValue(LARGE_ICON_KEY, new ImageIcon("data/icons/rename_16.png"));
		
		this.model = model;
		
		// Per defecte l'acció estará deshabilitada
		setEnabled(false);
		model.addPropertyChangeListener(this);
	}
	
	public void setContentAdministrationService(IContentAdministrationService contentAdministrationService)
	{
		this.contentAdministrationService = contentAdministrationService;
	}
	
	public void setEventNotificationService(IEventNotificationService eventNotificationService)
	{
		this.eventNotificationService = eventNotificationService;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		Picture picture = model.getSelectedPicture();
		
		// Si hi ha una imatge sel.leccionada, es procedeix a renombrar
		if(picture!=null){
			String newName = JOptionPane.showInputDialog(ApplicationWindowManager.getInstance().getMainWindow(), 
					"Renombrar imatge", 
					CommonUtils.getPictureBasename(picture));
			if(newName!=null && newName.trim().length()==0){
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Ha d'introduir un nom vàlid", "Advertencia", JOptionPane.WARNING_MESSAGE);
			}
			else if(StringUtils.isNotBlank(newName)){
				try{
					String extension = StringUtils.substringAfterLast(picture.getRelativePath(), ".");
					String relativePath = null;
					if(StringUtils.contains(picture.getRelativePath(), File.separator)){
						relativePath = StringUtils.substringBeforeLast(picture.getRelativePath(), File.separator) + File.separator + newName;
					}
					else{
						relativePath = newName;
					}
					if(StringUtils.isNotBlank(extension)){
						relativePath += "." + extension;
					}					
					
					Picture newPicture = new Picture(picture);
					newPicture.setRelativePath(relativePath);
					
					String destPath = CommonUtils.getPictureAbsolutePath(newPicture);
					if(new File(destPath).exists()){
						log.error("El nom introduit ja existeix");				
						JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "El nom introduit ja existeix", "Advertència", JOptionPane.WARNING_MESSAGE);
						
						return;
					}
					
					String srcPath = CommonUtils.getPictureAbsolutePath(picture);
					FileUtils.moveFile(new File(srcPath), new File(destPath));
					
					contentAdministrationService.updatePicture(picture.getId(), 
							newPicture.getCollection()!=null ? newPicture.getCollection().getId() : null, 
							newPicture.getRepository()!=null ? newPicture.getRepository().getId() : null, 
							newPicture.getPictureGroup()!=null ? newPicture.getPictureGroup().getId() : null, 
							newPicture.getRelativePath(), 
							newPicture.isCollected(), 
							newPicture.getCollectionDate());
									
					// Es llança event de refresc per les vistes
					ApplicationEvent appEvent = new ApplicationEvent(ApplicationEvent.EVENT_PICTURE_MODIFIED);
					appEvent.setOldValue(picture);
					appEvent.setNewValue(newPicture);
					eventNotificationService.notifyEvent(IApplicationEventListener.class, appEvent);	
				}
				catch(IOException e){
					log.error("Error al renombrar imatge: " + e.getMessage(), e);				
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al renombrar imatge: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
				catch(Exception e){
					log.error("Error al renombrar imatge: " + e.getMessage(), e);				
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al renombrar imatge: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) 
	{	
		if(ApplicationModel.PROPERTY_SELECTED_PICTURE.equals(event.getPropertyName())){
			// Si no hi ha cap picture sel.leccionada es deshabilita s'acció
			if(event.getNewValue()==null){
				setEnabled(false);
			}
			else{
				setEnabled(true);
			}
		}
	}
}
