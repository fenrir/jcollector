package org.fenrir.jcollector.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.jcollector.ui.dialog.TagManagementDialog;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.4.20140610
 */
public class TagManagementAction extends AbstractAction 
{
    private static final long serialVersionUID = 1L;
	
    private final Logger log = LoggerFactory.getLogger(TagManagementAction.class);
	
    public TagManagementAction()
    {
        super("Administrar Tags");
    }
	
    @Override
    public void actionPerformed(ActionEvent event) 
    {
        try{
            TagManagementDialog dialog = new TagManagementDialog();
            dialog.open();
        }
        catch(Exception e){
            log.error("Error al obrir finestra de gestió de tags: {}", e.getMessage(), e);
        }
    }
}
