package org.fenrir.jcollector.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.TaskProgressDialog;
import org.fenrir.jcollector.ui.worker.RegenerateThumbnailsWorker;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.4.20140522
 */
@SuppressWarnings("serial")
public class RegenerateThumbnailsAction extends AbstractAction 
{
	private Logger log = LoggerFactory.getLogger(RegenerateThumbnailsAction.class);
	
	public RegenerateThumbnailsAction()
	{
		super("Regenerar miniatures");
	}
	
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		try{
			RegenerateThumbnailsWorker task = new RegenerateThumbnailsWorker();
			TaskProgressDialog dialog = new TaskProgressDialog("Regenerar miniatures", task);
			dialog.open();
		}
		catch(Exception e){
			log.error("Error al regenerar les miniatures de les imatges: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),"Error al regenerar les miniatures de les imatges: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}
