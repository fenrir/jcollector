package org.fenrir.jcollector.ui.action;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.WizardDialog;
import org.fenrir.jcollector.ui.model.ApplicationModel;
import org.fenrir.jcollector.ui.wizard.ImportRepositoryContentWizard;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.1.20110821
 */
@SuppressWarnings("serial")
public class ImportRepositoryContentAction extends AbstractAction 
{	
	private Logger log = LoggerFactory.getLogger(ImportRepositoryContentAction.class);
	
	public ImportRepositoryContentAction()
	{
		super("Importar contingut repositori");
	}
	
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		try{
			String wizardTitle = "Importació de contingut";
			ApplicationModel model = (ApplicationModel)ApplicationContext.getInstance().getRegisteredComponent(ApplicationModel.class);
			ImportRepositoryContentWizard wizard = new ImportRepositoryContentWizard(model.getSelectedRepository());			
			new WizardDialog<ImportRepositoryContentWizard>(ApplicationWindowManager.getInstance().getMainWindow(), wizardTitle, new Dimension(450, 500), wizard).open();						
		}
		catch(Exception e){
			log.error("Error al crear wl wizard d'importació de contingut: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al crear el wizard d'importació de contingut: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}	
}
