package org.fenrir.jcollector.ui.action;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.WizardDialog;
import org.fenrir.jcollector.ui.model.ApplicationModel;
import org.fenrir.jcollector.ui.wizard.RepositoryManagementWizard;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.1.20110924
 */
@SuppressWarnings("serial")
public class RepositoryManagementAction extends AbstractAction 
{
	public static final int MODE_CREATE = RepositoryManagementWizard.MODE_CREATE;
	public static final int MODE_MODIFY = RepositoryManagementWizard.MODE_MODIFY;
	public static final int MODE_DELETE = RepositoryManagementWizard.MODE_DELETE;
			
	private Logger log = LoggerFactory.getLogger(RepositoryManagementAction.class);
	
	private int mode = 0;
	
	public RepositoryManagementAction(int mode)
	{
		if(mode==MODE_CREATE){
			putValue(AbstractAction.NAME, "Crear repositori");
		}		
		else if(mode==MODE_MODIFY){
			putValue(AbstractAction.NAME, "Modificar repositori");
		}
		else if(mode==MODE_DELETE){
			putValue(AbstractAction.NAME, "Eliminar repositori");
		}
		else{
			throw new IllegalArgumentException("Mode no vàlid per l'acció de gestió de repositoris");
		}
		
		this.mode = mode;
	}
			
	@Override
	public void actionPerformed(ActionEvent event) 
	{				
		try{	
			String wizardTitle = null;
			RepositoryManagementWizard wizard = null;
			ApplicationModel model = (ApplicationModel)ApplicationContext.getInstance().getRegisteredComponent(ApplicationModel.class);
			if(mode==MODE_CREATE){
				wizardTitle = "Crear repositori";				
				wizard = new RepositoryManagementWizard(mode);
			}
			else if(mode==MODE_MODIFY){
				wizardTitle = "Modificar repositori";
				wizard = new RepositoryManagementWizard(model.getSelectedRepository(), mode);
			}
			else{
				wizardTitle = "Eliminar repositori";
				wizard = new RepositoryManagementWizard(model.getSelectedRepository(), mode);
			}
			new WizardDialog<RepositoryManagementWizard>(ApplicationWindowManager.getInstance().getMainWindow(), wizardTitle, new Dimension(500, 350), wizard).open();
		}
		catch(Exception e){
			log.error("Error al crear Wizard de gestió de repositoris: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al crear wizard de gestió de repositoris: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} 
	}	
}
