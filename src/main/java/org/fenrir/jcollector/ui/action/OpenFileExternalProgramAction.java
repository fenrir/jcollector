package org.fenrir.jcollector.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.jcollector.ui.model.ApplicationModel;
import org.fenrir.jcollector.core.CorePreferenceConstants;
import org.fenrir.jcollector.core.service.IContentAdministrationService;
import org.fenrir.jcollector.core.service.IContentSearchService;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.event.ApplicationEvent;
import org.fenrir.jcollector.core.event.IApplicationEventListener;
import org.fenrir.jcollector.util.CommonUtils;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.4.20140601
 */
@SuppressWarnings("serial")
public class OpenFileExternalProgramAction extends AbstractAction implements PropertyChangeListener
{
	private Logger log = LoggerFactory.getLogger(OpenFileExternalProgramAction.class);

	private ApplicationModel model;

	public OpenFileExternalProgramAction(ApplicationModel model)
	{
		super();
		putValue(LARGE_ICON_KEY, new ImageIcon("data/icons/file_open_16.png"));

		this.model = model;

		// Per defecte l'acció estará deshabilitada
		setEnabled(false);
		model.addPropertyChangeListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent event)
	{
		try{
			IPreferenceService preferenceService = (IPreferenceService)ApplicationContext.getInstance().getRegisteredComponent(IPreferenceService.class);
			String applicationPath = preferenceService.getProperty(CorePreferenceConstants.EXTERNAL_PROGRAM);
			// S'avisa si la configuració no té especificada una aplicació
			if(StringUtils.isBlank(applicationPath)){
				log.warn("La configuració actual no té espeficiada una aplicació externa");
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "La configuració actual no té espeficiada una aplicació externa", "Advertència", JOptionPane.WARNING_MESSAGE);

				return;
			}

			final Picture selectedPicture = model.getSelectedPicture();
			final String filePath = CommonUtils.getPictureAbsolutePath(selectedPicture);

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("filePath", filePath);
			CommandLine command = CommandLine.parse(applicationPath);
			command.addArgument("${filePath}", false);
			command.setSubstitutionMap(params);
			ExecuteResultHandler resultHandler = new ExecuteResultHandler()
			{
				@Override
				public void onProcessFailed(ExecuteException e)
				{
					log.error("Error en el programa extern executat exitCode=" + e.getExitValue() + ": " + e.getMessage(), e);
				}

				@Override
				public void onProcessComplete(int exitValue)
				{
					log.debug("Execució del programa extern completada amb èxit exitValue=" + exitValue);
					try{
						// Es comproba si s'ha modificat el fitxer
						String checksum = CommonUtils.getFileChecksum(new File(filePath));
						if(!selectedPicture.getChecksum().equals(checksum)){
							IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
							IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
							IContentAdministrationService contentAdministrationService = (IContentAdministrationService)ApplicationContext.getInstance().getRegisteredComponent(IContentAdministrationService.class);

							contentAdministrationService.updatePicture(selectedPicture.getId(),
									selectedPicture.getCollection()!=null ? selectedPicture.getCollection().getId() : null,
									selectedPicture.getRepository()!=null ? selectedPicture.getRepository().getId() : null,
									selectedPicture.getPictureGroup()!=null ? selectedPicture.getPictureGroup().getId() : null,
									new File(filePath),
									selectedPicture.isCollected(),
									selectedPicture.getCollectionDate());

							Picture newPicture = contentSearchService.findPictureById(selectedPicture.getId());
							ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_PICTURE_MODIFIED);
							event.setOldValue(selectedPicture);
							event.setNewValue(newPicture);
							eventNotificationService.notifyEvent(IApplicationEventListener.class, event);
						}
					}
					catch(IOException e){
						log.error("Error al calcular el checksum de la imatge " + filePath + ": " + e.getMessage(), e);
						JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al calcular el checksum de la imatge: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					catch(Exception e){
						log.error("Error al crear la miniatura de la imatge " + filePath + ": " + e.getMessage(), e);
						JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al crear la miniatura de la imatge: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			};
			DefaultExecutor executor = new DefaultExecutor();
			// Al proporcionar un resultHandler s'habilita l'execució asincrona del procés
			executor.execute(command, resultHandler);
		}
		catch(Exception e){
			log.error("Error al executar aplicació externa per obrir la imatge: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al executar aplicació externa per obrir la imatge: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent event)
	{
		if(ApplicationModel.PROPERTY_SELECTED_PICTURE.equals(event.getPropertyName())){
			// Si no hi ha cap picture sel.leccionada es deshabilita s'acció
			if(event.getNewValue()==null){
				setEnabled(false);
			}
			else{
				setEnabled(true);
			}
		}
	}
}
