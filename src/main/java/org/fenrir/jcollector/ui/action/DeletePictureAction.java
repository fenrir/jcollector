package org.fenrir.jcollector.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.event.ApplicationEvent;
import org.fenrir.jcollector.core.event.IApplicationEventListener;
import org.fenrir.jcollector.core.service.IContentAdministrationService;
import org.fenrir.jcollector.ui.model.ApplicationModel;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.4.20140601
 */
@SuppressWarnings("serial")
public class DeletePictureAction extends AbstractAction implements PropertyChangeListener 
{
	private Logger log = LoggerFactory.getLogger(DeletePictureAction.class);
	
	private IContentAdministrationService contentAdministrationService;
	private IEventNotificationService eventNotificationService;
	
	private ApplicationModel model;				
	
	public DeletePictureAction(ApplicationModel model)
	{
		super();
		putValue(LARGE_ICON_KEY, new ImageIcon("data/icons/file_delete_16.png"));
		
		this.model = model;
		
		// Per defecte l'acció estará deshabilitada
		setEnabled(false);
		model.addPropertyChangeListener(this);
	}
	
	public void setContentAdministrationService(IContentAdministrationService contentAdministrationService)
	{
		this.contentAdministrationService = contentAdministrationService;
	}
	
	public void setEventNotificationService(IEventNotificationService eventNotificationService)
	{
		this.eventNotificationService = eventNotificationService;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		// Es demana confirmació abans d'esborrar
		if(JOptionPane.showConfirmDialog(ApplicationWindowManager.getInstance().getMainWindow(), "S'esborrarà permanenment la imatge. Està segur?", "Advertència", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE)==JOptionPane.YES_OPTION){			
			try{
				Picture oldPicture = model.getSelectedPicture();
				
				// S'esborra el registre de la imatge de la BDD
				contentAdministrationService.deletePicture(oldPicture.getId(), true);
																		
				// Es llança event de refresc per les vistes
				ApplicationEvent appEvent = new ApplicationEvent(ApplicationEvent.EVENT_PICTURE_DELETED);
				appEvent.setOldValue(oldPicture);
				eventNotificationService.notifyEvent(IApplicationEventListener.class, appEvent);								
			}
			catch(Exception e){
				log.error("Error al esborrar imatge: " + e.getMessage(), e);				
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al esborrar imatge: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) 
	{	
		if(ApplicationModel.PROPERTY_SELECTED_PICTURE.equals(event.getPropertyName())){
			// Si no hi ha cap picture sel.leccionada es deshabilita s'acció
			if(event.getNewValue()==null){
				setEnabled(false);
			}
			else{
				setEnabled(true);
			}
		}
	}
}
