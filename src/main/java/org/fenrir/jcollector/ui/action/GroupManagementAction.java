package org.fenrir.jcollector.ui.action;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.jcollector.ui.model.ApplicationModel;
import org.fenrir.jcollector.ui.wizard.GroupManagementWizard;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.WizardDialog;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.1.20110816
 */
@SuppressWarnings("serial")
public class GroupManagementAction extends AbstractAction 
{
	public static final int MODE_CREATE = GroupManagementWizard.MODE_CREATE;
	public static final int MODE_MODIFY = GroupManagementWizard.MODE_MODIFY;
	public static final int MODE_DELETE = GroupManagementWizard.MODE_DELETE;		
	
	private Logger log = LoggerFactory.getLogger(GroupManagementAction.class);
	
	private int mode = 0;
	
	public GroupManagementAction(int mode)
	{
		if(mode==MODE_CREATE){
			putValue(AbstractAction.NAME, "Crear grup");
		}		
		else if(mode==MODE_MODIFY){
			putValue(AbstractAction.NAME, "Modificar grup");
		}
		else if(mode==MODE_DELETE){
			putValue(AbstractAction.NAME, "Eliminar grup");
		}
		else{
			throw new IllegalArgumentException("Mode no vàlid per l'acció de gestió de grups");
		}
		
		this.mode = mode;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) 
	{				
		try{	
			String wizardTitle = null;
			GroupManagementWizard wizard = null;
			ApplicationModel model = (ApplicationModel)ApplicationContext.getInstance().getRegisteredComponent(ApplicationModel.class);
			if(mode==MODE_CREATE){
				wizardTitle = "Crear grup";				
				wizard = new GroupManagementWizard(mode);
			}
			else if(mode==MODE_MODIFY){
				wizardTitle = "Modificar grup";
				wizard = new GroupManagementWizard(model.getSelectedGroup(), mode);
			}
			else{
				wizardTitle = "Eliminar grup";
				wizard = new GroupManagementWizard(model.getSelectedGroup(), mode);
			}
			new WizardDialog<GroupManagementWizard>(ApplicationWindowManager.getInstance().getMainWindow(), wizardTitle, new Dimension(500, 450), wizard).open();
		}
		catch(Exception e){
			log.error("Error al crear wizard de gestió de grups: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al crear wizard de gestió de grups: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} 
	}
}
