package org.fenrir.jcollector.ui;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.action.SwitchWorkspaceAction;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.event.IWorkspaceEventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.service.IWorkspaceAdministrationService;
import org.fenrir.jcollector.ui.action.CollectionManagementAction;
import org.fenrir.jcollector.ui.action.GroupManagementAction;
import org.fenrir.jcollector.ui.action.ImportGroupContentAction;
import org.fenrir.jcollector.ui.action.RegenerateThumbnailsAction;
import org.fenrir.jcollector.ui.action.RepositoryManagementAction;
import org.fenrir.jcollector.ui.action.ImportRepositoryContentAction;
import org.fenrir.jcollector.ui.action.TagManagementAction;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.4.20140601
 */
@EventListener(definitions=IWorkspaceEventListener.class)
@SuppressWarnings("serial")
public class MenuBar extends JMenuBar implements IWorkspaceEventListener
{		
	/*----------*
    * ATRIBUTS *
    *----------*/
    /* Items dels menús que han d'estar desactivats si no hi ha workspace actiu */
    private JMenu mEdit;
    private JMenu mCollection;
    private JMenuItem iImportGroupContent;
    private JMenu mRepository;
    private JMenuItem iModifyRepository;
    private JMenuItem iImportRepositoryContent;            
    
	public MenuBar()
	{
		/* Menú File */
        JMenu mFile = new JMenu("Fitxer");
        // Acció Canviar workspace
        JMenuItem iSwitchWorkspace = new JMenuItem();
        iSwitchWorkspace.setAction(new SwitchWorkspaceAction());
        mFile.add(iSwitchWorkspace);
        // Acció Sortir
        mFile.addSeparator();
        JMenuItem iExit = new JMenuItem(new AbstractAction("Sortir") 
        {        	
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                // TODO v0.2 Mostrar advertència al sortir
                ApplicationWindowManager.getInstance().closeWindow();
            }
        });
        mFile.add(iExit);
        this.add(mFile);
        
        /* Menú edició */
        mEdit = new JMenu("Edició");
        JMenuItem iManageTags = new JMenuItem(new TagManagementAction());
        mEdit.add(iManageTags);
        JMenuItem iRegenerateThumbnails = new JMenuItem(new RegenerateThumbnailsAction());
        mEdit.add(iRegenerateThumbnails);
        this.add(mEdit);
        
        /* Menú Col.leccions */
        mCollection = new JMenu("Col.lecció");
        // Crear col.lecció        
        JMenuItem iNewCollection = new JMenuItem(new CollectionManagementAction(CollectionManagementAction.MODE_CREATE));
        mCollection.add(iNewCollection);
        // Modificar col.lecció
        JMenuItem iModifyCollection = new JMenuItem(new CollectionManagementAction(CollectionManagementAction.MODE_MODIFY)); 
        mCollection.add(iModifyCollection);        
        // Esborrar col.lecció
        // TODO v0.1 Es deshabilita l'eliminació de col.leccions fins solucionar dependències
//        iDeleteCollection = new JMenuItem("Esborrar col.lecció");
//        iDeleteCollection.addActionListener(listener);
//        iDeleteCollection.setActionCommand(COMMAND_COLLECTION_DELETE);
//        iDeleteCollection.setEnabled(false);
//        mCollection.add(iDeleteCollection);
        mCollection.addSeparator();
        // Crear grup
        JMenuItem iCreateGroup = new JMenuItem(new GroupManagementAction(GroupManagementAction.MODE_CREATE));        
        mCollection.add(iCreateGroup);
        // Modificar grup
        JMenuItem iModifyGroup = new JMenuItem(new GroupManagementAction(GroupManagementAction.MODE_MODIFY));        
        mCollection.add(iModifyGroup);
        // Importar contingut grup
        iImportGroupContent = new JMenuItem(new ImportGroupContentAction());
        mCollection.add(iImportGroupContent);
        this.add(mCollection);
        
        /* Menú Repositoris */
        mRepository = new JMenu("Repositori");        
        // Crear repositori
        JMenuItem iNewRepository = new JMenuItem(new RepositoryManagementAction(RepositoryManagementAction.MODE_CREATE));        
        mRepository.add(iNewRepository);
        // Modificar repositori
        iModifyRepository = new JMenuItem(new RepositoryManagementAction(RepositoryManagementAction.MODE_MODIFY));
        mRepository.add(iModifyRepository);
        // Importar contingut repositori
        mRepository.addSeparator();
        iImportRepositoryContent = new JMenuItem(new ImportRepositoryContentAction());    
        mRepository.add(iImportRepositoryContent);
        this.add(mRepository);                
        
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        IEventNotificationService eventNotificationService = (IEventNotificationService)applicationContext.getRegisteredComponent(IEventNotificationService.class);
        eventNotificationService.addListener(IWorkspaceEventListener.class, this);
        IWorkspaceAdministrationService workspaceAdministrationService = (IWorkspaceAdministrationService)applicationContext.getRegisteredComponent(IWorkspaceAdministrationService.class);
        // En cas que s'hagi carregat el workspace per defecte, s'haurà d'habilitar manualment els menús ja que no arribarà l'event
        if(workspaceAdministrationService.isWorkspaceLoaded()){
            workspaceLoaded();
        }
        else{
            workspaceUnloaded();
        }
    }

    @Override
    public void beforeWorkspaceLoad()
    {
        
    }
    
    @Override
    public void workspaceLoaded()
    {
        mEdit.setEnabled(true);
        mCollection.setEnabled(true);
        mRepository.setEnabled(true);
    }
	
    @Override
    public void beforeWorkspaceUnload()
    {
        
    }
    
    @Override
    public void workspaceUnloaded()
    {
        mEdit.setEnabled(false);
        mCollection.setEnabled(false);
        mRepository.setEnabled(false);
    }
}
