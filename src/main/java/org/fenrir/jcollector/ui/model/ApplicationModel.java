package org.fenrir.jcollector.ui.model;

import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.entity.Tag;
import org.fenrir.jcollector.core.event.ApplicationEvent;
import org.fenrir.jcollector.core.event.IApplicationEventListener;
import org.fenrir.yggdrasil.ui.mvc.AbstractModel;

/**
 * TODO v1.0 Javadoc
 * Classe que representa l'estat de l'aplicació en el model MVC, que en aquest moment està format
 * pels elements sel.leccionats (col.lecció, repositori, grup, imatge o tag).
 * Com que es tratarà d'un objecte compartit entre varis controladors de les vistes al que arribaran
 * diferents events produits a tot arreu de l'aplicació, s'ha de gestionar l'objecte creat com a singleton.
 * La manera d'extendre la seva funcionalitat segons el controlador s'haurà de fer mitjançant el
 * patró decorator guardant sempre la instancia original del model d'aplicació i gestionant
 * els events a través d'ella.
 * @author Antonio Archilla Nava
 * @version v0.2.20120811
 */
public class ApplicationModel extends AbstractModel implements IApplicationEventListener
{
	public static final String PROPERTY_SELECTED_COLLECTION = "SELECTED_COLLECTION";
	public static final String PROPERTY_SELECTED_REPOSITORY = "SELECTED_REPOSITORY";
	public static final String PROPERTY_SELECTED_GROUP = "SELECTED_GROUP";
	public static final String PROPERTY_SELECTED_PICTURE = "SELECTED_PICTURE";
	public static final String PROPERTY_SELECTED_TAG = "SELECTED_TAG";

	private Collection selectedCollection;
	private Repository selectedRepository;
	private PictureGroup selectedGroup;
	private Picture selectedPicture;
	private Tag selectedTag;

	public Collection getSelectedCollection()
	{
		return selectedCollection;
	}

	public void setSelectedCollection(Collection selectedCollection)
	{
		setSelectedCollection(selectedCollection, true);
	}

	public void setSelectedCollection(Collection selectedCollection, boolean fireChangeEvent)
	{
		Collection oldSelectedCollection = this.selectedCollection;
		this.selectedCollection = selectedCollection;
		if(fireChangeEvent){
			firePropertyChange(PROPERTY_SELECTED_COLLECTION, oldSelectedCollection, selectedCollection);
		}
	}

	public Repository getSelectedRepository()
	{
		return selectedRepository;
	}

	public void setSelectedRepository(Repository selectedRepository)
	{
		setSelectedRepository(selectedRepository, true);
	}

	public void setSelectedRepository(Repository selectedRepository, boolean fireChangeEvent)
	{
		Repository oldSelectedRepository = this.selectedRepository;
		this.selectedRepository = selectedRepository;
		if(fireChangeEvent){
			firePropertyChange(PROPERTY_SELECTED_REPOSITORY, oldSelectedRepository, selectedRepository);
		}
	}

	public PictureGroup getSelectedGroup()
	{
		return selectedGroup;
	}

	public void setSelectedGroup(PictureGroup selectedGroup)
	{
		setSelectedGroup(selectedGroup, true);
	}

	public void setSelectedGroup(PictureGroup selectedGroup, boolean fireChangeEvent)
	{
		PictureGroup oldSelectedGroup = this.selectedGroup;
		this.selectedGroup = selectedGroup;
		if(fireChangeEvent){
			firePropertyChange(PROPERTY_SELECTED_GROUP, oldSelectedGroup, selectedGroup);
		}
	}

	public Picture getSelectedPicture()
	{
		return selectedPicture;
	}

	public void setSelectedPicture(Picture selectedPicture)
	{
		setSelectedPicture(selectedPicture, true);
	}

	public void setSelectedPicture(Picture selectedPicture, boolean fireChangeEvent)
	{
		Picture oldSelectedPicture = this.selectedPicture;
		this.selectedPicture = selectedPicture;
		if(fireChangeEvent){
			firePropertyChange(PROPERTY_SELECTED_PICTURE, oldSelectedPicture, selectedPicture);
		}
	}

	public Tag getSelectedTag()
	{
		return selectedTag;
	}

	public void setSelectedTag(Tag selectedTag)
	{
		setSelectedTag(selectedTag, true);
	}

	public void setSelectedTag(Tag selectedTag, boolean fireChangeEvent)
	{
		Tag oldSelectedTag = this.selectedTag;
		this.selectedTag = selectedTag;
		if(fireChangeEvent){
			firePropertyChange(PROPERTY_SELECTED_TAG, oldSelectedTag, selectedTag);
		}
	}

	@Override
	public void applicationEventPerformed(ApplicationEvent event)
	{
		if(ApplicationEvent.EVENT_GROUP_MODIFIED.equals(event.getType())){
			PictureGroup oldGroup = (PictureGroup)event.getOldValue();
			PictureGroup newGroup = (PictureGroup)event.getNewValue();
			if(oldGroup!=null && selectedGroup!=null
					&& oldGroup.getId().equals(selectedGroup.getId())){
				setSelectedGroup(newGroup);
			}
		}
		else if(ApplicationEvent.EVENT_REPOSITORY_MODIFIED.equals(event.getType())){
			Repository oldRepository = (Repository)event.getOldValue();
			Repository newRepository = (Repository)event.getNewValue();
			if(oldRepository!=null && selectedRepository!=null
					&& oldRepository.getId().equals(selectedRepository.getId())){
				setSelectedRepository(newRepository);
			}
		}
		else if(ApplicationEvent.EVENT_PICTURE_MODIFIED.equals(event.getType())){
			Picture oldPicture = (Picture)event.getOldValue();
			Picture newPicture = (Picture)event.getNewValue();
			if(oldPicture!=null && selectedPicture!=null
					&& oldPicture.getId().equals(selectedPicture.getId())){
				setSelectedPicture(newPicture);
			}
		}
		else if(ApplicationEvent.EVENT_PICTURE_DELETED.equals(event.getType())){
			Picture oldPicture = (Picture)event.getOldValue();
			if(oldPicture!=null && selectedPicture!=null
					&& oldPicture.getId().equals(selectedPicture.getId())){
				setSelectedPicture(null);
			}
		}
	}
}
