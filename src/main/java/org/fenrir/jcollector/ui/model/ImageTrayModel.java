package org.fenrir.jcollector.ui.model;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.entity.Tag;
import org.fenrir.jcollector.ui.dto.TrayElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20110830
 */
public class ImageTrayModel extends ApplicationModel 
{
	private ApplicationModel applicationModel;
	
	private List<TrayElement> trayElements = new ArrayList<TrayElement>();
	
	public ImageTrayModel(ApplicationModel applicationModel)
	{
		this.applicationModel = applicationModel;
	}
	
	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) 
    {
		super.addPropertyChangeListener(listener);
		applicationModel.addPropertyChangeListener(listener);        
    }

	@Override
    public void removePropertyChangeListener(PropertyChangeListener listener) 
    {
		super.removePropertyChangeListener(listener);
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
	
	@Override
	public Collection getSelectedCollection() 
	{
		return applicationModel.getSelectedCollection();
	}
	
	@Override
	public void setSelectedCollection(Collection selectedCollection) 
	{
		applicationModel.setSelectedCollection(selectedCollection);		
	}
	
	@Override
	public Repository getSelectedRepository() 
	{
		return applicationModel.getSelectedRepository();
	}
	
	@Override
	public void setSelectedRepository(Repository selectedRepository) 
	{
		applicationModel.setSelectedRepository(selectedRepository);		
	}
	
	@Override
	public PictureGroup getSelectedGroup() 
	{
		return applicationModel.getSelectedGroup();
	}
	
	@Override
	public void setSelectedGroup(PictureGroup selectedGroup) 
	{
		applicationModel.setSelectedGroup(selectedGroup);		
	}
	
	@Override
	public Picture getSelectedPicture() 
	{		
		return applicationModel.getSelectedPicture();
	}
	
	@Override
	public void setSelectedPicture(Picture selectedPicture) 
	{
		applicationModel.setSelectedPicture(selectedPicture);		
	}
	
	@Override
	public Tag getSelectedTag() 
	{
		return applicationModel.getSelectedTag();
	}

	@Override
	public void setSelectedTag(Tag selectedTag) 
	{
		applicationModel.setSelectedTag(selectedTag);		
	}
	
	public List<TrayElement> getTrayElements()
	{
		return trayElements;
	}
	
	public void addTrayElement(TrayElement element)
	{
		if(!trayElements.contains(element)){
			trayElements.add(element);
		}
	}
	
	public void removeTrayElement(TrayElement element)
	{
		trayElements.remove(element);
	}
	
	public void removeAllUnpinnedTrayElements()
	{
		Iterator<TrayElement> iterator = trayElements.iterator();
		while(iterator.hasNext()){
			TrayElement element = iterator.next();
			if(!element.isPinned()){
				iterator.remove();
			}
		}
	}
	
	public void clearTray()
	{
		trayElements.clear();
	}
	
	public void setPicturePinned(Picture picture, boolean pinned)
	{
		TrayElement element = new TrayElement(picture);
		int index = trayElements.indexOf(element);
		if(index>=0){
			trayElements.get(index).setPinned(pinned);
		}
	}
	
	public void addUnpinnedPicture(Picture picture)
	{
		TrayElement element = new TrayElement(picture, false);
		if(!trayElements.contains(element)){
			trayElements.add(element);
		}
	}	
	
	/**
	 * Reemplaça la imatge si aquesta està present al tray, estigui ancorada o lliure 
	 * En cas contrari no es fa res
	 * @param oldPicture Picture
	 * @param newPicture Picture
	 */
	public void replacePicture(Picture oldPicture, Picture newPicture)
	{
		TrayElement element = new TrayElement(oldPicture);
		int index = trayElements.indexOf(element);
		if(index>=0){
			TrayElement oldElement = trayElements.remove(index);
			element.setPicture(newPicture);
			element.setPinned(oldElement.isPinned());
			trayElements.add(index, element);
		}		
	}	
	
	public void removePicture(Picture picture)
	{
		TrayElement element = new TrayElement(picture);
		trayElements.remove(element);
	}
}
