package org.fenrir.jcollector.ui.wizard;

import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.util.TreeList;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20130721
 */
@SuppressWarnings("serial")
public class GroupSelectionWizardPage extends AbstractWizardPage<GroupManagementWizard> 
{
    public static final String ID = "org.fenrir.jcollector.ui.wizard.groupSelectionWizardPage";			

    private JTree treeGroups;		 

    public GroupSelectionWizardPage(String title, String description, GroupManagementWizard wizard)throws Exception
    {
        super(title, description, wizard);		
    }
	
    @Override
    public String getId()
    {
        return ID;
    }
	
    @Override
    public JPanel createContents()
    {
        JPanel pContent = new JPanel();		
        pContent.setLayout(new GridLayout(1, 1));

        /* Definició dels components */
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode();
        DefaultTreeModel treeModel = new DefaultTreeModel(rootNode);
        treeGroups = new JTree(treeModel);
        // El node arrel no serà visible per tal de poder posar tots els elements sota el mateix arbre
        treeGroups.setRootVisible(false);
        treeGroups.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        // Si es (des)sel.lecciona es llança la validació de la pàgina
        treeGroups.addTreeSelectionListener(new TreeSelectionListener() 
        {			
            @Override
            public void valueChanged(TreeSelectionEvent event) 
            {
                DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)treeGroups.getLastSelectedPathComponent();
                if(selectedNode!=null){
                    wizard.setSelectedGroup((PictureGroup)selectedNode.getUserObject());
                }
                // En el cas que no hi hagi nodes sel.leccionats el validat desactivarà el botó d'avançar
                wizard.validateCurrentPage();				
            }
        });
        JScrollPane scrollPane = new JScrollPane(treeGroups);		
        pContent.add(scrollPane);
		
        return pContent;
    }
	
    /**
     * 
     */
    public void loadFormData(TreeList tree)
    {    	
    	DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode();
    	DefaultTreeModel treeModel = (DefaultTreeModel)treeGroups.getModel();	    	
    	for(TreeList.TreeNode node:tree.getRoot().getChildren()){                	
    		DefaultMutableTreeNode childBranch = addBranch(node);
    		rootNode.add(childBranch);
        }
        treeModel.setRoot(rootNode);        
        treeModel.reload();
    }
    
    private DefaultMutableTreeNode addBranch(TreeList.TreeNode node)
    {
    	DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(node.getObject());
    	for(TreeList.TreeNode child:node.getChildren()){
    		DefaultMutableTreeNode childBranch = addBranch(child);
    		treeNode.add(childBranch);
    	}
    	
    	return treeNode;
    }
    
    @Override
    public boolean validatePage()
    {
    	return treeGroups.getSelectionCount()>0;    	
    }      
}
