package org.fenrir.jcollector.ui.wizard;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import javax.swing.JOptionPane;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.event.ApplicationEvent;
import org.fenrir.jcollector.core.event.IApplicationEventListener;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.core.service.IContentAdministrationService;
import org.fenrir.jcollector.core.service.IContentSearchService;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.1.20111025
 */
public class RepositoryManagementWizard extends AbstractWizard implements IRepositoryWizardSupport
{
	public static final int MODE_CREATE = 1;
	public static final int MODE_MODIFY = 2;
	public static final int MODE_DELETE = 3;

	private Logger log = LoggerFactory.getLogger(RepositoryManagementWizard.class);
	
	private RepositorySelectionWizardPage<RepositoryManagementWizard> repositorySelectionPage;
	private RepositoryWizardPage repositoryPage;
	
	private int mode;
	private Repository selectedRepository;
	
	public RepositoryManagementWizard(int mode) throws Exception
	{					
		this(null, mode);
	}
	
	public RepositoryManagementWizard(Repository selectedRepository, int mode) throws Exception
	{
		if(mode==MODE_CREATE){
			setTitle("Crear repositori");
		}
		else if(mode==MODE_MODIFY){
			setTitle("Modificar repositori");
		}
		else if(mode==MODE_DELETE){
			setTitle("Eliminar repositori");
		}
		else{
			throw new IllegalArgumentException("Mode " + mode + " no permès");
		}	
		
		this.mode = mode;
		if(selectedRepository!=null){
			this.selectedRepository = new Repository(selectedRepository);
		}		
		addPages();
	}
	
	@Override
	public String getIcon()
	{
		if(mode==MODE_CREATE){
			return "data/icons/repository_add_48.png";
		}
		
		return null;
	}
	
	protected void addPages() throws Exception
	{
		if((mode==MODE_MODIFY || mode==MODE_DELETE) && selectedRepository==null){
			String repositorySelectionPageTitle = "Sel.lecció d'un repositori existent";
			String repositorySelectionPageDesc = mode==MODE_MODIFY ? "Sel.leccioni el repositori a modificar" : "Sel.leccioni el repositori a eliminar"; 			
			repositorySelectionPage = new RepositorySelectionWizardPage<RepositoryManagementWizard>(repositorySelectionPageTitle, repositorySelectionPageDesc, this);
			addPage(repositorySelectionPage);
		}
		if(mode==MODE_CREATE || mode==MODE_MODIFY){
			String repositoryPageTitle = mode==MODE_CREATE ? "Creació d'un nou repositori" : "Modificació d'un repositori existent";
			String repositoryPageDesc = mode==MODE_CREATE ? "Introdueixi les dades del nou repositori" : "Introdueixi les modificaciones de les dades del repositori";
			repositoryPage = new RepositoryWizardPage(repositoryPageTitle, repositoryPageDesc, this);
			// S'afegeix la pàgina al wizard abans de fer les comprobacions per tal que la crida a validateCurrentPage del wizard actui sobre aquesta
			addPage(repositoryPage);
			// En cas de modificació, si s'ha especificat la col.lecció, es carrega
			if(selectedRepository!=null){
				repositoryPage.setFormData(selectedRepository);
			}			
		}
		if(mode==MODE_DELETE){
			
		}
	}	
	
	@Override
	protected boolean canPerformPrevious() 
	{
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();		
		if(RepositoryWizardPage.ID.equals(page.getId()) && repositorySelectionPage!=null){
			return true;
		}
		
		return false;
	}

	@Override
	protected boolean canPerformNext() 
	{
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();		
		if(RepositorySelectionWizardPage.ID.equals(page.getId())){
			return true;
		}
		
		return false;
	}

	@Override
	protected boolean canPerformFinalize() 
	{
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();		
		if(RepositoryWizardPage.ID.equals(page.getId())){
			return true;
		}
		
		return false;
	}

	@Override
	public boolean performPrevious()
	{
		return true;
	}
	
	@Override
	public boolean performNext()
	{
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();
		// Si la pàgina actual es la de sel.lecció de col.leccions, es carrega les dades de la d'edició
		if(RepositorySelectionWizardPage.ID.equals(page.getId())){
			repositoryPage.setFormData(selectedRepository);
			
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean performFinalize()
	{
		IContentAdministrationService contentAdministrationService = (IContentAdministrationService)ApplicationContext.getInstance().getRegisteredComponent(IContentAdministrationService.class);
		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
		IEventNotificationService eventNotifier = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
		
		if(mode==MODE_CREATE){
			try{			
				Repository newRepository = repositoryPage.getFormData();
				newRepository = contentAdministrationService.createRepository(newRepository.getName(), newRepository.getFolderPath(), newRepository.getSource(), newRepository.isUrl(), newRepository.isActive());
				
				// Es llença l'event de col.lecció creada				
				ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_REPOSITORY_CREATED);
				event.setNewValue(newRepository);
				eventNotifier.notifyEvent(IApplicationEventListener.class, event);
			}
			catch(Exception e){
	    		log.error("Error al crear un nou repositori: "+e.getMessage(), e);
	    		JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al crear un nou repositori: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	    		
	    		return false;
			}				
		}
		else if(mode==MODE_MODIFY){
			Repository newRepository = repositoryPage.getFormData();
			try{							
				contentAdministrationService.updateRepository(selectedRepository.getId(), newRepository.getName(), newRepository.getFolderPath(), newRepository.getSource(), newRepository.isUrl(), newRepository.isActive());								
				
				// Si s'ha activat el repositori es crea el directori correponent en cas de ser necessari
				if(!selectedRepository.isActive() && newRepository.isActive()){
					File folder = new File(newRepository.getFolderPath());
					if(!folder.exists()){
						FileUtils.forceMkdir(folder);
					}
				}
				// Si s'ha desactivat s'elimina en cas d'indicar-se que així es faci
				else if(selectedRepository.isActive() && !newRepository.isActive()){
					File folder = new File(selectedRepository.getFolderPath());
					List<Picture> pictures = contentSearchService.findOutstandingPicturesByRepository(selectedRepository.getId());
					for(Picture picture:pictures){
						contentAdministrationService.deletePicture(picture.getId());
					}

					// Si el directori no està buit es podrà esborrar juntament amb el seu contingut
					if(folder.listFiles().length>0){
						int option = JOptionPane.showConfirmDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Dessitja esborrar el directori del repositori inactivat i el seu contingut?");
						if(option==JOptionPane.OK_OPTION){
							FileUtils.deleteDirectory(folder);
						}
					}
					// En el cas que si estigui buit, s'esborrarà
					else{
						folder.delete();
					}
				}
				
				// Es llença l'event de col.lecció modificada
				// S'ha de recuperar de nou les dades de BDD per tenir-les actualitzades.
				// TODO Treure quan s'actualitzi el servei per tal que en fer updates retorni l'objecte
				newRepository = contentSearchService.findRepositoryById(selectedRepository.getId());
				ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_REPOSITORY_MODIFIED);
				event.setOldValue(selectedRepository);
				event.setNewValue(newRepository);
				eventNotifier.notifyEvent(IApplicationEventListener.class, event);
			}
			catch(Exception e){
	    		log.error("Error al modificar un repositori: "+e.getMessage(), e);
	    		JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al modificar un repositori: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	    		
	    		return false;
			}	
						
			// Si s'ha cambiat el directori, es mouen els fitxers a la nova ubicació
			try{
				String strOldFolder = selectedRepository.getFolderPath();
				String strNewFolder = newRepository.getFolderPath();
				if(strNewFolder!=null && !strNewFolder.equals(strOldFolder)){
					File oldFolder = new File(strOldFolder);
					File newFolder = new File(strNewFolder);
					// Si no s'ha renombrat el directori. En cas d'haver renombrat el directori del grup no cal moure res
					if(oldFolder.exists()){
						// Si el directori destí no està buit s'avisa de si es vol continuar
						if(!FileUtils.listFiles(newFolder, null, true).isEmpty()){
							String msg = "El directori destí a on es mouran les imatges no està buit. "
									 + "En el cas de col.lisió es sobreescriuran els fitxers del d'aquest directori. "
									 + "Desitja continuar?";
							int option = JOptionPane.showConfirmDialog(ApplicationWindowManager.getInstance().getMainWindow(), msg, "Avís", JOptionPane.YES_NO_OPTION);
							if(option==JOptionPane.NO_OPTION){
								return false;
							}
						}
						
						Collection<File> files = FileUtils.listFiles(oldFolder, null, true);
						for(File srcFile:files){					
							String relativePath = StringUtils.substringAfter(srcFile.getAbsolutePath(), oldFolder.getAbsolutePath());
							if(relativePath.startsWith(File.separator)){
								relativePath = relativePath.substring(1);
							}
							File destFile = new File(newFolder.getAbsolutePath() + File.separator + relativePath);
							if(destFile.exists()){
								log.warn("Sobreescrivint fitxer " + destFile.getAbsolutePath());
								FileUtils.deleteQuietly(destFile);
							}			
							FileUtils.moveFile(srcFile, destFile);
						}
					}
				}				
			}
			catch(IOException e){
				log.error("Error al moure les imatges del repositori al nou directori: "+e.getMessage(), e);
	    		JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al moure les imatges del repositori al nou directori: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	    		
	    		return false;
			}
		}		
		else if(mode==MODE_DELETE){
			
		}
		
		return true;
	}
	
	@Override
	public boolean performCancel()
	{
		return true;
	}
	
	@Override
	public List<Repository> getAllRepositories() throws BusinessException
	{
		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
		return contentSearchService.findAllRepositories();
	}

	@Override
	public void setSelectedRepository(Repository repository)
	{
		this.selectedRepository = repository;
	}
		
	public boolean checkRepositoryDeactivation()
	{
		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
		try{
			if(selectedRepository!=null && contentSearchService.findOutstandingPicturesByRepository(selectedRepository.getId()).size()>0){
				return false;
			}
		}
		catch(BusinessException e){
			log.error("Error en obtenir les imatges del repositori " + selectedRepository.getId() + ": " + e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error en obtenir les imatges del repositori " + selectedRepository.getId() + ": " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);    		
		}
		
		return true;
	}
}
