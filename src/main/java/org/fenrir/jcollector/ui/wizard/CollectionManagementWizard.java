package org.fenrir.jcollector.ui.wizard;

import java.util.List;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.event.ApplicationEvent;
import org.fenrir.jcollector.core.event.IApplicationEventListener;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.core.service.IContentAdministrationService;
import org.fenrir.jcollector.core.service.IContentSearchService;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.4.20140522
 */
public class CollectionManagementWizard extends AbstractWizard implements ICollectionWizardSupport
{
	public static final int MODE_CREATE = 1;
	public static final int MODE_MODIFY = 2;
	public static final int MODE_DELETE = 3;

	private Logger log = LoggerFactory.getLogger(CollectionManagementWizard.class);
	
	private CollectionSelectionWizardPage<CollectionManagementWizard> collectionSelectionPage;
	private CollectionWizardPage collectionPage;
	
	private int mode;
	private Collection selectedCollection;			
	
	public CollectionManagementWizard(int mode) throws Exception
	{					
		this(null, mode);
	}
	
	public CollectionManagementWizard(Collection collection, int mode) throws Exception
	{
		if(mode==MODE_CREATE){
			setTitle("Crear col.lecció");
		}
		else if(mode==MODE_MODIFY){
			setTitle("Modificar col.lecció");
		}
		else if(mode==MODE_DELETE){
			setTitle("Eliminar col.lecció");
		}
		else{
			throw new IllegalArgumentException("Mode " + mode + " no permès");
		}	
		
		this.mode = mode;
		this.selectedCollection = collection;
		addPages();
	}
	
	protected void addPages() throws Exception
	{
		if((mode==MODE_MODIFY || mode==MODE_DELETE) && selectedCollection==null){
			String collectionSelectionPageTitle = "Sel.lecció d'una col.lecció existent";
			String collectionSelectionPageDesc = mode==MODE_MODIFY ? "Sel.leccioni la col.lecció a modificar" : "Sel.leccioni la col.lecció a eliminar"; 			
			collectionSelectionPage = new CollectionSelectionWizardPage<CollectionManagementWizard>(collectionSelectionPageTitle, collectionSelectionPageDesc, this);
			addPage(collectionSelectionPage);
		}
		if(mode==MODE_CREATE || mode==MODE_MODIFY){
			String collectionPageTitle = mode==MODE_CREATE ? "Creació d'una nova col.lecció" : "Modificació d'una col.lecció existent";
			String collectionPageDesc = mode==MODE_CREATE ? "Introdueixi les dades de la nova col.lecció" : "Introdueixi les modificaciones de les dades de la col.lecció";
			collectionPage = new CollectionWizardPage(collectionPageTitle, collectionPageDesc, this);
			// S'afegeix la pàgina al wizard abans de fer les comprobacions per tal que la crida a validateCurrentPage del wizard actui sobre aquesta
			addPage(collectionPage);
			// En cas de modificació, si s'ha especificat la col.lecció, es carrega
			if(selectedCollection!=null){
				collectionPage.setFormData(selectedCollection);
			}			
		}
		if(mode==MODE_DELETE){
			
		}
	}	
	
	@Override
	protected boolean canPerformPrevious() 
	{
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();		
		if(CollectionWizardPage.ID.equals(page.getId()) && collectionSelectionPage!=null){
			return true;
		}
		
		return false;
	}

	@Override
	protected boolean canPerformNext() 
	{
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();		
		if(CollectionSelectionWizardPage.ID.equals(page.getId())){
			return true;
		}
		
		return false;
	}

	@Override
	protected boolean canPerformFinalize() 
	{
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();		
		if(CollectionWizardPage.ID.equals(page.getId())){
			return true;
		}
		
		return false;
	}

	@Override
	public boolean performPrevious()
	{
		return true;
	}
	
	@Override
	public boolean performNext()
	{
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();
		// Si la pàgina actual es la de sel.lecció de col.leccions, es carrega les dades de la d'edició
		if(CollectionSelectionWizardPage.ID.equals(page.getId())){
			collectionPage.setFormData(selectedCollection);
			
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean performFinalize()
	{
		IContentAdministrationService contentAdministrationService = (IContentAdministrationService)ApplicationContext.getInstance().getRegisteredComponent(IContentAdministrationService.class);
		IEventNotificationService eventNotifier = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
		
		if(mode==MODE_CREATE){
			try{			
				Collection newCollection = collectionPage.getFormData();
				contentAdministrationService.createCollection(newCollection.getName(), newCollection.getCreationDate());
				
				// Es llença l'event de col.lecció creada					
				ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_COLLECTION_CREATED);
				event.setNewValue(newCollection);
				eventNotifier.notifyEvent(IApplicationEventListener.class, event);
			}
			catch(Exception e){
	    		log.error("Error al crear una nova col.lecció: "+e.getMessage(), e);
	    		JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al crear una nova col.lecció: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	    		
	    		return false;
			}				
		}
		else if(mode==MODE_MODIFY){
			try{			
				Collection newCollection = collectionPage.getFormData();
				contentAdministrationService.updateCollection(selectedCollection.getId(), newCollection.getName(), newCollection.getCreationDate());
				
				// Es llença l'event de col.lecció creada					
				ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_COLLECTION_MODIFIED);
				event.setOldValue(selectedCollection);
				event.setNewValue(newCollection);
				eventNotifier.notifyEvent(IApplicationEventListener.class, event);
			}
			catch(Exception e){
	    		log.error("Error al modificar una nova col.lecció: "+e.getMessage(), e);
	    		JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al modificar una nova col.lecció: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	    		
	    		return false;
			}	
		}		
		else if(mode==MODE_DELETE){
			
		}
		
		return true;
	}
	
	@Override
	public boolean performCancel()
	{
		return true;
	}
	
	@Override
	public List<Collection> getAllCollections() throws BusinessException
	{
		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
		return contentSearchService.findAllCollections();
	}
	
	@Override
	public void setSelectedCollection(Collection collection)
	{
		this.selectedCollection = collection;
	}
}
