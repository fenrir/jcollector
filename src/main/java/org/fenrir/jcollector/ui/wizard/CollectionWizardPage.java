package org.fenrir.jcollector.ui.wizard;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.1.20110807
 */
@SuppressWarnings("serial")
public class CollectionWizardPage extends AbstractWizardPage<AbstractWizard>
{
	public static final String ID = "org.fenrir.jcollector.ui.wizard.collectionWizardPage";

	public final Logger log = LoggerFactory.getLogger(CollectionWizardPage.class);
	
	private JTextField inputName;
    // TODO Canviar per DatePicker
    private JTextField inputCreationDate;
    
    private Collection originalFormData;
    private Collection newFormData;
    
    private boolean isEdited = false;
    private boolean firePageValidation = true;
	
	public CollectionWizardPage(String title, String description, AbstractWizard wizard)throws Exception
	{
		super(title, description, wizard);
	}
	
	@Override
	public String getId()
	{
		return ID;
	}
	
	public JPanel createContents()
	{
		JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        /* Definició dels components */
        JLabel lName = new JLabel("Nom:", SwingConstants.RIGHT);
        JLabel lCreationDate = new JLabel("Data creació:", SwingConstants.RIGHT);        
        inputName = new JTextField();
        inputName.getDocument().addDocumentListener(new DocumentListener() 
        {			
			@Override
			public void removeUpdate(DocumentEvent event) 
			{			
				if(log.isDebugEnabled()){
					log.debug("Actualitzant camp nom");
				}
				if(firePageValidation){
					isEdited = true;
					wizard.validateCurrentPage();
				}
			}
			
			@Override
			public void insertUpdate(DocumentEvent event) 
			{			
				if(log.isDebugEnabled()){
					log.debug("Actualitzant camp nom");
				}
				if(firePageValidation){
					isEdited = true;
					wizard.validateCurrentPage();
				}				
			}
			
			/**
			 * No s'utilitza
			 */
			@Override
			public void changedUpdate(DocumentEvent event) 
			{			
				
			}
		});                	
        inputCreationDate = new JTextField();
        inputCreationDate.getDocument().addDocumentListener(new DocumentListener() 
        {			
			@Override
			public void removeUpdate(DocumentEvent event) 
			{			
				if(log.isDebugEnabled()){
					log.debug("Actualitzant camp data");
				}
				if(firePageValidation){
					isEdited = true;
					wizard.validateCurrentPage();
				}
			}
			
			@Override
			public void insertUpdate(DocumentEvent event) 
			{			
				if(log.isDebugEnabled()){
					log.debug("Actualitzant camp data");
				}
				if(firePageValidation){
					isEdited = true;
					wizard.validateCurrentPage();
				}				
			}
			
			/**
			 * No s'utilitza
			 */
			@Override
			public void changedUpdate(DocumentEvent event) 
			{			
				
			}
		});

        /* Grup horitzontal */
        layout.setHorizontalGroup(
        	layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(lName, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lCreationDate, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(inputCreationDate, GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
                    .addComponent(inputName, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)                    
                )
                .addContainerGap()
            )
        );

        /* Grup vertical */
        layout.setVerticalGroup(
        	layout.createParallelGroup(GroupLayout.Alignment.LEADING)
           	.addGroup(layout.createSequentialGroup()
           		.addContainerGap()
           		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
           			.addComponent(lName)
           			.addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
           		)
           		.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
           		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
           			.addComponent(lCreationDate)
           			.addComponent(inputCreationDate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
           		)
           		.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
           	)
        );

        return pContents;
	}		
	
	@Override
	public boolean validatePage()
	{		
		String collectionName = inputName.getText();
		/* Comprovació dels valors introduïts */
        if(StringUtils.isEmpty(collectionName)){
        	if(isEdited){
        		wizard.showErrorMessage("El camp NOM no pot ser buit");
        	}
            return false;
        }        
        else if(StringUtils.isEmpty(inputCreationDate.getText())){
        	if(isEdited){
        		wizard.showErrorMessage("El camp DATA CREACIÓ no pot ser buit");
        	}
            return false;
        }         
        
        try{
           	new SimpleDateFormat("dd/MM/yyyy").parse(inputCreationDate.getText());
        }
        catch(ParseException e){
        	if(isEdited){
        		wizard.showErrorMessage("El camp DATA CREACIÓ no conté una data vàlida");
        	}
        	return false;
        }         
        
        return true;
	}
	
	public Collection getFormData() throws ParseException
	{
		newFormData = new Collection();
		// En cas que sigui edició la id serà la mateixa que la del objecte original
		if(originalFormData!=null){
			newFormData.setId(originalFormData.getId());
		}
        newFormData.setName(inputName.getText());        
        newFormData.setCreationDate(new SimpleDateFormat("dd/MM/yyyy").parse(inputCreationDate.getText()));      
		
		return newFormData;
	}
	
	public void setFormData(Collection formData)
	{
		this.originalFormData = formData;
		
		// Per tal que no llenci els events de canvi dels camps i activi les validacions
		firePageValidation = false;
		inputName.setText(formData.getName());        
        String strCreationDate = new SimpleDateFormat("dd/MM/yyyy").format(formData.getCreationDate());
        inputCreationDate.setText(strCreationDate);
        // Es torna a activar les validacions
        firePageValidation = true;
	}
}
