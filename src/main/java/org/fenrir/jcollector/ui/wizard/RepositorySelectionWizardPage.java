package org.fenrir.jcollector.ui.wizard;

import java.awt.GridLayout;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20130721
 */
@SuppressWarnings("serial")
public class RepositorySelectionWizardPage<T extends AbstractWizard & IRepositoryWizardSupport> extends AbstractWizardPage<T>
{
    public static final String ID = "org.fenrir.jcollector.ui.wizard.repositorySelectionWizardPage";			

    private JList repositoryList; 
	
    public RepositorySelectionWizardPage(String title, String description, T wizard)throws Exception
    {
        super(title, description, wizard);	

        loadFormData();
    }
	
    @Override
    public String getId()
    {
        return ID;
    }
	
    @Override
    public JPanel createContents()
    {
        JPanel pContent = new JPanel();		
        pContent.setLayout(new GridLayout(1, 1, 10, 10));

        /* Definició dels components */
        JScrollPane scrollPane = new JScrollPane();        
        repositoryList = new JList();
        repositoryList.addListSelectionListener(new ListSelectionListener() 
        {		
            @Override
            public void valueChanged(ListSelectionEvent event) 
            {
                wizard.setSelectedRepository((Repository)repositoryList.getSelectedValue());
                wizard.validateCurrentPage();					
            }
        });
        scrollPane.setViewportView(repositoryList);
        pContent.add(scrollPane);
		
        return pContent;
    }
		
    private void loadFormData() throws Exception
    {
    	DefaultListModel listModel = new DefaultListModel();
    	for(Repository repository:wizard.getAllRepositories()){                	
        	listModel.addElement(repository);
        }
        repositoryList.setModel(listModel);
    }
    
    @Override
    public boolean validatePage()
    {
    	return repositoryList.getSelectedIndex() != -1;
    }        
}
