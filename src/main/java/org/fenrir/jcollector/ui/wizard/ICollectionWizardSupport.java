package org.fenrir.jcollector.ui.wizard;

import java.util.List;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.exception.BusinessException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20110803
 */
public interface ICollectionWizardSupport
{
	public List<Collection> getAllCollections() throws BusinessException;
	public void setSelectedCollection(Collection collection);
}
