package org.fenrir.jcollector.ui.wizard;

import java.util.List;

import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.exception.BusinessException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20110821
 */
public interface IRepositoryWizardSupport 
{
	public List<Repository> getAllRepositories() throws BusinessException;
	public void setSelectedRepository(Repository repository);
}
