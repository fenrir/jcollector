package org.fenrir.jcollector.ui.wizard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.widget.AdvancedTextField;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;
import org.fenrir.jcollector.ui.dialog.GroupSelectionDialog;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.exception.BusinessException;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.4.20140601
 */
@SuppressWarnings("serial")
public class GroupWizardPage extends AbstractWizardPage<GroupManagementWizard>
{
    public static final String ID = "org.fenrir.jcollector.ui.wizard.groupWizardPage";

    private final Logger log = LoggerFactory.getLogger(GroupWizardPage.class);

    private JTextField inputName;
    private JComboBox comboCollections;
    private PictureGroup parent;
    private AdvancedTextField inputParent;
    private JTextField inputPattern;
    private AdvancedTextField inputFolder;

    private PictureGroup formData = null;

    private boolean isPatternEdited = false;
    private boolean isEdited = false;
    private boolean firePageValidation = true;

    public GroupWizardPage(String title, String description, GroupManagementWizard wizard)throws Exception
    {
        super(title, description, wizard);
    }

    @Override
    public String getId()
    {
        return ID;
    }

    @Override
    public JPanel createContents()
    {
        JPanel pContent = new JPanel();

        /* Components */
        JLabel lName = new JLabel("Nom", JLabel.RIGHT);
        inputName = new JTextField();
        inputName.getDocument().addDocumentListener(new DocumentListener()
        {
            @Override
            public void removeUpdate(DocumentEvent event)
            {
                if(log.isDebugEnabled()){
                    log.debug("Actualitzant camp nom");
                }
                // S'actualitzen la resta de camps depenents del nom
                String name = inputName.getText();
                if(!isPatternEdited && StringUtils.isNotBlank(name)){
                    Pattern pattern = Pattern.compile("([\\s-_]+)");
                    Matcher matcher = pattern.matcher(name.trim());
                    String replacedPattern = matcher.replaceAll("_");
                    inputPattern.setText(replacedPattern.toLowerCase() + "_##");
                }

                if(firePageValidation){
                    isEdited = true;
                    wizard.validateCurrentPage();
                }
            }

            @Override
            public void insertUpdate(DocumentEvent event)
            {
                if(log.isDebugEnabled()){
                    log.debug("Actualitzant camp nom");
                }
                // S'actualitzen la resta de camps depenents del nom
                String name = inputName.getText();
                if(!isPatternEdited && StringUtils.isNotBlank(name)){
                    Pattern pattern = Pattern.compile("([\\s-_]+)");
                    Matcher matcher = pattern.matcher(name.trim());
                    String replacedPattern = matcher.replaceAll("_");
                    inputPattern.setText(replacedPattern.toLowerCase() + "_##");
                }

                if(firePageValidation){
                    isEdited = true;
                    wizard.validateCurrentPage();
                }
            }

            /**
             * No s'utilitza
             */
            @Override
            public void changedUpdate(DocumentEvent event)
            {

            }
        });
        JLabel lCollectionTitle = new JLabel("Col.lecció", JLabel.RIGHT);
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();
        comboCollections = new JComboBox(comboBoxModel);
        comboCollections.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                Collection selectedCollection = (Collection)((DefaultComboBoxModel)comboCollections.getModel()).getSelectedItem();
                if(selectedCollection!=null && firePageValidation){
                    isEdited = true;
                    wizard.validateCurrentPage();
                }
            }
        });
        JLabel lParent = new JLabel("Grup pare", JLabel.RIGHT);
        inputParent = new AdvancedTextField("/org/fenrir/yggdrasil/ui/icons/search_16.png");
        inputParent.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent event)
            {
                int keyPressed = event.getKeyCode();
                if(keyPressed==KeyEvent.VK_ENTER){
                    try{
                        GroupSelectionDialog dialog = null;
                        String groupTerm = inputParent.getText();
                        Collection selectedCollection = (Collection)((DefaultComboBoxModel)comboCollections.getModel()).getSelectedItem();
                        dialog = new GroupSelectionDialog(null, selectedCollection, groupTerm);

                        if(dialog.open()==GroupSelectionDialog.DIALOG_OK){
                            parent = dialog.getSelectedGroup();
                            inputParent.setText(parent.getName());
                            inputFolder.setText(parent.getFolderPath());
                            comboCollections.setSelectedItem(parent.getCollection());
                        }
                    }
                    catch(Exception e){
                            log.error("Error al obrir finestra sel.lecció grup: " + e.getMessage(), e);
                    }
                }
                else if(parent!=null){
                        parent = null;
                }
            }
        });
        inputParent.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
            	try{
					GroupSelectionDialog dialog = null;
					String groupTerm = inputParent.getText();
					Collection selectedCollection = (Collection)((DefaultComboBoxModel)comboCollections.getModel()).getSelectedItem();
					dialog = new GroupSelectionDialog(null, selectedCollection, groupTerm);

					if(dialog.open()==GroupSelectionDialog.DIALOG_OK){
						parent = dialog.getSelectedGroup();
						inputParent.setText(parent.getName());
						inputFolder.setText(parent.getFolderPath());
						comboCollections.setSelectedItem(parent.getCollection());
					}

					// Es valida la plana
	                if(firePageValidation){
						isEdited = true;
						wizard.validateCurrentPage();
					}
				}
				catch(Exception e){
					log.error("Error al obrir finestra sel.lecció grup: " + e.getMessage(), e);
				}
            }
        });
        JLabel lFolder = new JLabel("Directori", JLabel.RIGHT);
        inputFolder = new AdvancedTextField("data/icons/file_open_16.png");
        // Es deshabilita l'edició manual i es força obrir l'explorador
        inputFolder.setEditable(false);
        inputFolder.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                JFileChooser fileChooser = new JFileChooser(inputFolder.getText());
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int retValue = fileChooser.showOpenDialog(null);
                if(retValue==JFileChooser.APPROVE_OPTION){
                    inputFolder.setText(fileChooser.getSelectedFile().getAbsolutePath());
                }

                // Es valida la plana
                if(firePageValidation){
					isEdited = true;
					wizard.validateCurrentPage();
				}
            }
        });
        JLabel lPattern = new JLabel("Patró", JLabel.RIGHT);
        inputPattern = new JTextField();
        inputPattern.addKeyListener(new KeyAdapter()
        {
        	@Override
			public void keyReleased(KeyEvent event)
			{
        		// S'indica que el patró s'ha editat i no es modificarà en canviar el camp NOM
        		isPatternEdited = true;

        		if(firePageValidation){
					isEdited = true;
					wizard.validateCurrentPage();
				}
			}
        });
        JLabel legend = new JLabel("[W]: Width; [H]: Height; #: Contador");

        GroupLayout layout = new GroupLayout(pContent);
        pContent.setLayout(layout);
        /* Grup horitzontal */
        layout.setHorizontalGroup(
        	layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                    .addComponent(lPattern, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
                    .addComponent(lFolder, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
                    .addComponent(lParent, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
                    .addComponent(lCollectionTitle, GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                    .addComponent(lName, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(comboCollections, 0, 286, Short.MAX_VALUE)
                    .addComponent(inputName, GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addComponent(inputFolder, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                        .addComponent(inputParent, GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                    )
                    .addComponent(inputPattern, GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                    .addComponent(legend, GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                )
                .addContainerGap()
            )
        );

        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lName)
                    .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lCollectionTitle)
                    .addComponent(comboCollections, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                    .addComponent(lParent)
                    .addComponent(inputParent, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                    .addComponent(lFolder)
                    .addComponent(inputFolder, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lPattern)
                    .addComponent(inputPattern, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(legend)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            )
        );

		return pContent;
	}

	@Override
	public boolean validatePage()
	{
		if(StringUtils.isBlank(inputName.getText())){
			if(isEdited){
				wizard.showErrorMessage("S'ha d'especificar el nom del grup");
			}
			return false;
		}
		Collection selectedCollection = (Collection)((DefaultComboBoxModel)comboCollections.getModel()).getSelectedItem();
		if(selectedCollection==null){
			if(isEdited){
				wizard.showErrorMessage("S'ha d'especificar la col.lecció del grup");
			}
			return false;
		}
		if(StringUtils.isBlank(inputPattern.getText())){
			if(isEdited){
				wizard.showErrorMessage("S'ha d'especificar el patró del grup");
			}

			return false;
		}
		if(StringUtils.isBlank(inputFolder.getText())){
			if(isEdited){
				wizard.showErrorMessage("S'ha d'especificar el directori del grup");
			}

			return false;
		}

		return true;
	}

	public PictureGroup getFormData()
	{
		if(formData==null){
			formData = new PictureGroup();
			formData.setCreationDate(new Date());
		}
		formData.setName(inputName.getText());
		Collection selectedCollection = (Collection)((DefaultComboBoxModel)comboCollections.getModel()).getSelectedItem();
		formData.setCollection(selectedCollection);
		formData.setParent(parent);
		formData.setFolderPath(inputFolder.getText());
		formData.setPattern(inputPattern.getText());

		return formData;
	}

	public void setFormData(Collection collection) throws BusinessException
	{
		// Per tal que no llenci els events de canvi dels camps i activi les validacions
		firePageValidation = false;
		DefaultComboBoxModel comboModel = (DefaultComboBoxModel)comboCollections.getModel();
		// Es neteja el combo per refrescar els continguts
		comboModel.removeAllElements();
		// Opció en blanc
		comboModel.addElement(null);
		// Col.leccions
		List<Collection> vCollections = wizard.getAllCollections();
		Collection selectedCollection = null;
		for(Collection elem:vCollections){
			comboModel.addElement(elem);
			if(elem.getId().equals(collection.getId())){
				selectedCollection = elem;
			}
		}
		comboModel.setSelectedItem(selectedCollection);
		// Es torna a activar les validacions
        firePageValidation = true;
	}

	public void setFormData(Collection collection, PictureGroup formData) throws BusinessException
	{
		// Per tal que no llenci els events de canvi dels camps i activi les validacions
		firePageValidation = false;
		this.formData = formData;
		inputName.setText(formData.getName());
		inputFolder.setText(formData.getFolderPath());
		inputPattern.setText(formData.getPattern());

		if(formData.getParent()!=null){
			parent = formData.getParent();
			inputParent.setText(parent.getName());
			collection = parent.getCollection();
		}

		setFormData(collection);
		// Es torna a activar les validacions
        firePageValidation = true;
	}
}
