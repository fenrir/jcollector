package org.fenrir.jcollector.ui.wizard;

import java.util.List;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.jcollector.core.service.IContentAdministrationService;
import org.fenrir.jcollector.core.service.IContentSearchService;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.event.ApplicationEvent;
import org.fenrir.jcollector.core.event.IApplicationEventListener;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.util.TreeList;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140530
 */
public class GroupManagementWizard extends AbstractWizard implements ICollectionWizardSupport
{
	public static final int MODE_CREATE = 1;
	public static final int MODE_MODIFY = 2;
	public static final int MODE_DELETE = 3;

	private final Logger log = LoggerFactory.getLogger(GroupManagementWizard.class);
	
	private int mode;

	private CollectionSelectionWizardPage<GroupManagementWizard> collectionSelectionPage;
	private GroupSelectionWizardPage groupSelectionPage;
	private GroupWizardPage groupPage;
	
	private Collection selectedCollection;
	private PictureGroup selectedGroup;			
	
	public GroupManagementWizard(int mode) throws Exception
	{				
		this(null, mode);
	}
	
	public GroupManagementWizard(PictureGroup group, int mode) throws Exception
	{
		if(mode==MODE_CREATE){
			setTitle("Crear grup");
		}
		else if(mode==MODE_MODIFY){
			setTitle("Modificar grup");
		}
		else if(mode==MODE_DELETE){
			setTitle("Eliminar grup");
		}
		else{
			throw new IllegalArgumentException("Mode " + mode + " no permès");
		}	
		
		this.mode = mode;		
		if(group!=null){
			IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
			this.selectedCollection = contentSearchService.findCollectionById(group.getCollection().getId());
			this.selectedGroup = new PictureGroup(group);
		}
		addPages();
	}
	
	protected void addPages() throws Exception
	{
		// Si ja s'ha sel.leccionat un grup no cal sel.leccionar una col.lecció
		if(selectedGroup==null){
			String collectionSelectionPageTitle = "Sel.lecció d'una col.lecció existent";
			String collectionSelectionPageDesc = "Sel.leccioni la col.lecció del grup"; 			
			collectionSelectionPage = new CollectionSelectionWizardPage<GroupManagementWizard>(collectionSelectionPageTitle, collectionSelectionPageDesc, this);
			addPage(collectionSelectionPage);
		}
		
		if((mode==MODE_MODIFY || mode==MODE_DELETE) && selectedGroup==null){
			String groupSelectionPageTitle = "Sel.lecció d'un grup existent";
			String groupSelectionPageDesc = mode==MODE_MODIFY ? "Sel.leccioni el grup a modificar" : "Sel.leccioni el grup a eliminar"; 			
			groupSelectionPage = new GroupSelectionWizardPage(groupSelectionPageTitle, groupSelectionPageDesc, this);
			addPage(groupSelectionPage);
		}
		if(mode==MODE_CREATE || mode==MODE_MODIFY){
			String groupSelectionPageTitle = mode==MODE_CREATE ? "Creació d'un nou grup" : "Modificació d'un grup existent";
			String groupSelectionPageDesc = mode==MODE_CREATE ? "Introdueixi les dades del nou grup" : "Introdueixi les modificaciones de les dades del grup";
			groupPage = new GroupWizardPage(groupSelectionPageTitle, groupSelectionPageDesc, this);
			// S'afegeix la pàgina al wizard abans de fer les comprobacions per tal que la crida a validateCurrentPage del wizard actui sobre aquesta
			addPage(groupPage);
			// En cas de modificació, si s'ha especificat el grup, es carrega
			if(selectedGroup!=null){
				groupPage.setFormData(selectedCollection, selectedGroup);
			}			
		}
		if(mode==MODE_DELETE){
			
		}
	}
	
	public boolean canPerformPrevious()
	{						
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();		
		if(GroupWizardPage.ID.equals(page.getId()) && (groupSelectionPage!=null || collectionSelectionPage!=null) 
				|| GroupSelectionWizardPage.ID.equals(page.getId())){
			return true;
		}
		
		return false;
	}
	
	public boolean canPerformNext()
	{
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();		
		if(CollectionSelectionWizardPage.ID.equals(page.getId())
				|| GroupSelectionWizardPage.ID.equals(page.getId())){
			return true;
		}
		
		return false;
	}
	
	public boolean canPerformFinalize()
	{
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();
		// Cas de la creació y la modificació de grups: Ultima pàgina = GroupWizardPage  
		if((MODE_CREATE==mode || MODE_MODIFY==mode) && GroupWizardPage.ID.equals(page.getId())){
			return true;
		}
		// Cas de l'eliminació de grups: Ultima pàgina = GroupSelectionWizardPage
		else if(MODE_DELETE==mode && GroupSelectionWizardPage.ID.equals(page.getId())){
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean performPrevious()
	{
		return true;
	}
	
	@Override
	public boolean performNext()
	{
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();	
		/* Pàgina sel.lecció de col.leccions */
		// En el cas de les creacions, es carrega s'indica la col.lecció a la pàgina d'edició de les dades del grup
		if(CollectionSelectionWizardPage.ID.equals(page.getId()) && mode==MODE_CREATE){
			try{
				groupPage.setFormData(selectedCollection);
			
				return true;
			}
			catch(BusinessException e){
				log.error("Error al d'inicializat les dades de la pàgina d'edició del grup: " + e.getMessage(), e);
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al d'inicializat les dades de la pàgina d'edició del grup: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
			
			return false;
		}
		// En el cas que sigui una modificació o eliminació es carreguen les grups depenent de la col.lecció triada
		if(CollectionSelectionWizardPage.ID.equals(page.getId())){
			groupSelectionPage.loadFormData(collectPictureGroups(selectedCollection));
			
			return true;
		}
		/* Pàgina sel.lecció de grups */
		// En el cas que sigui una modificació es carreguen les dades del grup escollit
		else if(GroupSelectionWizardPage.ID.equals(page.getId())){
			try{
				groupPage.setFormData(selectedCollection, selectedGroup);
			
				return true;
			}
			catch(BusinessException e){
				log.error("Error al d'inicializat les dades de la pàgina d'edició del grup: " + e.getMessage(), e);
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al d'inicializat les dades de la pàgina d'edició del grup: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
			
			return false;
		}
		
		return false;		
	}
	
	@Override
	public boolean performFinalize()
	{
		IContentAdministrationService contentAdministrationService = (IContentAdministrationService)ApplicationContext.getInstance().getRegisteredComponent(IContentAdministrationService.class);
		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
		IEventNotificationService eventNotifier = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
		
		if(mode==MODE_CREATE){
			try{			
				PictureGroup group = groupPage.getFormData();
				group = contentAdministrationService.createPictureGroup(group.getCollection().getId(), group.getParent().getId(), group.getName(), group.getFolderPath(), group.getPattern());
				
				// Es llença l'event de grup creat					
				ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_GROUP_CREATED);
				event.setNewValue(group);
				eventNotifier.notifyEvent(IApplicationEventListener.class, event);
			}
			catch(Exception e){
	    		log.error("Error al crear un nou grup: "+e.getMessage(), e);
	    		JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al crear un nou grup: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	    		
	    		return false;
			}				
		}
		else if(mode==MODE_MODIFY){
			try{		
				
				PictureGroup group = groupPage.getFormData();
				contentAdministrationService.updatePictureGroup(selectedGroup.getId(), group.getCollection().getId(), group.getParent().getId(), group.getName(), group.getFolderPath(), group.getPattern());
				
				// Es llença l'event de col.lecció creada
				// S'ha de recuperar de nou les dades de BDD per tenir-les actualitzades.
				// TODO Treure quan s'actualitzi el servei per tal que en fer updates retorni l'objecte
				group = contentSearchService.findPictureGroupById(selectedGroup.getId());
				ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_GROUP_MODIFIED);
				event.setOldValue(selectedGroup);
				event.setNewValue(group);
				eventNotifier.notifyEvent(IApplicationEventListener.class, event);
			}
			catch(Exception e){
	    		log.error("Error al modificar un grup: "+e.getMessage(), e);
	    		JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al modificar un grup: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	    		
	    		return false;
			}	
		}		
		else if(mode==MODE_DELETE){
			
		}
		
		return true;
	}
	
	public boolean performCancel()
	{
		return true;
	}	
	
	@Override
	public List<Collection> getAllCollections() throws BusinessException
	{
		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
		return contentSearchService.findAllCollections();
	}
	
	@Override
	public void setSelectedCollection(Collection collection)
	{
		this.selectedCollection = collection;
	}

	public Collection getCollectionById(Long collection) throws BusinessException
	{
		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
		return contentSearchService.findCollectionById(collection);
	}
	
	public PictureGroup getPictureGroupById(Long group) throws BusinessException
	{
		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
		return contentSearchService.findPictureGroupById(group);
	}
	
	public void setSelectedGroup(PictureGroup group)
	{
		this.selectedGroup = group;
	}
	
	private TreeList collectPictureGroups(Collection collection)
	{
		// Es crea un arbre amb el node arrel buit ("Dummy")
		TreeList result = new TreeList(true);		
		
		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);		
		try{
			// Grups de primer nivell
			List<PictureGroup> rootGroups = contentSearchService.findCollectionPictureGroups(collection.getId());
			for(PictureGroup group:rootGroups){	
				TreeList rootNode = buildSubtree(group);
				result.getRoot().addChild(rootNode.getRoot());
			}			
		}
		catch(BusinessException e){
			log.error("Error buscant grups: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(null, "Error buscant grups: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		
		return result;
	}
	
	private TreeList buildSubtree(PictureGroup parent) throws BusinessException
	{
		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);				
		
		TreeList tree = new TreeList(parent);
		List<PictureGroup> childGroups = contentSearchService.findPictureGroupsByParent(parent.getId());
		if(!childGroups.isEmpty()){			
			for(PictureGroup group:childGroups){
				TreeList childBranch = buildSubtree(group);
				tree.getRoot().addChild(childBranch.getRoot());
			}
		}
		
		return tree;
	}
}
