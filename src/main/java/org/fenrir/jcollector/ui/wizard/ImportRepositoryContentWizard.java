package org.fenrir.jcollector.ui.wizard;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.service.IContentSearchService;
import org.fenrir.jcollector.core.event.ApplicationEvent;
import org.fenrir.jcollector.core.event.IApplicationEventListener;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.ui.worker.ImportRepositoryContentWorker;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.4.20140601
 */
public class ImportRepositoryContentWizard extends AbstractWizard implements IRepositoryWizardSupport 
{
	private final Logger log = LoggerFactory.getLogger(ImportRepositoryContentWizard.class);
	
	private RepositorySelectionWizardPage<ImportRepositoryContentWizard> repositorySelectionPage;
	private ImportRepositoryContentWizardPage importContentWizardPage;
	
	private Repository selectedRepository;
	
	public ImportRepositoryContentWizard(Repository repository) throws Exception
	{		
		setTitle("Importar contingut del repositori");		
		
		this.selectedRepository = repository;
		addPages();
	}
	
	@Override
	public String getIcon()
	{		
		return "data/icons/repository_import_48.png";		
	}
	
	protected void addPages() throws Exception
	{
		if(selectedRepository==null){
			repositorySelectionPage = new RepositorySelectionWizardPage<ImportRepositoryContentWizard>("Sel.lecció d'un repositori existent", "Sel.leccioni el repositori del que vol importar contingut", this);
			addPage(repositorySelectionPage);
		}

		importContentWizardPage = new ImportRepositoryContentWizardPage("Importar contingut", "Progrés de la importació de contingut", this);
		addPage(importContentWizardPage);
		if(selectedRepository!=null){
			ImportRepositoryContentWorker worker = new ImportRepositoryContentWorker(selectedRepository);
			worker.addProcessListener(importContentWizardPage);
			worker.execute();
		}		
	}
		
	/**
	 * En aquest wizard no tindrà sentit tirar enrere ni en el cas que s'hagi de sel.leccionar un repositori
	 * perquè una vegada acabada la importació de contingut s'haurà de tancar la finestra obligatoriament
	 * @return boolean - false
	 */
	@Override
	protected boolean canPerformPrevious() 
	{
		return false;
	}
	
	@Override
	protected boolean canPerformNext() 
	{
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();		
		if(RepositorySelectionWizardPage.ID.equals(page.getId())){
			return true;
		}
		
		return false;
	}
	
	@Override
	protected boolean canPerformFinalize() 
	{
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();		
		if(ImportRepositoryContentWizardPage.ID.equals(page.getId())){
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean performPrevious()
	{
		return true;
	}
	
	@Override
	public boolean performNext()
	{
		AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();		
		if(RepositorySelectionWizardPage.ID.equals(page.getId())){
			ImportRepositoryContentWorker worker = new ImportRepositoryContentWorker(selectedRepository);
			worker.addProcessListener(importContentWizardPage);
			worker.execute();
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * En aquest cas, al finalitzar el wizard no es realitza cap acció, llença l'event de modificació
	 * i tanca la finestra. Per aquest raó sempre es retorna true
	 * @return boolean - true
	 */
	@Override
	public boolean performFinalize()
	{
		try{
			// Es llença l'event de repositori actualitzat
			IEventNotificationService eventNotifier = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
			ApplicationEvent event = new ApplicationEvent(ApplicationEvent.EVENT_REPOSITORY_CONTENT_UPDATED);
			event.setOldValue(selectedRepository);
			eventNotifier.notifyEvent(IApplicationEventListener.class, event);
		}
		catch(Exception e){
			log.error("Error notificant event de contingut actualitzat: {}", e.getMessage(), e);
			ApplicationWindowManager.getInstance().displayErrorMessage("Error actualitzant el contingut del repositori: " + e.getMessage(), e);
		}
		
		return true;
	}
	
	/**
	 * TODO Aturar procés d'importació 
	 */
	@Override
	public boolean performCancel()
	{
		return true;
	}
	
	@Override
	public List<Repository> getAllRepositories() throws BusinessException
	{
		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
		return contentSearchService.findAllRepositories();
	}

	@Override
	public void setSelectedRepository(Repository repository)
	{
		this.selectedRepository = repository;
	}
}
