package org.fenrir.jcollector.ui.wizard;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import java.util.List;
import org.fenrir.yggdrasil.ui.event.IWorkerUpdateListener;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20110821
 */
@SuppressWarnings("serial")
public class ImportRepositoryContentWizardPage extends AbstractWizardPage<ImportRepositoryContentWizard> implements IWorkerUpdateListener<String>
{
	public static final String ID = "org.fenrir.jcollector.ui.wizard.importRepositoryContentWizardPage";

	private JProgressBar progressBar;
	private JList list;

	private boolean finished = false;
	
	public ImportRepositoryContentWizardPage(String title, String description, ImportRepositoryContentWizard wizard)throws Exception
	{
		super(title, description, wizard);		
	}
	
	public String getId()
	{
		return ID;
	}
	
	public JPanel createContents()
	{
		JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        
        progressBar = new JProgressBar(0, 100);       
        JLabel lResultats = new JLabel();
        lResultats.setText("Resultats");
        list = new JList();	
        list.setModel(new DefaultListModel());
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(list);
        
        /* Grup horitzontal */
        layout.setHorizontalGroup(
        	layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        	.addGroup(layout.createSequentialGroup()
        		.addContainerGap()
        		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        			.addComponent(progressBar, GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)        			
                    .addComponent(lResultats)
                    .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
        		)
        		.addContainerGap()
        	)
        );        
        /* Grup vertical */
        layout.setVerticalGroup(
        	layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        	.addGroup(layout.createSequentialGroup()
       			.addComponent(progressBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        		.addComponent(lResultats)
        		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        		.addComponent(scrollPane)
        		.addContainerGap()
        	)
        );        
        
        return pContents;
	}
	
	public boolean validatePage()
    {		
		return finished;
    }
	
	@Override
	public void process(List<String> chunks)
	{
		for(String str:chunks){
			((DefaultListModel)list.getModel()).addElement(str);
		}
		list.ensureIndexIsVisible(list.getModel().getSize() - 1);
	}
	
	@Override
	public void done()
	{
		finished = true;
		wizard.validateCurrentPage();
	}
}
