package org.fenrir.jcollector.ui.wizard;

import java.awt.GridLayout;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;
import org.fenrir.jcollector.core.entity.Collection;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20110803
 */
@SuppressWarnings("serial")
public class CollectionSelectionWizardPage<T extends AbstractWizard & ICollectionWizardSupport> extends AbstractWizardPage<T>
{
	public static final String ID = "org.fenrir.jcollector.ui.wizard.collectionSelectionWizardPage";			
	
	private JList collectionList; 
	
	public CollectionSelectionWizardPage(String title, String description, T wizard)throws Exception
	{
		super(title, description, wizard);	
		
		loadFormData();
	}
	
	public String getId()
	{
		return ID;
	}
	
	public JPanel createContents()
	{
		JPanel pContent = new JPanel();		
		pContent.setLayout(new GridLayout(1, 1, 10, 10));
		
		/* Definició dels components */
        JScrollPane scrollPane = new JScrollPane();        
        collectionList = new JList();
        collectionList.addListSelectionListener(new ListSelectionListener() 
        {		
			@Override
			public void valueChanged(ListSelectionEvent event) 
			{
				wizard.setSelectedCollection((Collection)collectionList.getSelectedValue());
				wizard.validateCurrentPage();					
			}
		});
        scrollPane.setViewportView(collectionList);
        pContent.add(scrollPane);
		
		return pContent;
	}
		
    private void loadFormData() throws Exception
    {
    	DefaultListModel listModel = new DefaultListModel();
    	for(Collection collection:wizard.getAllCollections()){                	
        	listModel.addElement(collection);
        }
        collectionList.setModel(listModel);
    }
    
    public boolean validatePage()
    {
    	return collectionList.getSelectedIndex() != -1;
    }        
}
