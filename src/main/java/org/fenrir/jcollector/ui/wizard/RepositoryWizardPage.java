package org.fenrir.jcollector.ui.wizard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.util.Date;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.widget.AdvancedTextField;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;
import org.fenrir.jcollector.core.entity.Repository;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.2.20130721
 */
@SuppressWarnings("serial")
public class RepositoryWizardPage extends AbstractWizardPage<RepositoryManagementWizard>
{
    public static final String ID = "org.fenrir.jcollector.ui.wizard.repositoryWizardPage";

    public final Logger log = LoggerFactory.getLogger(CollectionWizardPage.class);

    private JTextField inputName;
    private JTextField inputSource;
    private JCheckBox checkIsUrl;
    private AdvancedTextField inputFolderPath;
    private JCheckBox checkIsActive;

    private Repository originalFormData;
    private Repository newFormData;

    private boolean isEdited = false;
    private boolean firePageValidation = true;

    public RepositoryWizardPage(String title, String description, RepositoryManagementWizard wizard)throws Exception
    {
        super(title, description, wizard);
    }

    @Override
    public String getId()
    {
        return ID;
    }

    @Override
    public JPanel createContents()
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        /* Definició dels components */
        JLabel lName = new JLabel("Nom:", SwingConstants.RIGHT);
        JLabel lSource = new JLabel("Origen:", SwingConstants.RIGHT);
        JLabel lFolderPath = new JLabel("Directori:", SwingConstants.RIGHT);
        inputName = new JTextField();
        inputName.getDocument().addDocumentListener(new DocumentListener()
        {
            @Override
            public void removeUpdate(DocumentEvent event)
            {
                if(log.isDebugEnabled()){
                    log.debug("Actualitzant camp nom");
                }
                if(firePageValidation){
                    isEdited = true;
                    wizard.validateCurrentPage();
                }
            }

            @Override
            public void insertUpdate(DocumentEvent event)
            {
                if(log.isDebugEnabled()){
                    log.debug("Actualitzant camp nom");
                }
                if(firePageValidation){
                    isEdited = true;
                    wizard.validateCurrentPage();
                }
            }

            /**
             * No s'utilitza
             */
            @Override
            public void changedUpdate(DocumentEvent event)
            {

            }
        });
        inputSource = new JTextField();
        inputSource.getDocument().addDocumentListener(new DocumentListener()
        {
            @Override
            public void removeUpdate(DocumentEvent event)
            {
                if(log.isDebugEnabled()){
                    log.debug("Actualitzant camp origen");
                }
                if(firePageValidation){
                    isEdited = true;
                    wizard.validateCurrentPage();
                }
            }

            @Override
            public void insertUpdate(DocumentEvent event)
            {
                if(log.isDebugEnabled()){
                    log.debug("Actualitzant camp origen");
                }
                if(firePageValidation){
                    isEdited = true;
                    wizard.validateCurrentPage();
                }
            }

            /**
             * No s'utilitza
             */
            @Override
            public void changedUpdate(DocumentEvent event)
            {

            }
        });
        checkIsUrl = new JCheckBox("URL");
        inputFolderPath = new AdvancedTextField("data/icons/file_open_16.png");
        inputFolderPath.setEditable(false);
        inputFolderPath.addDocumentListener(new DocumentListener()
        {
            @Override
            public void removeUpdate(DocumentEvent event)
            {
                if(log.isDebugEnabled()){
                    log.debug("Actualitzant camp directori");
                }
                if(firePageValidation){
                    isEdited = true;
                    wizard.validateCurrentPage();
                }
            }

            @Override
            public void insertUpdate(DocumentEvent event)
            {
                if(log.isDebugEnabled()){
                    log.debug("Actualitzant camp directori");
                }
                if(firePageValidation){
                    isEdited = true;
                    wizard.validateCurrentPage();
                }
            }

            /**
             * No s'utilitza
             */
            @Override
            public void changedUpdate(DocumentEvent event)
            {

            }
        });
        inputFolderPath.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                JFileChooser fileChooser = new JFileChooser(inputFolderPath.getText());
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int retValue = fileChooser.showOpenDialog(null);
                if(retValue==JFileChooser.APPROVE_OPTION){
                    inputFolderPath.setText(fileChooser.getSelectedFile().getAbsolutePath());
                }
            }
        });
        checkIsActive = new JCheckBox("Actiu");

        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(lName, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lSource, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lFolderPath, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(checkIsActive)
                    .addComponent(inputName, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                    .addComponent(inputSource, GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                    .addComponent(checkIsUrl)
                    .addComponent(inputFolderPath, GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                )
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            )
        );

        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lName)
                    .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lSource)
                    .addComponent(inputSource, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(checkIsUrl)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lFolderPath)
                    .addComponent(inputFolderPath, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(checkIsActive)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            )
        );

        return pContents;
    }

    @Override
    public boolean validatePage()
    {
        String repositoryName = inputName.getText();
        boolean isActive = checkIsActive.isSelected();
        String repositorySource = inputSource.getText();
        String repositoryFolderPath = inputFolderPath.getText().trim();
        /* Comprovació dels valors introduïts */
        if(StringUtils.isEmpty(repositoryName)){
            if(isEdited){
                wizard.showErrorMessage("El camp NOM no pot ser buit");
            }
            return false;
        }
        else if(StringUtils.isEmpty(repositorySource)){
            if(isEdited){
                wizard.showErrorMessage("El camp ORIGEN no pot ser buit");
            }
            return false;
        }
        else if(isActive && StringUtils.isEmpty(repositoryFolderPath)){
            if(isEdited){
                wizard.showErrorMessage("El camp DIRECTORI no pot ser buit");
            }
            return false;
        }
        else if(!isActive && !wizard.checkRepositoryDeactivation()){
            if(isEdited){
                wizard.showErrorMessage("No es pot desactivar un repositori amb imatges sense classificar");
            }
            return false;
        }

        return true;
    }

    public Repository getFormData()
    {
        newFormData = new Repository();
        // En cas que sigui edició la id serà la mateixa que la del objecte original
        if(originalFormData!=null){
            newFormData.setId(originalFormData.getId());
        }
        newFormData.setName(inputName.getText());
        newFormData.setCreationDate(new Date());
        newFormData.setFolderPath(inputFolderPath.getText());
        newFormData.setIsUrl(checkIsUrl.isSelected());
        newFormData.setSource(inputSource.getText());
        newFormData.setIsActive(checkIsActive.isSelected());

        return newFormData;
    }

    public void setFormData(Repository formData)
    {
        this.originalFormData = formData;

        // Per tal que no llenci els events de canvi dels camps i activi les validacions
        firePageValidation = false;
        inputName.setText(formData.getName());
        inputFolderPath.setText(formData.getFolderPath());
        checkIsUrl.setSelected(formData.isUrl());
        inputSource.setText(formData.getSource());
        checkIsActive.setSelected(formData.isActive());
        // Es torna a activar les validacions
        firePageValidation = true;
    }
}
