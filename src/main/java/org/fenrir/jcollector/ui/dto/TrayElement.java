package org.fenrir.jcollector.ui.dto;

import java.awt.image.BufferedImage;
import org.fenrir.jcollector.core.entity.Picture;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20111015
 */
public class TrayElement 
{
	private Picture picture;
	private BufferedImage image;
	private boolean pinned = false;
	
	public TrayElement(Picture picture)
	{
		this.picture = picture;
	}
	
	public TrayElement(Picture picture, boolean pinned)
	{
		this(picture);
		this.pinned = pinned;
	}

	public Picture getPicture() 
	{
		return picture;
	}

	public void setPicture(Picture picture) 
	{
		this.picture = picture;
	}
	
	public BufferedImage getImage()
	{
		return image;
	}
	
	public void setImage(BufferedImage image)
	{
		this.image = image;
	}

	public boolean isPinned() 
	{
		return pinned;
	}

	public void setPinned(boolean pinned) 
	{
		this.pinned = pinned;
	}

	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((picture == null) ? 0 : picture.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(!(obj instanceof TrayElement)){
			return false;
		}
		
		TrayElement other = (TrayElement)obj;
		if(picture==null){
			if(other.picture!=null){
				return false;
			}
		} 
		else if(!picture.getId().equals(other.picture.getId())){
			return false;
		}
		
		return true;
	}
}

