package org.fenrir.jcollector.ui.dto;

import java.awt.Point;
import java.awt.image.BufferedImage;
import org.fenrir.jcollector.core.entity.PictureGroup;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20110719
 */
public class PictureGroupCell implements ICell
{
	private PictureGroup group;
	private BufferedImage image;
	private Point position;
	
	@Override
	public BufferedImage getImage()
	{
		return image;
	}
	
	@Override
	public void setImage(BufferedImage image)
	{
		this.image = image;
	}
	
	@Override
	public Point getPosition()
	{
		return position;
	}
	
	@Override
	public void setPostion(Point position)
	{
		this.position = position;
	}
	
	@Override
	public String getText()
	{
		return group.getName();
	}
	
	@Override
	public PictureGroup getElement()
	{
		return group;
	}
	
	@Override
	public void setElement(Object element)
	{
		this.group = (PictureGroup)element;
	}
	
	@Override
	public boolean isContainerCell()
	{
		return true;
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		
		int result = 1;
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}		
		if (!(obj instanceof PictureGroupCell)){
			return false;
		}
		
		PictureGroupCell other = (PictureGroupCell)obj;
		if(group==null){
			if(other.getElement()!=null){
				return false;
			}
		} 
		else if(!group.equals(other.getElement())){
			return false;
		}
		
		return true;
	}
}
