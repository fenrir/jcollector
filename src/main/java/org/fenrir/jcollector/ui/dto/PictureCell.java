package org.fenrir.jcollector.ui.dto;

import java.awt.Point;
import java.awt.image.BufferedImage;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.util.CommonUtils;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20110719
 */
public class PictureCell implements ICell
{
	private Picture picture;
	private BufferedImage image;
	private Point position;
	
	@Override
	public BufferedImage getImage()
	{
		return image;
	}
	
	@Override
	public void setImage(BufferedImage image)
	{
		this.image = image;
	}
	
	@Override
	public Point getPosition()
	{
		return position;
	}
	
	@Override
	public void setPostion(Point position)
	{
		this.position = position;
	}
	
	@Override
	public String getText()
	{
		return CommonUtils.getPictureBasename(picture);
	}
	
	@Override
	public Picture getElement()
	{
		return picture;
	}
	
	@Override
	public void setElement(Object element)
	{
		this.picture = (Picture)element;
	}
	
	@Override
	public boolean isContainerCell()
	{
		return false;
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		
		int result = 1;
		result = prime * result + ((picture == null) ? 0 : picture.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}		
		if (!(obj instanceof PictureCell)){
			return false;
		}
		
		PictureCell other = (PictureCell)obj;
		if(picture==null){
			if(other.getElement()!=null){
				return false;
			}
		} 
		else if(!picture.equals(other.getElement())){
			return false;
		}
		
		return true;
	}
}
