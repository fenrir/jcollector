package org.fenrir.jcollector.util;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import marvin.image.MarvinImage;
import marvin.io.MarvinImageIO;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.jcollector.core.CorePreferenceConstants;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.core.service.IContentSearchService;

/**
 * TODO v1.0 Javadoc
 * TODO v0.2 Gestió d'excepcions
 * @author Antonio Archilla Nava
 * @version v0.2.20130721
 */
public class CommonUtils
{
	private static final String THUMBNAILS_FOLDER = "thumbnails";
	private static final String THUMBNAILS_EXTENSION = "png";

	private static final Logger log = LoggerFactory.getLogger(CommonUtils.class);

	public static String getFileChecksum(File file) throws IOException
	{
		FileInputStream fis = new FileInputStream(file);
		try{
			// Es retorna un String de longitud 32 char. amb el checksum calculat
			String md5 = DigestUtils.md5Hex(fis);
	
			return md5;
		}
		finally{
			fis.close();
		}
	}

	public static String getPictureAbsolutePath(Picture p) throws BusinessException
	{
		String filePath;

		IContentSearchService contentSearchService = (IContentSearchService)ApplicationContext.getInstance().getRegisteredComponent(IContentSearchService.class);
		// Si encara no ha estat classificat, es recupera la ruta a partir del repositori
		if(!p.isCollected() && p.getRepository()!=null){
                    Repository repository = contentSearchService.findRepositoryById(p.getRepository().getId());
                    String repositoryPath = repository.getFolderPath();
                    filePath = repositoryPath + File.separator + p.getRelativePath();
		}
		// En cas contrari es calcula a partir de la ruta del grup al que pertany
		else{
            PictureGroup group = contentSearchService.findPictureGroupById(p.getPictureGroup().getId());
            String groupPath = group.getFolderPath();
            filePath = groupPath + File.separator + p.getRelativePath();
		}

		return filePath;
	}

	public static String getPictureBasename(Picture picture)
	{
		// S'elimina la ruta
		String basename = getPictureFilename(picture);
		// S'elimina l'extensió
		basename = StringUtils.substringBeforeLast(basename, ".");

		return basename;
	}

	public static String getPictureFilename(Picture picture)
	{
		String filename = picture.getRelativePath();
		// S'elimina la ruta si en té
		if(picture.getRelativePath().contains(File.separator)){
			filename = StringUtils.substringAfterLast(filename, File.separator);
		}

		return filename;
	}

	/**
	 * Construeix el path relatiu al workspace de la miniatura de la imatge proporcionada
	 * a partir de la seva ID
	 * @param pictureId Long - ID de la imatge
	 * @return String - Path de la miniatura relatiu al workspace
	 */
	public static String getThumbnailRelativePath(Long pictureId)
	{
        String paddedId = StringUtils.leftPad(pictureId.toString(), 2, "0");
        String folder = paddedId.substring(paddedId.length() - 2);
        String thumbnail = THUMBNAILS_FOLDER + File.separator + folder + File.separator + paddedId + "." + THUMBNAILS_EXTENSION;

        return thumbnail;
	}

	public static List<String> readStreamLines(InputStream is) throws IOException
	{
		List<String> lines = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));

		try{
			String line;
			while((line = br.readLine())!=null){
				lines.add(line);
			}
	
			return lines;
		}
		finally{
			br.close();
		}
	}

	public static BufferedImage loadImage(String filePath) throws IOException
	{		
        MarvinImage mImage = MarvinImageIO.loadImage(filePath);
        return mImage.getBufferedImage();        
	}

	public static BufferedImage loadThumbnail(Picture p) throws IOException, FileNotFoundException
	{
		IPreferenceService preferenceService = (IPreferenceService)ApplicationContext.getInstance().getRegisteredComponent(IPreferenceService.class);
		String workspaceFolder = preferenceService.getProperty(CorePreferenceConstants.WORKSPACE_CURRENT);
		String thumbnail = workspaceFolder + File.separator + CommonUtils.getThumbnailRelativePath(p.getId());
		File file = new File(thumbnail);
		if(!file.exists()){
			throw new FileNotFoundException("La miniatura " + thumbnail + " de la imatge ID=" + p.getId() + "no existeix");
		}

        MarvinImage mImage = MarvinImageIO.loadImage(file.getAbsolutePath());
        return mImage.getBufferedImage();        
	}

	public static BufferedImage createAndLoadThumbnail(Picture p)
	{
		BufferedImage image = null;
		try{
			image = loadThumbnail(p);
		}
		// Si la miniatura no existeix, s'intenta crear de nou
		catch(FileNotFoundException e){
			try{
				image = createThumbnail(p);
			}
			catch(Exception e2){
				log.error("Error al crear miniatura: {}", e2.getMessage(), e2);
				// TODO v0.1 Posar imatge d'imatge no trobada
			}
		}
		catch(Exception e){
			log.error("Error al recuperar miniatura: {}", e.getMessage(), e);
			// TODO v0.1 Posar imatge d'imatge no trobada
		}

		return image;
	}

	public static BufferedImage createThumbnail(String filepath, Long pictureId) throws IOException
	{
        MarvinImage mImage = MarvinImageIO.loadImage(filepath);
        int width = mImage.getWidth();
        int height = mImage.getHeight();
        int resizedWidth = width;
        int resizedHeight = height;
        if(width>=height && width>150){
            double aspectRatio = (double)height / (double)width;            
            resizedWidth = 150;
            resizedHeight = (int)(150D * aspectRatio);
        }
        else if(height>150){
            double aspectRatio = (double)width / (double)height;
            resizedWidth = (int)(150D * aspectRatio);
            resizedHeight = 150;
        }
        mImage.resize(resizedWidth, resizedHeight);
        
        IPreferenceService preferenceService = (IPreferenceService)ApplicationContext.getInstance().getRegisteredComponent(IPreferenceService.class);
        String workspaceFolder = preferenceService.getProperty(CorePreferenceConstants.WORKSPACE_CURRENT);
        // L'arxiu de destinació es calculará a partir de la id de la imatge
        String thumbnailPath = workspaceFolder + File.separator + CommonUtils.getThumbnailRelativePath(pictureId);
        String folderPath = FilenameUtils.getFullPathNoEndSeparator(thumbnailPath);
        if(!new File(folderPath).exists()){
        	FileUtils.forceMkdir(new File(folderPath));
        }
        MarvinImageIO.saveImage(mImage, thumbnailPath);
        
        return mImage.getBufferedImage();        
	}

	public static BufferedImage createThumbnail(Picture p) throws IOException, BusinessException
	{
		String filePath = CommonUtils.getPictureAbsolutePath(p);
		return createThumbnail(filePath, p.getId());
	}

	public static void deleteThumbnail(long pictureId)
	{
		IPreferenceService preferenceService = (IPreferenceService)ApplicationContext.getInstance().getRegisteredComponent(IPreferenceService.class);
        String workspaceFolder = preferenceService.getProperty(CorePreferenceConstants.WORKSPACE_CURRENT);
        // L'arxiu de destinació es calculará a partir de la id de la imatge
        String thumbnailPath = workspaceFolder + File.separator + CommonUtils.getThumbnailRelativePath(pictureId);
        File file = new File(thumbnailPath);
        if(file.exists()){
        	FileUtils.deleteQuietly(file);
        }
	}

	public static void cleanThumbnailsDirectory() throws IOException
	{
		IPreferenceService preferenceService = (IPreferenceService)ApplicationContext.getInstance().getRegisteredComponent(IPreferenceService.class);
        String workspaceFolder = preferenceService.getProperty(CorePreferenceConstants.WORKSPACE_CURRENT);
        String folder = workspaceFolder + File.separator + THUMBNAILS_FOLDER;
		FileUtils.cleanDirectory(new File(folder));
	}
}
