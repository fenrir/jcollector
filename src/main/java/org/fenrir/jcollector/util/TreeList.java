package org.fenrir.jcollector.util;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO v1.0 Javadoc
 * TODO Implementar iteradors
 * TODO Simplificar les operaciones de posar / treure fills
 * @author Antonio Archilla Nava
 * @version v0.1.20110819
 */
public class TreeList
{
	private TreeNode root;
		
	public TreeList()
	{
		
	}
	
	public TreeList(boolean createEmptyRootNode)
	{
		if(createEmptyRootNode){
			this.root = new TreeNode();
		}
	}
	
	public TreeList(Object rootObject)
	{
		this.root = new TreeNode(rootObject);
	}
	
	public TreeList(TreeNode root)
	{
		this.root = root;
	}
	
	public TreeNode getRoot()
	{
		return root;
	}
	
	public void setRoot(TreeNode root)
	{
		this.root = root;
	}
	
	public static class TreeNode
	{
		private Object object;
		private List<TreeNode> children = new ArrayList<TreeNode>();
		
		public TreeNode()
		{
			
		}
		
		public TreeNode(Object object)
		{
			this.object = object;
		}
		
		public Object getObject()
		{
			return object;
		}
		
		public void setObject(Object object)
		{
			this.object = object;
		}
		
		public List<TreeNode> getChildren()
		{
			return children;
		}
		
		public void addChild(TreeNode child)
		{
			this.children.add(child);
		}
	}
}
