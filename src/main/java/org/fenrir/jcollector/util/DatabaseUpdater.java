package org.fenrir.jcollector.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Date;
import liquibase.Contexts;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.jcollector.core.exception.DatabaseOperationException;

/**
 * TODO v1.0 Documentació
 * Per generar el changelog inicial de la base de dades s'ha d'executar la següent comanda:
 * {@code
 * java -jar liquibase.jar --driver=org.apache.derby.jdbc.EmbeddedDriver \
 *    --classpath=derby.jar \
 *    --changeLogFile=db.changelog.xml \
 *    --url="jdbc:derby:/path/to/database;create=true" \
 *    --username=admin \
 *    --password=admin \
 *    generateChangeLog
 * }
 * Si es vol generar un changelog que contingui només les dades emmagatzemades, afegir
 * {@code --diffTypes="data"} abans d'especificar l'operació "generateChangelog"  
 * Per generar el changelog diff s'ha d'executar la comanda:
 * {@code
 * java -jar liquibase.jar --driver=org.apache.derby.jdbc.EmbeddedDriver \ 
 *    --classpath=derby.jar \
 *    --url="jdbc:derby:/path/to/database.base;create=true" \
 *    --username=admin \
 *    --password=admin \
 *    diffChangeLog \
 *    --referenceUrl="jdbc:derby:/path/to/database.new;create=true" \
 *    --referenceUsername=admin \
 *    --referencePassword=admin
 * }
 * @author Antonio Archilla Nava
 * @version v0.4.20140611
 */
public class DatabaseUpdater 
{
	// TODO Mirar si s'ha d'extreure al service
	private static final String LIQUIBASE_DATABASE_CHANGELOG = "org/fenrir/jcollector/conf/db_changelog_v4.xml";
	
    private final Logger log = LoggerFactory.getLogger(DatabaseUpdater.class);
    
    private Connection connection;
    private Liquibase liquibase;
    private String[] contexts;
    
    public void initializeUpdater(String provider, String url, String user, String password) throws DatabaseOperationException
    {
        /* Configuració de la connexió a BDD */
        // Es carrega el driver			
        try{
            Class.forName(provider);
            connection = DriverManager.getConnection(url, user, password);
            connection.setAutoCommit(false);
            
            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
            liquibase = new Liquibase(LIQUIBASE_DATABASE_CHANGELOG, new ClassLoaderResourceAccessor(), database);
        }
        catch(ClassNotFoundException e){
            log.error("No s'ha trobat el driver de connexió a base de dades: {}", e.getMessage(), e);
            throw new DatabaseOperationException("No s'ha trobat el driver de connexió a base de dades: " + e.getMessage(), e);
        }
        catch(SQLException e){
            log.error("Error en obrir la connexió a base de dades per dur a terme l'actualització: {}", e.getMessage(), e);
            throw new DatabaseOperationException("Error en obrir la connexió a base de dades per dur a terme l'actualització: " + e.getMessage(), e);
        }
        catch(DatabaseException e){
            log.error("Error configurant la connexió a base de dades: {}", e.getMessage(), e);
            throw new DatabaseOperationException("Error configurant la connexió a base de dades: " + e.getMessage(), e);
        }
        catch(LiquibaseException e){
            log.error("Error configurant l'actualizació de l'esquema de base de dades: {}", e.getMessage(), e);
            throw new DatabaseOperationException("Error configurant l'actualizació de l'esquema de base de dades: " + e.getMessage(), e);
        }
    }
    
    public void finalizeUpdater() throws DatabaseOperationException
    {
    	
		try{
			/* S'esborra el flag que indica que s'està actualitzant la BDD. Això permet que un posterior procés
             * d'actualització pugui dur-se a terme
             */
			if(liquibase!=null){
				liquibase.forceReleaseLocks();
			}
		}
		catch(LiquibaseException e){
            log.error("Error en realitzar l'actualització de la base de dades: {}", e.getMessage(), e);
        }
		finally{
			if(connection!=null){
	        	try{
	        		connection.close();
	        	}
	        	catch(SQLException e){
	                log.error("Error en tancar la connexió a base de dades: {}", e.getMessage(), e);
	                throw new DatabaseOperationException("Error en tancar la connexió a base de dades: " + e.getMessage(), e);
	            }
	        }
		}
    }
    
    public String[] getContexts()
    {
    	return contexts;
    }
    
    public void setContexts(String... contexts)
    {
    	this.contexts = contexts;
    }
    
    public boolean canPerformUpdate() throws DatabaseOperationException
    {
    	try{
            // Es pot especificat una etiqueta a mode de guard per llançar uns determinats changesets (mirar tag changeset en la definició xml)
            Contexts objContexts = null;
            if(contexts!=null){
            	objContexts = new Contexts(contexts);
            }
            return liquibase.listUnexpectedChangeSets(objContexts).isEmpty();
        }
        catch(Exception e){
            log.error("Error realitzant la comprobació de la base de dades: {}", e.getMessage(), e);
            throw new DatabaseOperationException("Error realitzant la comprobació de la base de dades: " + e.getMessage(), e);
        }
    }
    
    public void performUpdate() throws DatabaseOperationException
    {
        try{
            // Paràmetres d'entrada
            liquibase.setChangeLogParameter("currentDate", MessageFormat.format("{0, date, yyyy-MM-dd HH:mm:ss}", new Object[]{new Date()}));
            // Es pot especificat una etiqueta a mode de guard per llançar uns determinats changesets (mirar tag changeset en la definició xml)
            Contexts objContexts = null;
            if(contexts!=null){
            	objContexts = new Contexts(contexts);
            }
            liquibase.update(objContexts);
        }
        catch(Exception e){
            log.error("Error realitzant l'actualització de la base de dades: {}", e.getMessage(), e);
            throw new DatabaseOperationException("Error realitzant l'actualització de la base de dades: " + e.getMessage(), e);
        }
    }
}
