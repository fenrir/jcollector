package org.fenrir.jcollector.util;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.fenrir.jcollector.core.entity.Picture;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20111228
 */
public class PictureNameComparator implements Comparator<Picture>
{
	@Override
	public int compare(Picture o1, Picture o2) 
	{
		String name1 = CommonUtils.getPictureBasename(o1);
		String name2 = CommonUtils.getPictureBasename(o2);
		
		Collection<Group> groupsName1 = getGroups(name1);
		Collection<Group> groupsName2 = getGroups(name2);
		
		boolean end = false;
		Iterator<Group> iterator1 = groupsName1.iterator();
		Iterator<Group> iterator2 = groupsName2.iterator();
		while(iterator1.hasNext() && iterator2.hasNext() && !end){
			Group g1 = iterator1.next();
			Group g2 = iterator2.next();
			
			if(Group.TYPE_NUMERIC.equals(g1.getType()) && Group.TYPE_STRING.equals(g2.getType())){
				return -1;				
			}
			else if(Group.TYPE_STRING.equals(g1.getType()) && Group.TYPE_NUMERIC.equals(g2.getType())){
				return 1;
			}
			else if(Group.TYPE_STRING.equals(g1.getType())){
				int result = g1.getChunk().compareToIgnoreCase(g2.getChunk());
				if(result!=0){
					return result;
				}			
			}			
			else{
				int maxLength = Math.max(g1.getChunk().length(), g2.getChunk().length());
				String chunk1 = StringUtils.leftPad(g1.getChunk(), maxLength, "0");
				String chunk2 = StringUtils.leftPad(g2.getChunk(), maxLength, "0");
				
				int result = chunk1.compareTo(chunk2);
				if(result!=0){
					return result;
				}
			}			
		}
		
		// Si els fragments anteriors són totalment iguals, es comproba quin té més grups
		return groupsName1.size() - groupsName2.size();
	}	
	
	private Collection<Group> getGroups(String str)
	{
		Set<Group> groups = new TreeSet<Group>();
	
		Matcher numberMatcher = Pattern.compile("[\\d]+").matcher(str);
		while(numberMatcher.find()){			
			String chunk = numberMatcher.group();
			int startIndex = numberMatcher.start();
			int endIndex = numberMatcher.end();	
			
			Group group = new Group(Group.TYPE_NUMERIC, chunk, startIndex, endIndex);
			groups.add(group);			
		}
		
		Matcher strMatcher = Pattern.compile("[a-zA-Z]+").matcher(str);		
		while(strMatcher.find()){
			String chunk = strMatcher.group();
			int startIndex = strMatcher.start();
			int endIndex = strMatcher.end();	
			
			Group group = new Group(Group.TYPE_STRING, chunk, startIndex, endIndex);
			groups.add(group);			
		}
		
		return groups;
	}
	
	private class Group implements Comparable<Group>
	{
		public static final String TYPE_STRING = "STRING";
		public static final String TYPE_NUMERIC = "NUMERIC";
	
		private String type;
		private String chunk;
		private int startIndex;
		private int endIndex;		
		
		public Group(String type, String chunk, int startIndex, int endIndex)
		{
			this.type = type;
			this.chunk = chunk;
			this.startIndex = startIndex;
			this.endIndex = endIndex;
		}
		
		public String getType() 
		{
			return type;
		}

		public String getChunk() 
		{
			return chunk;
		}

		public int getStartIndex() 
		{
			return startIndex;
		}

		public int getEndIndex() 
		{
			return endIndex;
		}

		@Override
		public int compareTo(Group g)
		{
			return this.startIndex - g.startIndex;
		}
	}
}
