package org.fenrir.jcollector;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140128
 */
public class ApplicationConstants 
{
	public static final String APPLICATION_PROPERTIES_FILE_PATH = "/org/fenrir/jcollector/application.properties";	
    public static final String PERSISTENCE_UNIT = "mainUnit";
}
