package org.fenrir.jcollector;

import com.google.inject.persist.PersistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.TaskProgressDialog;
import org.fenrir.yggdrasil.ui.event.IWindowListener;
import org.fenrir.yggdrasil.ui.worker.AbstractWorker;
import org.fenrir.yggdrasil.core.AbstractApplication;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.jcollector.ui.event.ApplicationWindowListener;
import org.fenrir.jcollector.core.event.IDatabaseEventListener;
import org.fenrir.jcollector.core.exception.IncompatibleDatabaseVersionException;
import org.fenrir.jcollector.core.service.IDatabaseUpdaterService;

/**
 * TODO v1.0 Javadoc
 * TODO v0.2 Gestió d'excepcions
 * @author Antonio Archilla Nava
 * @version v0.4.20140611
 */
public class Application extends AbstractApplication
{
    private Logger log = LoggerFactory.getLogger(Application.class);
        
    @Override
    public void start() throws ApplicationException
    {
    	// S'especifica l'implementació del listener de la finestra principal de l'aplicació
    	IEventNotificationService notificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	ApplicationWindowListener listener = new ApplicationWindowListener();
    	listener.setApplicationInstance(this);
    	notificationService.addListener(IWindowListener.class, listener);
    }

    @Override
    public void stop() throws ApplicationException
    {
    	
    }
 
    @Override
    public void onWorkspaceLoaded() throws ApplicationException
    {
        if(log.isDebugEnabled()){
            log.debug("Configurant contexte pel workspace per defecte");
        }
        
        final ApplicationContext applicationContext = ApplicationContext.getInstance();
        IDatabaseUpdaterService databaseUpdaterService = (IDatabaseUpdaterService)applicationContext.getRegisteredComponent(IDatabaseUpdaterService.class);
        
        // Es mira si s'ha d'actualitzar el model de la base de dades
        try{
        	databaseUpdaterService.performSchemaUpdate();
        }
        catch(IncompatibleDatabaseVersionException e){
        	log.warn("La versió de la base de dades és incompatible: {}", e.getMessage(), e);
        	return;
        }
        // S'inicia el mòdul de persistència
        PersistService persisteService = (PersistService)applicationContext.getRegisteredComponent(PersistService.class);
        persisteService.start();
        
        // Es comunica l'event de conexió amb la base de dades
        IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
        try{
            eventNotificationService.notifyNamedEvent(IDatabaseEventListener.class, IDatabaseEventListener.EVENT_DATABASE_CONNECTION_ESTABLISHED_ID);
        }
        catch(Exception e){
            log.error("Error llançant event {}: {}", new Object[]{IDatabaseEventListener.EVENT_DATABASE_CONNECTION_ESTABLISHED_ID, e.getMessage(), e});
        }                
    }
 
    @Override
    public void onBeforeWorkspaceUnload()
    {
        if(log.isDebugEnabled()){
            log.debug("Executant aturada de la persistència i l'scheduler de tasques");
        }
        
        PersistService persisteService = (PersistService)ApplicationContext.getInstance().getRegisteredComponent(PersistService.class);
        persisteService.stop();
        
        // Es comunica l'event de desconexió amb la base de dades
        IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
        try{
            eventNotificationService.notifyEvent(IDatabaseEventListener.class, IDatabaseEventListener.EVENT_DATABASE_CONNECTION_RELEASED_ID);
        }
        catch(Exception e){
            log.error("Error llançant event {}: {}", new Object[]{IDatabaseEventListener.EVENT_DATABASE_CONNECTION_RELEASED_ID, e.getMessage(), e});
        }
    }
    
    public void performDataUpdates()
    {
        final ApplicationContext applicationContext = ApplicationContext.getInstance();
        final IDatabaseUpdaterService databaseUpdaterService = (IDatabaseUpdaterService)applicationContext.getRegisteredComponent(IDatabaseUpdaterService.class);
        
        // Només s'executen les actualitzacions si és necessari
        if(databaseUpdaterService.isDataUpdateNeeded()){
        	// En el cas que la finestra pricipal estigui activa, es mostra la finestra de progrés
        	if(ApplicationWindowManager.getInstance().isMainWindowActive()){
	            AbstractWorker<Void, String> worker = new AbstractWorker<Void, String>() 
	            {
	                @Override
	                protected Void doInBackground() throws Exception 
	                {                
		                    performDataUpdatesDelegate();
		                    
	                    return null;
	                }
	            };            
	            TaskProgressDialog dialog = null;
	            try{
	                dialog = new TaskProgressDialog("Actualitzant dades", worker);
	                dialog.open();
	            }
	            catch(Exception e){
	                log.error("Error actualitzant les dades gestionades per l'aplicació: {}", e.getMessage(), e);
	                // Primes es tanca la finestra de progrés en cas que s'hagi quedat oberta
	                if(dialog!=null && dialog.isVisible()){
	                    dialog.setVisible(false);
	                    dialog.dispose();
	                }
	                ApplicationWindowManager.getInstance().displayErrorMessage("Error actualitzant les dades gestionades per l'aplicació", e);
	            }
	        }        
	    	else{
	    		try{
	        		performDataUpdatesDelegate();
	        	}
	        	catch(Exception e){
	        		log.error("Error actualitzant les dades gestionades per l'aplicació: {}", e.getMessage(), e);
	                ApplicationWindowManager.getInstance().displayErrorMessage("Error actualitzant les dades gestionades per l'aplicació", e);
	        	}
	    	}
        }
        
        // XXX Es configuren les tasques automàtiques depenents de les dades de la BDD una vegada s'han fet totes les actualitzacions de dades
    }
    
    private void performDataUpdatesDelegate()
    {
    	ApplicationContext applicationContext = ApplicationContext.getInstance();
    	IDatabaseUpdaterService databaseUpdaterService = (IDatabaseUpdaterService)applicationContext.getRegisteredComponent(IDatabaseUpdaterService.class);
    	// Actualització de la base de dades en cas que calgui actualitzar les dades emmagatzemades                
        databaseUpdaterService.performDataUpdate();
    }
}
