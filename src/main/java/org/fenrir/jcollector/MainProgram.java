package org.fenrir.jcollector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO Documentació
 * @author Antonio Archilla Nava
 * @version v0.4.20140330
 */
public class MainProgram 
{
    public static void main(String... args) throws Exception
    {                
        Logger log = LoggerFactory.getLogger(MainProgram.class);

        boolean error = false;

        Application application = new Application();
        try{
            application.initialize();
        }
        catch(ApplicationException e){
            log.error("Error fatal inicialitzant l'aplicació: {}", e.getMessage(), e);
            ApplicationWindowManager.displayStandaloneErrorMessage("Error fatal incialitzant l'aplicació: " + e.getMessage(), e);
            // No es pot fer un exit -1 perquè es tanca la finestra de l'error
            error = true;
        }

        if(!error){
	        try{
	            application.run();
	        }
	        catch(ApplicationException e){
	            log.error("Error fatal executant l'aplicació: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error fatal executant l'aplicació: " + e.getMessage(), e);
	            // No es pot fer un exit -1 perquè es tanca la finestra de l'error
	        }
        }        
    }    
}
