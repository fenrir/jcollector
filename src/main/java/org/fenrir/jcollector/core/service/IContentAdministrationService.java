package org.fenrir.jcollector.core.service;

import java.io.File;
import java.util.Date;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.entity.Tag;
import org.fenrir.jcollector.core.exception.BusinessException;

/**
 * TODO v1.0 Javadoc
 * TODO Fer que els updates retornin l'objecte amb les dades actualitzades
 * @author Antonio Archilla Nava
 * @version v0.2.20130720
 */
public interface IContentAdministrationService
{
	/*---------------------------------*
     *           Repositoris           *
     *---------------------------------*/
	public Repository createRepository(String name, String folderPath, String source, boolean isUrl, boolean isActive) throws BusinessException;
	public Repository updateRepository(Long id, String name, String folderPath, String source, boolean isUrl, boolean isActive) throws BusinessException;

	/*---------------------------------*
     *           Col.leccions          *
     *---------------------------------*/
	public Collection createCollection(String name, Date creationDate) throws BusinessException;
	public Collection updateCollection(Long id, String name, Date creationDate) throws BusinessException;

	/*---------------------------------*
     *              Grups              *
     *---------------------------------*/
	public PictureGroup createPictureGroup(Long collection, Long parent, String name, String folderPath, String pattern) throws BusinessException;
	public void updatePictureGroup(Long id, Long collection, Long parent, String name, String folderPath, String pattern) throws BusinessException;

	/*---------------------------------*
     *             Imatges             *
     *---------------------------------*/
	public Picture createPicture(Long repository, File file) throws BusinessException;
	public Picture createPicture(Long collection, Long repository, Long pictureGroup, File file, Boolean collected, Date collectedDate) throws BusinessException;
	public void updatePicture(Long id, Long collection, Long repository, Long pictureGroup,	String relativePath, Boolean collected, Date collectedDate) throws BusinessException;

	/**
	 * Actualitza el registre de la imatge a base de dades recalculant els valors de mida
	 * checksum i path relatiu al grup / repositori a partir de fitxer especificat
	 * @param id Long ID del registre
	 * @param collection Long ID de la col.lecció associada. Pot ser null
	 * @param repsoitory Long ID del repositori associat. Pot ser null
	 * @param pictureGroup Long ID del grup associat. Pot ser null
	 * @param file File Fitxer de la imatge associada al registre
	 * @param collected boolean Indica si la imatge ha estat classificada
	 * @param collectedDate Date Data de classificació de la imatge en cas d'etar-ho. Pot ser null
	 * @throws BussinnessException Si ha fallat l'actualització
	 */
	public void updatePicture(Long id,Long collection, Long repository, Long pictureGroup, File file, Boolean collected, Date collectedDate) throws BusinessException;
	public void deletePicture(Long picture) throws BusinessException;
	public void deletePicture(Long picture, boolean deleteFile) throws BusinessException;

	/*---------------------------------*
     *            Etiquetes            *
     *---------------------------------*/
	public Tag createTag(String name, Boolean preferred) throws BusinessException;
	public Tag updateTag(Long id, String name, Boolean preferred) throws BusinessException;
	public void deleteTag(Long tag) throws BusinessException;
	public void tagPicture(Long tag, Long picture) throws BusinessException;
	public Tag tagPicture(String tagName, Long picture) throws BusinessException;
	public void deletePictureTag(Long tag, Long picture) throws BusinessException;
	public void deleteAllPictureTags(Long picture) throws BusinessException;
}
