package org.fenrir.jcollector.core.service.impl;

import java.lang.reflect.Method;
import java.util.List;
import javax.inject.Inject;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.jcollector.core.CorePreferenceConstants;
import org.fenrir.jcollector.core.dao.IDatabaseUpdateDAO;
import org.fenrir.jcollector.core.entity.DatabaseUpdate;
import org.fenrir.jcollector.core.exception.DatabaseOperationException;
import org.fenrir.jcollector.core.exception.IncompatibleDatabaseVersionException;
import org.fenrir.jcollector.core.service.IDatabaseUpdaterService;
import org.fenrir.jcollector.util.DatabaseUpdater;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140611
 */
public class DatabaseUpdaterServiceImpl implements IDatabaseUpdaterService
{
	// Context d'execució de l'actualització de la BDD. Possibles valors: devel, pro
	private static final String SCHEMA_UPDATE_CONTEXT = "devel";
	
    private final Logger log = LoggerFactory.getLogger(DatabaseUpdaterServiceImpl.class);

    @Inject
    private IPreferenceService preferenceService;

    @Inject
    private IDatabaseUpdateDAO databaseUpdateDAO;
    
    private Boolean databaseSchemaCompatible;

    public void setPreferenceService(IPreferenceService preferenceService)
    {
        this.preferenceService = preferenceService;
    }

    public void setDatabaseUpdateDAO(IDatabaseUpdateDAO databaseUpdateDAO)
    {
        this.databaseUpdateDAO = databaseUpdateDAO;
    }

    @Override
    public boolean isDatabaseSchemaCompatible() throws ApplicationException
    {
    	if(databaseSchemaCompatible!=null){
    		return databaseSchemaCompatible;
    	}
    	
        String provider = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_PROVIDER);
        String url = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_URL);
        String user = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_USER);
        String password = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_PASSWORD);

        DatabaseUpdater databaseUpdater = new DatabaseUpdater();
        try{
            databaseUpdater.initializeUpdater(provider, url, user, password);
            databaseUpdater.setContexts(SCHEMA_UPDATE_CONTEXT);
            // En el cas que la base de dades sigui posterior a la versió que suporta l'aplicació, es llança un error i no es deixa continuar
            databaseSchemaCompatible = databaseUpdater.canPerformUpdate();
        }
        catch(DatabaseOperationException e){
            log.error("Error en actualitzar la base de dades: {}", e.getMessage(), e);
            throw new ApplicationException("Error comprobant la compatibilitat de la base de dades", e);
        }
        finally{
        	try{
        		databaseUpdater.finalizeUpdater();
        	}
        	catch(DatabaseOperationException e){
                log.error("Error finalitzant procés d'actualització de la base de dades: {}", e.getMessage(), e);
                throw new ApplicationException("Error finalitzant procés de comprobació de la base de dades", e);
            }
        }
        
        return databaseSchemaCompatible;
    }
    
    @Override
    public void performSchemaUpdate() throws ApplicationException, IncompatibleDatabaseVersionException
    {
        // Es mira si s'ha d'actualitzar el model de la base de dades
        String provider = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_PROVIDER);
        String url = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_URL);
        String user = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_USER);
        String password = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_PASSWORD);

        DatabaseUpdater databaseUpdater = new DatabaseUpdater();
        try{
            databaseUpdater.initializeUpdater(provider, url, user, password);
            databaseUpdater.setContexts(SCHEMA_UPDATE_CONTEXT);
            // En el cas que la base de dades sigui posterior a la versió que suporta l'aplicació, es llança un error i no es deixa continuar
            if(!databaseUpdater.canPerformUpdate()){
            	databaseSchemaCompatible = false;
            	throw new IncompatibleDatabaseVersionException("La base de dades no és compatible amb la versió actual de l'aplicació. Actualitzi a una versió posterior");
            }
            
            databaseUpdater.performUpdate();
        }
        catch(DatabaseOperationException e){
            log.error("Error en actualitzar la base de dades: {}", e.getMessage(), e);
            throw new ApplicationException("Error actualitzant la base de dades", e);
        }
        finally{
        	try{
        		databaseUpdater.finalizeUpdater();
        	}
        	catch(DatabaseOperationException e){
                log.error("Error finalitzant procés d'actualització de la base de dades: {}", e.getMessage(), e);
                throw new ApplicationException("Error finalitzant procés d'actualització de la base de dades", e);
            }
        }
    }

    @Override
    @Transactional
    public void performDataUpdate()
    {
        // Tasques d'actulització pendents
        List<DatabaseUpdate> updates = databaseUpdateDAO.findAllDatabaseUpdates();
        for(DatabaseUpdate update:updates){
            try{
                // Es crea una nova instancia de l'actualitzador
                Object updaterObject = Class.forName(update.getUpdateClass()).getConstructor().newInstance(null);
                // S'injecten els objectes gestionats per Guice
                ApplicationContext.getInstance().injectMembers(updaterObject);
                // Es crida al mètode d'actualització especificat
                Method updateMethod = updaterObject.getClass().getMethod(update.getUpdateMethod());
                updateMethod.invoke(updaterObject);

                // Si s'ha pogut executar l'actualització amb èxit s'esborra el registre de la base de dades
                databaseUpdateDAO.deleteDatabaseUpdate(update);
            }
            catch(Exception e){
                log.error("Error en el procés d'actualització {}::{}: {}", new Object[]{update.getUpdateClass(), update.getUpdateMethod(), e.getMessage(), e});
            }
        }
    }
    
    @Override
    @Transactional
    public boolean isDataUpdateNeeded()
    {
        return databaseUpdateDAO.countAllDatabaseUpdates()>0;
    }
}
