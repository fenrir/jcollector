package org.fenrir.jcollector.core.service.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import com.google.inject.persist.Transactional;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.jcollector.core.dao.ICollectionDAO;
import org.fenrir.jcollector.core.dao.IPictureDAO;
import org.fenrir.jcollector.core.dao.IPictureGroupDAO;
import org.fenrir.jcollector.core.dao.IRepositoryDAO;
import org.fenrir.jcollector.core.dao.ITagDAO;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.entity.Tag;
import org.fenrir.jcollector.core.exception.BusinessException;
import org.fenrir.jcollector.core.service.IContentAdministrationService;
import org.fenrir.jcollector.util.CommonUtils;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20140314
 */
public class ContentAdministrationServiceImpl implements IContentAdministrationService
{
    private final Logger log = LoggerFactory.getLogger(ContentAdministrationServiceImpl.class);

    @Inject
    private ICollectionDAO collectionDAO;
    @Inject
    private IRepositoryDAO repositoryDAO;
    @Inject
    private IPictureDAO pictureDAO;
    @Inject
    private IPictureGroupDAO pictureGroupDAO;
    @Inject
    private ITagDAO tagDAO;

    public void setCollectionDAO(ICollectionDAO collectionDAO)
    {
        this.collectionDAO = collectionDAO;
    }

    public void setRepositoryDAO(IRepositoryDAO repositoryDAO)
    {
        this.repositoryDAO = repositoryDAO;
    }

    public void setPictureDAO(IPictureDAO pictureDAO)
    {
        this.pictureDAO = pictureDAO;
    }

    public void setPictureGroupDAO(IPictureGroupDAO pictureGroupDAO)
    {
        this.pictureGroupDAO = pictureGroupDAO;
    }

    public void setTagDAO(ITagDAO tagDAO)
    {
        this.tagDAO = tagDAO;
    }

    /*---------------------------------*
     *           Repositoris           *
     *---------------------------------*/
    @Transactional
    @Override
    public Repository createRepository(String name, String folderPath, String source, boolean isUrl, boolean isActive)
    {
        Repository repository = new Repository();
        repository.setName(name);
        repository.setFolderPath(folderPath);
        repository.setSource(source);
        repository.setIsUrl(isUrl);
        repository.setIsActive(isActive);
        return repositoryDAO.createRepository(repository);                
    }

    @Transactional
    @Override
    public Repository updateRepository(Long id, String name, String folderPath, String source, boolean isUrl, boolean isActive)
    {
        Repository repository = new Repository();
        repository.setId(id);
        repository.setName(name);
        repository.setFolderPath(folderPath);
        repository.setSource(source);
        repository.setIsUrl(isUrl);
        repository.setIsActive(isActive);
        return repositoryDAO.updateRepository(repository);                
    }

    /*---------------------------------*
     *           Col.leccions          *
     *---------------------------------*/
    @Transactional
    @Override
    public Collection createCollection(String name, Date creationDate)
    {
        Collection collection = new Collection();
        collection.setName(name);
        collection.setCreationDate(creationDate);
        collectionDAO.createCollection(collection);
        return collection;        
    }

    @Transactional
    @Override
    public Collection updateCollection(Long id, String name, Date creationDate)
    {
        Collection collection = collectionDAO.findCollectionById(id);
        collection.setName(name);
        collection.setCreationDate(creationDate);
        return collectionDAO.updateCollection(collection);        
    }

    /*---------------------------------*
     *              Grups              *
     *---------------------------------*/
    @Transactional
	@Override
	public PictureGroup createPictureGroup(Long collection, Long parent, String name, String folderPath, String pattern) throws BusinessException
	{
		PictureGroup pictureGroup = new PictureGroup();
		Collection objCollection = collectionDAO.findCollectionById(collection);
		pictureGroup.setCollection(objCollection);
		if(parent!=null){
			PictureGroup objParent = pictureGroupDAO.findPictureGroupById(parent);
			pictureGroup.setParent(objParent);
		}
		pictureGroup.setName(name);
		pictureGroup.setFolderPath(folderPath);
		pictureGroup.setPattern(pattern);

		return pictureGroupDAO.createPictureGroup(pictureGroup);		
	}

    @Transactional
	@Override
	public void updatePictureGroup(final Long id, final Long collection, final Long parent, final String name, final String folderPath, final String pattern) throws BusinessException
	{
    	PictureGroup group = pictureGroupDAO.findPictureGroupById(id);
		group.setName(name);
		group.setFolderPath(folderPath);
		Collection objCollection = collectionDAO.findCollectionById(collection);
		group.setCollection(objCollection);
		if(parent!=null){
			PictureGroup objParent = pictureGroupDAO.findPictureGroupById(parent);
			group.setParent(objParent);
		}
		group.setPattern(pattern);
		pictureGroupDAO.updatePictureGroup(group);
	}

	/*---------------------------------*
     *             Imatges             *
     *---------------------------------*/
	@Transactional
	@Override
	public Picture createPicture(Long repository, File file) throws BusinessException
	{
		return createPicture(null, repository, null, file, false, null);
	}

	@Transactional
	@Override
	public Picture createPicture(Long collection,
			Long repository,
			Long pictureGroup,
			File file,
			Boolean collected,
			Date collectedDate) throws BusinessException
	{
		Picture picture = new Picture();
		if(collection!=null){
			Collection objCollection = collectionDAO.findCollectionById(collection);
			picture.setCollection(objCollection);
		}
		if(repository!=null){
			Repository objRepository = repositoryDAO.findRepositoryById(repository); 
			picture.setRepository(objRepository);
		}
		if(pictureGroup!=null){
			PictureGroup objGroup = pictureGroupDAO.findPictureGroupById(pictureGroup);
			picture.setPictureGroup(objGroup);
		}
		// Es calcula el checksum MD5
		try{
			String checksum = CommonUtils.getFileChecksum(file);
			picture.setChecksum(checksum);
		}
		catch(Exception e){
			log.error("Error al calcular checksum del fitxer {}: {}", new Object[]{file.getAbsolutePath(), e.getMessage(), e});
			throw new BusinessException("Error al calcular checksum del fitxer " + file.getAbsolutePath() + ": " + e.getMessage(), e);
		}
		// Si encara no ha estat classificada, el path relatiu vindrà donat pel repositori
		if(!collected){
			Repository objRepository = repositoryDAO.findRepositoryById(repository);
			File folder = new File(objRepository.getFolderPath());
			String relativePath = StringUtils.substringAfter(file.getAbsolutePath(), folder.getAbsolutePath());
			if(relativePath.startsWith(File.separator)){
				relativePath = relativePath.substring(1);
			}
			picture.setRelativePath(relativePath);
		}
		// En cas contrari, vindrà donat pel grup
		else{
			String filename = FilenameUtils.getName(file.getAbsolutePath());
			picture.setRelativePath(filename);
		}
		picture.setCollectionDate(collectedDate);
		picture.setCollected(collected);

		try{
			BufferedImage image = CommonUtils.loadImage(file.getAbsolutePath());
			picture.setWidth(image.getWidth());
			picture.setHeight(image.getHeight());
		}
		catch(IOException e){
			log.error("Error llegint les dades de l'imatge: {}: {}", new Object[]{file.getAbsolutePath(), e.getMessage(), e});
			throw new BusinessException("Error llegint les dades de l'imatge " + file.getAbsolutePath() + ": " + e.getMessage(), e);
		}

		picture = pictureDAO.createPicture(picture);

		try{
			// Es crea la miniatura
			CommonUtils.createThumbnail(file.getAbsolutePath(), picture.getId());
		}
		// Encara que no s'hagi pogut crear la miniatura, es continua...
		catch(IOException e){
			log.error("Error al crear thumbnail {}: {}", new Object[]{file.getAbsolutePath(), e.getMessage(), e});
		}

		return picture;
	}

	@Transactional
	@Override
	public void updatePicture(Long id,
			Long collection,
			Long repository,
			Long pictureGroup,
			String relativePath,
			Boolean collected,
			Date collectedDate) throws BusinessException
	{
		Picture picture = pictureDAO.findPictureById(id);
		if(collection!=null){
			Collection objCollection = collectionDAO.findCollectionById(collection);
			picture.setCollection(objCollection);
		}
		if(repository!=null){
			Repository objRepository = repositoryDAO.findRepositoryById(repository); 
			picture.setRepository(objRepository);
		}
		if(pictureGroup!=null){
			PictureGroup objGroup = pictureGroupDAO.findPictureGroupById(pictureGroup);
			picture.setPictureGroup(objGroup);
		}
		picture.setRelativePath(relativePath);
		picture.setCollectionDate(collectedDate);
		picture.setCollected(collected);
		pictureDAO.updatePicture(picture);
	}

	@Transactional
	@Override
	public void updatePicture(Long id,
			Long collection,
			Long repository,
			Long pictureGroup,
			File file,
			Boolean collected,
			Date collectedDate) throws BusinessException
	{
		// Si encara no ha estat classificada, el path relatiu vindrà donat pel repositori
		String relativePath;
		if(!collected){
			Repository objRepository = repositoryDAO.findRepositoryById(repository);
			File folder = new File(objRepository.getFolderPath());
			relativePath = StringUtils.substringAfter(file.getAbsolutePath(), folder.getAbsolutePath());
			if(relativePath.startsWith(File.separator)){
				relativePath = relativePath.substring(1);
			}
		}
		// En cas contrari, vindrà donat pel grup
		else{
			relativePath = FilenameUtils.getName(file.getAbsolutePath());			
		}

		Picture picture = pictureDAO.findPictureById(id);
		if(collection!=null){
			Collection objCollection = collectionDAO.findCollectionById(collection);
			picture.setCollection(objCollection);
		}
		if(repository!=null){
			Repository objRepository = repositoryDAO.findRepositoryById(repository); 
			picture.setRepository(objRepository);
		}
		if(pictureGroup!=null){
			PictureGroup objGroup = pictureGroupDAO.findPictureGroupById(pictureGroup);
			picture.setPictureGroup(objGroup);
		}
		picture.setRelativePath(relativePath);
		picture.setCollectionDate(collectedDate);
		picture.setCollected(collected);
		
		// Es calcula el checksum MD5
		try{
			String checksum = CommonUtils.getFileChecksum(file);
			picture.setChecksum(checksum);
		}
		catch(Exception e){
			log.error("Error al calcular checksum del fitxer " + file.getAbsolutePath() + ": " + e.getMessage(), e);
			throw new BusinessException("Error al calcular checksum del fitxer " + file.getAbsolutePath() + ": " + e.getMessage(), e);
		}

		try{
			BufferedImage image = CommonUtils.loadImage(file.getAbsolutePath());
			picture.setWidth(image.getWidth());
			picture.setHeight(image.getHeight());
		}
		catch(IOException e){
			log.error("Error llegint les dades de l'imatge: {}: {}", new Object[]{file.getAbsolutePath(), e.getMessage(), e});
			throw new BusinessException("Error llegint les dades de l'imatge " + file.getAbsolutePath() + ": " + e.getMessage(), e);
		}

		pictureDAO.updatePicture(picture);

		try{
			// Es crea la miniatura
			CommonUtils.createThumbnail(file.getAbsolutePath(), picture.getId());
		}
		// Encara que no s'hagi pogut crear la miniatura, es continua...
		catch(IOException e){
			log.error("Error al crear thumbnail " + file.getAbsolutePath() + ": " + e.getMessage(), e);
		}
	}

	@Transactional
	@Override
	public void deletePicture(final Long picture) throws BusinessException
	{
        deletePicture(picture, false);
	}

	@Transactional
	@Override
	public void deletePicture(Long picture, boolean deleteFile) throws BusinessException
	{
		try{
			Picture objPicture = pictureDAO.findPictureById(picture);
			// S'eliminen els tags associats a la imatge
			deleteAllPictureTags(picture);
			// S'elimina la imatge
			pictureDAO.deletePicture(objPicture);
			// S'esborra el fitxer de disc
			if(deleteFile){
				String fullPath = CommonUtils.getPictureAbsolutePath(objPicture);
				File file = new File(fullPath);
				FileUtils.forceDelete(file);
			}
			// S'esborra la miniatura associada
			CommonUtils.deleteThumbnail(picture);
		}
		catch(Exception e){
			log.error("Error al esborrar imatge {}: {}", new Object[]{picture, e.getMessage(), e});
			throw new BusinessException("Error al esborrar imatge " + picture + ": " + e.getMessage(), e);
		}
	}

	/*---------------------------------*
     *            Etiquetes            *
     *---------------------------------*/
    @Transactional
    @Override
    public Tag createTag(String name, Boolean preferred)
    {
        Tag tag = new Tag();
        tag.setName(name);
        tag.setPreferred(preferred);
        return tagDAO.createTag(tag);
    }
    
    @Transactional
    @Override
    public Tag updateTag(Long id, String name, Boolean preferred)
    {
        Tag tag = tagDAO.findTagById(id);        
        tag.setName(name);
        tag.setPreferred(preferred);
        return tagDAO.updateTag(tag);
    }
    
    @Transactional
    @Override    
    public void deleteTag(Long id)
    {
        Tag tag = tagDAO.findTagById(id);
        tagDAO.deleteTag(tag);
    }

    @Transactional
    @Override
    public void tagPicture(Long tagId, Long pictureId)
    {
        Tag tag = tagDAO.findTagById(tagId);
        Picture picture = pictureDAO.findPictureById(pictureId);
        tagPicturePrivate(tag, picture);
    }
    
    @Transactional
    @Override
    public Tag tagPicture(String tagName, Long pictureId)
    {
    	List<Tag> tags = tagDAO.findTagsByName(tagName);
    	Tag tag = null;
    	if(!tags.isEmpty()){
    		tag = tags.get(0);
    		Picture picture = pictureDAO.findPictureById(pictureId);
    		tagPicturePrivate(tag, picture);
    	}
    	return tag;
    }
    
    @Transactional
    private void tagPicturePrivate(Tag tag, Picture picture)
    {
    	// Si el tag s'acaba de crear es possible que la llista d'incidències sigui null
        if(tag.getPictures()==null){
            ArrayList<Picture> pictures = new ArrayList<Picture>();
            pictures.add(picture);
            tag.setPictures(pictures);
        }
        else{
            tag.getPictures().add(picture);
        }
        tagDAO.updateTag(tag);
    }

    @Transactional
    @Override
    public void deletePictureTag(Long tagId, Long pictureId)
    {
        Tag tag = tagDAO.findTagById(tagId);      
        Picture picture = pictureDAO.findPictureById(pictureId);
        tag.getPictures().remove(picture);
        tagDAO.updateTag(tag);
    }

    @Transactional
    @Override
    public void deleteAllPictureTags(final Long pictureId)
    {
        Picture picture = pictureDAO.findPictureById(pictureId);
        picture.getTags().clear();
        pictureDAO.updatePicture(picture);
    }
}
