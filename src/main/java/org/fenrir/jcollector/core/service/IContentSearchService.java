package org.fenrir.jcollector.core.service;

import java.util.List;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.entity.Tag;
import org.fenrir.jcollector.core.exception.BusinessException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140311
 */
public interface IContentSearchService 
{
	/*---------------------------------*
     *           Col.leccions          *
     *---------------------------------*/
    public List<Collection> findAllCollections();
    public Collection findCollectionById(Long id);

    /*---------------------------------*
     *           Repositoris           *
     *---------------------------------*/
    public List<Repository> findAllRepositories();
    public List<Repository> findAllActiveRepositories();
	public List<Repository> findRepositoriesNameLike(String namePrefix) throws BusinessException;
    public Repository findRepositoryById(Long id);
	public Repository findRepositoryBySource(String source) throws BusinessException;
	
	/*---------------------------------*
     *              Grups              *
     *---------------------------------*/
	public List<PictureGroup> findAllPictureGroups() throws BusinessException;
	public List<PictureGroup> findCollectionPictureGroups(Long collection) throws BusinessException;
	public List<PictureGroup> findCollectionPictureGroups(Long collection, Long parent) throws BusinessException;
	public List<PictureGroup> findPictureGroupsNameLike(String prefix) throws BusinessException;
	public List<PictureGroup> findPictureGroupsByParent(Long parent) throws BusinessException;
	public List<PictureGroup> findPictureGroupsByFolderPath(String folderPath) throws BusinessException; 
	public PictureGroup findPictureGroupById(Long id) throws BusinessException;
	
	/*---------------------------------*
     *             Imatges             *
     *---------------------------------*/
	public List<Picture> findPicturesByGroup(Long group, boolean collected) throws BusinessException;
	public long countPicturesByGroup(Long group, boolean collected) throws BusinessException;	
	public List<Picture> findOutstandingPicturesByRepository(Long repository) throws BusinessException;
	public long countOutstandingPicturesByRepository(Long repository) throws BusinessException;
	public List<Picture> findTaggedPictures(Long tag) throws BusinessException;
	public long countTaggedPictures(Long tag) throws BusinessException;
	public Picture findPictureById(Long id) throws BusinessException;
	
	/*---------------------------------*
     *            Etiquetes            *
     *---------------------------------*/
	public List<Tag> findAllTags() throws BusinessException;
	public List<Tag> findPreferredTags() throws BusinessException;
	public List<Tag> findPreferredTagsNameLike(String tagNamePrefix) throws BusinessException;
	public List<Tag> findTagsNameLike(String namePrefix) throws BusinessException;
	public List<Tag> findAllPictureTags(Long picture) throws BusinessException;
}
