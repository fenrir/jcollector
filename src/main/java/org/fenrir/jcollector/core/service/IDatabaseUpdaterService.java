package org.fenrir.jcollector.core.service;

import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.jcollector.core.exception.IncompatibleDatabaseVersionException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140611
 */
public interface IDatabaseUpdaterService
{
	public boolean isDatabaseSchemaCompatible() throws ApplicationException;
    public void performSchemaUpdate() throws ApplicationException, IncompatibleDatabaseVersionException;
    public void performDataUpdate();
    public boolean isDataUpdateNeeded();
}
