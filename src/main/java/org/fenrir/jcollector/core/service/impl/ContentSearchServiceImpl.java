package org.fenrir.jcollector.core.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import com.google.inject.persist.Transactional;
import org.fenrir.jcollector.core.dao.ICollectionDAO;
import org.fenrir.jcollector.core.dao.IPictureDAO;
import org.fenrir.jcollector.core.dao.IPictureGroupDAO;
import org.fenrir.jcollector.core.dao.IRepositoryDAO;
import org.fenrir.jcollector.core.dao.ITagDAO;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.entity.Tag;
import org.fenrir.jcollector.core.service.IContentSearchService;
import org.fenrir.jcollector.util.PictureNameComparator;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140311
 */
public class ContentSearchServiceImpl implements IContentSearchService
{
    @Inject
    private ICollectionDAO collectionDAO;
    @Inject
    private IRepositoryDAO repositoryDAO;
    @Inject
    private IPictureDAO pictureDAO;
    @Inject
    private IPictureGroupDAO pictureGroupDAO;
    @Inject
    private ITagDAO tagDAO;
	
    public void setCollectionDAO(ICollectionDAO collectionDAO)
    {
        this.collectionDAO = collectionDAO;
    }
	
    public void setRepositoryDAO(IRepositoryDAO repositoryDAO)
    {
        this.repositoryDAO = repositoryDAO;
    }
	
    public void setPictureDAO(IPictureDAO pictureDAO)
    {
        this.pictureDAO = pictureDAO;
    }

    public void setPictureGroupDAO(IPictureGroupDAO pictureGroupDAO)
    {
        this.pictureGroupDAO = pictureGroupDAO;
    }

    public void setTagDAO(ITagDAO tagDAO)
    {
        this.tagDAO = tagDAO;
    }
	
    /*---------------------------------*
     *           Col.leccions          *
     *---------------------------------*/
    @Transactional
    @Override
    public List<Collection> findAllCollections() 
    {
        return collectionDAO.findAllCollections();		
    }

    @Transactional
    @Override
    public Collection findCollectionById(Long id)
    {
        return collectionDAO.findCollectionById(id);            
    }

    /*---------------------------------*
     *           Repositoris           *
     *---------------------------------*/
    @Transactional
    @Override        
    public List<Repository> findAllRepositories()
    {
        return repositoryDAO.findAllRepositories();            
    }
	
    @Transactional
    @Override
    public List<Repository> findAllActiveRepositories()
    {
        return repositoryDAO.findAllActiveRepositories();		
    }
	
    @Transactional
	@Override
	public List<Repository> findRepositoriesNameLike(String namePrefix)
	{
		return repositoryDAO.findRepositoriesNameLike(namePrefix);
	}
	
    @Transactional
    @Override
    public Repository findRepositoryById(Long id)
    {
        return repositoryDAO.findRepositoryById(id);
    }
	
    @Transactional
	@Override
	public Repository findRepositoryBySource(String source)
	{
		return repositoryDAO.findRepositoryBySource(source);
	}

    /*---------------------------------*
     *              Grups              *
     *---------------------------------*/
    @Transactional
	@Override
	public List<PictureGroup> findAllPictureGroups()
	{
		return pictureGroupDAO.findAllPictureGroups();
	}

    @Transactional
	@Override
	public List<PictureGroup> findCollectionPictureGroups(Long collection)
	{
		return findCollectionPictureGroups(collection, null);
	}
	
    @Transactional
	@Override
	public List<PictureGroup> findCollectionPictureGroups(Long collection, Long parent)
	{
    	Collection objCollection = collectionDAO.findCollectionById(collection);
    	PictureGroup objParent = pictureGroupDAO.findPictureGroupById(parent);
		return pictureGroupDAO.findCollectionPictureGroups(objCollection, objParent);
	}
	
    @Transactional
	@Override
	public List<PictureGroup> findPictureGroupsNameLike(String prefix)
	{
		return pictureGroupDAO.findPictureGroupsNameLike(prefix);
	}
	
    @Transactional
	@Override
	public List<PictureGroup> findPictureGroupsByParent(Long parent)
	{
    	PictureGroup objParent = pictureGroupDAO.findPictureGroupById(parent);
		return pictureGroupDAO.findPictureGroupsByParent(objParent);
	}
	
    @Transactional
	@Override
	public List<PictureGroup> findPictureGroupsByFolderPath(String folderPath)
	{
		return pictureGroupDAO.findPictureGroupsByFolderPath(folderPath);
	}
	
    @Transactional
	@Override
	public PictureGroup findPictureGroupById(Long id)
	{
		return pictureGroupDAO.findPictureGroupById(id);
	}		
	
	/*---------------------------------*
     *             Imatges             *
     *---------------------------------*/
    @Transactional
	@Override
	public List<Picture> findOutstandingPicturesByRepository(Long repository)
	{
    	Repository objRepository = repositoryDAO.findRepositoryById(repository);
		return pictureDAO.findOutstandingPicturesByRepository(objRepository, new PictureNameComparator());
	}
	
    @Transactional
	@Override
	public long countOutstandingPicturesByRepository(Long repository)
	{
    	Repository objRepository = repositoryDAO.findRepositoryById(repository);
		return pictureDAO.countOutstandingPicturesByRepository(objRepository);
	}
	
    @Transactional
	@Override
	public List<Picture> findPicturesByGroup(Long group, boolean collected)
	{
    	PictureGroup objGroup = pictureGroupDAO.findPictureGroupById(group);
		return pictureDAO.findPicturesByGroup(objGroup, true, new PictureNameComparator());
	}		
	
    @Transactional
	@Override
	public long countPicturesByGroup(Long group, boolean collected)
	{
    	PictureGroup objGroup = pictureGroupDAO.findPictureGroupById(group);
		return pictureDAO.countPicturesByGroup(objGroup, collected);
	}
	
    @Transactional
	@Override
	public List<Picture> findTaggedPictures(Long tag)
	{
    	Tag objTag = tagDAO.findTagById(tag);
		return pictureDAO.findTaggedPictures(objTag, new PictureNameComparator());
	}
	
    @Transactional
	@Override
	public long countTaggedPictures(Long tag)
	{
    	Tag objTag = tagDAO.findTagById(tag);
		return pictureDAO.countTaggedPictures(objTag);
	}
	
    @Transactional
	@Override
	public Picture findPictureById(Long id)
	{
		return pictureDAO.findPictureById(id);
	}
	
	/*---------------------------------*
     *            Etiquetes            *
     *---------------------------------*/
    @Transactional
	@Override
	public List<Tag> findAllTags()
	{
		return tagDAO.findAllTags();
	}
	
    @Transactional
	@Override
	public List<Tag> findPreferredTags()
	{
		return tagDAO.findPreferredTags(true);
	}
	
    @Transactional
	@Override
	public List<Tag> findPreferredTagsNameLike(String tagNamePrefix)
	{
		return tagDAO.findPreferredTagsNameLike(tagNamePrefix);
	}
	
    @Transactional
	@Override
	public List<Tag> findTagsNameLike(String namePrefix)
	{
		return tagDAO.findTagsNameLike(namePrefix);
	}		
	
    @Transactional
	@Override
	public List<Tag> findAllPictureTags(Long picture)
	{		
    	Picture objPicture = pictureDAO.findPictureById(picture);
    	return new ArrayList<Tag>(objPicture.getTags());
	}
}
