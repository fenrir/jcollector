package org.fenrir.jcollector.core.exception;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20130415
 */
@SuppressWarnings("serial")
public class DatabaseOperationException extends Exception
{
    public DatabaseOperationException()
    {
        super();
    }

    public DatabaseOperationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DatabaseOperationException(String message)
    {
        super(message);
    }

    public DatabaseOperationException(Throwable cause)
    {
        super(cause);
    }
}