package org.fenrir.jcollector.core.exception;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140311
 */
@SuppressWarnings("serial")
public class BusinessException extends Exception 
{
	public BusinessException() 
	{
		super();	
	}

	public BusinessException(String message, Throwable cause) 
	{
		super(message, cause);
	}

	public BusinessException(String message) 
	{
		super(message);	
	}

	public BusinessException(Throwable cause) 
	{
		super(cause);	
	}	
}
