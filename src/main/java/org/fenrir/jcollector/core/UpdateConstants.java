package org.fenrir.jcollector.core;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.2.20130415
 */
public class UpdateConstants 
{
    public static final String UPDATE_FOLDER = "update";
    public static final int DOWNLOAD_BUFFER_SIZE = 2048; 
    public static final String DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13";
    
    public static final String FILE_UPDATE = "update.xml";
    public static final String FILE_UPDATE_LOCATION = UPDATE_FOLDER + "/" + FILE_UPDATE;
    public static final String FILE_ARTIFACTS = "artifacts.xml";
    public static final String FILE_ARTIFACTS_LOCATION = UPDATE_FOLDER + "/" + FILE_ARTIFACTS;
    public static final String FILE_ACTIONS = "actions.xml";
    public static final String FILE_ACTIONS_LOCATION = UPDATE_FOLDER + "/" + FILE_ACTIONS;
    public static final String FILE_APPLICATION = "/org/fenrir/jcollector/update/application.xml";
    
    /* Expressions XPath */
    public static final String APPLICATION_VERSION = "/application/@version";
    public static final String APPLICATION_ARTIFACT_IDS = "//artifacts/artifact/@id";
    public static final String APPLICATION_ARTIFACT_VERSION = "//artifacts/artifact[@id=''{0}'']/version";
    public static final String APPLICATION_ARTIFACT_PATH = "//artifacts/artifact[@id=''{0}'']/path";
    public static final String APPLICATION_ARTIFACT_URL = "//artifacts/artifact[@id=''{0}'']/url";

    public static final String UPDATE_LAST_VERSION = "//update[last()]/@version";
    public static final String UPDATE_MIN_VERSION = "//update[@version=''{0}'']/minVersion";
    public static final String UPDATE_DIST_URL = "//update[@version=''{0}'']/dist/url";
    public static final String UPDATE_ARTIFACTS_URL = "//update[@version=''{0}'']/artifacts/url";
    
    public static final String UPDATE_ARTIFACT_IDS = "//artifact/@id";
    public static final String UPDATE_ARTIFACT_VERSION = "//artifact[@id=''{0}'']/version";
    public static final String UPDATE_ARTIFACT_PATH = "//artifact[@id=''{0}'']/path";
    public static final String UPDATE_ARTIFACT_URL = "//artifact[@id=''{0}'']/url";
}
