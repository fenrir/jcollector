package org.fenrir.jcollector.core.entity;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.4.20140601
 */
@TableGenerator(
    name="PICTURE_ID_GENERATOR", 
    table="SEQUENCES", 
    pkColumnName="SEQUENCE_NAME", 
    valueColumnName="NEXT_VAL", 
    pkColumnValue="org.fenrir.jcollector.core.entity.Picture", 
    allocationSize=1)
@Entity
public class Picture implements Serializable
{
	private static final long serialVersionUID = 956641541199905629L;

	@Id
    @GeneratedValue(strategy= GenerationType.TABLE, generator="PICTURE_ID_GENERATOR")
	private Long id;
    
    @Version
    protected Long version;
    
    private String relativePath;	
    @ManyToOne
    private Collection collection;
    @ManyToOne
    private PictureGroup pictureGroup;
    @ManyToOne
    private Repository repository;		
    private Integer width;
    private Integer height;
    @Temporal(TemporalType.TIMESTAMP)
    private Date collectionDate;
    @Column(columnDefinition="Boolean default false")
    private Boolean collected;
    private String checksum;
    /**
     * TODO Mirar si hi ha forma de crear un proxy per obtenir la col.lecció a fi de que sigui LAZY
     * Les col.leccions s'han de posar en mode fetch=EAGER perquè es pot donar el cas que 
     * el get es faci en un entorn no transaccional, el que produeix un error.
     */
    @ManyToMany(mappedBy="pictures", fetch= FetchType.EAGER)
    protected Set<Tag> tags;
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;    
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    public Picture()
    {
    	
    }
    
    public Picture(Picture picture)
    {
    	this.setId(picture.getId());
    	this.setRelativePath(picture.getRelativePath());
    	this.setCollection(picture.getCollection());
    	this.setPictureGroup(picture.getPictureGroup());
    	this.setRepository(picture.getRepository());
    	this.setWidth(picture.getWidth());
    	this.setHeight(picture.getHeight());
    	this.setCollectionDate(picture.getCollectionDate());
    	this.setCollected(picture.isCollected());
    	this.setChecksum(picture.getChecksum());
        this.setTags(picture.getTags());
        this.setCreationDate(picture.getCreationDate());
        this.setLastUpdated(picture.getLastUpdated());
    }
    
    public Long getId() 
    {
        return id;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getVersion()
    {
        return version;
    }
    
    public void setVersion(Long version)
    {
        this.version = version;
    }
    
    public String getRelativePath() 
    {
        return relativePath;
    }
	
    public void setRelativePath(String relativePath) 
    {
        this.relativePath = relativePath;
    }
	
    public Collection getCollection() 
    {
        return collection;
    }
	
    public void setCollection(Collection collection) 
    {
        this.collection = collection;
    }
	
    public PictureGroup getPictureGroup() 
    {
        return pictureGroup;
    }
	
    public void setPictureGroup(PictureGroup pictureGroup) 
    {
        this.pictureGroup = pictureGroup;
    }
	
    public Repository getRepository() 
    {
        return repository;
    }
	
    public void setRepository(Repository repository) 
    {
        this.repository = repository;
    }
	
    public Integer getWidth() 
    {
        return width;
    }
	
    public void setWidth(Integer width) 
    {
        this.width = width;
    }
	
    public Integer getHeight() 
    {
        return height;
    }
	
    public void setHeight(Integer height) 
    {
        this.height = height;
    }
	
    public Date getCollectionDate() 
    {
        return collectionDate;
    }

    public void setCollectionDate(Date collectionDate) 
    {
        this.collectionDate = collectionDate;
    }
	
    public Boolean isCollected()
    {
        return collected;
    }
	
    public void setCollected(Boolean collected)
    {
        this.collected = collected;
    }
	
    public String getChecksum() 
    {
        return checksum;
    }
	
    public void setChecksum(String checksum) 
    {
        this.checksum = checksum;
    }

    public Set<Tag> getTags()
    {
    	if(tags==null){
    		return Collections.emptySet();
    	}
        return tags;
    }
    
    public void setTags(Set<Tag> tags)
    {
        this.tags = tags;
    }
    
    public Date getCreationDate() 
    {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) 
    {
        this.creationDate = new Date(creationDate.getTime());
    }

    public Date getLastUpdated() 
    {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) 
    {
        this.lastUpdated = new Date(lastUpdated.getTime());
    }

    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues() 
    {
        if(creationDate==null){
            creationDate = new Date();
        }
        lastUpdated = new Date();
    }

    @Override
    public int hashCode() 
    {
        final int prime = 31;

        int result = 1;
        result = prime * result + ((collection == null) ? 0 : collection.hashCode());
        result = prime * result + ((repository == null) ? 0 : repository.hashCode());
        result = prime * result + ((pictureGroup == null) ? 0 : pictureGroup.hashCode());
        result = prime * result + ((relativePath == null) ? 0 : relativePath.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(!(obj instanceof Picture)){
            return false;
        }

        Picture other = (Picture) obj;
        /* Relative path */
        if(relativePath==null){
            if(other.relativePath!=null){
                return false;
            }
        } 
        else if(!relativePath.equals(other.relativePath)){
            return false;
        }
        /* Collection */
        if(collection==null){
            if(other.collection!=null){
                return false;
            }
        } 
        else if(!collection.equals(other.collection)){
            return false;
        }
        /* Repository */
        if(repository==null){
            if(other.repository!=null){
                return false;
            }
        } 
        else if(!repository.equals(other.repository)){
            return false;
        }
        /* Picture group */
        if(pictureGroup==null){
            if(other.pictureGroup!=null){
                return false;
            }
        } 
        else if(!pictureGroup.equals(other.pictureGroup)){
            return false;
        }

        return true;
    }
}
