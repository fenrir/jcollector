package org.fenrir.jcollector.core.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.4.20140311
 */
@TableGenerator(
    name="TAG_ID_GENERATOR", 
    table="SEQUENCES", 
    pkColumnName="SEQUENCE_NAME", 
    valueColumnName="NEXT_VAL", 
    pkColumnValue="org.fenrir.jcollector.core.entity.Tag", 
    allocationSize=1)
@Entity
@Table(uniqueConstraints=@UniqueConstraint(columnNames = {"name"}))
public class Tag implements Serializable
{
	private static final long serialVersionUID = -2133509064007407812L;

	@Id
    @GeneratedValue(strategy= GenerationType.TABLE, generator="TAG_ID_GENERATOR")
    private Long id;
    
    @Version
    private Long version;

    @Column(unique=true)
    private String name;
    private Boolean preferred;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "PICTURE_TAGS",
        joinColumns = @JoinColumn(name="TAG_ID", referencedColumnName="ID"),
        inverseJoinColumns = @JoinColumn(name="PICTURE_ID", referencedColumnName="ID")
    )
    private Collection<Picture> pictures;
    
    @Temporal(TemporalType.DATE)
    private Date creationDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
	
    public void setVersion(Long version)
    {
        this.version = version;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public Boolean isPreferred()
    {
        return preferred;
    }
    
    public void setPreferred(Boolean preferred)
    {
        this.preferred = preferred;
    }
    
    public Collection<Picture> getPictures()
    {
        return pictures;
    }

    public void setPictures(Collection<Picture> pictures)
    {
        this.pictures = pictures;
    }
    
    public Date getCreationDate()
    {
        return creationDate;
    }
    
    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }
    
    public Date getLastUpdated()
    {
        return lastUpdated;
    }
    
    public void setLastUpdated(Date lastUpdated)
    {
        this.lastUpdated = lastUpdated;
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();
        if(creationDate==null){
            creationDate = new Date();
        }
    }
    
    @Override
    public String toString()
    {
        return name;
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        Tag other = (Tag) obj;
        /* Name */
        if(name==null){
            if(other.name!=null){
                return false;
            }
        } 
        else if(!name.equals(other.name)){
            return false;
        }
        
        return true;
    }
}
