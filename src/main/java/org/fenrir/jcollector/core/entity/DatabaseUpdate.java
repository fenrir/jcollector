package org.fenrir.jcollector.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.4.20140311
 */
@Entity
@Table(uniqueConstraints=@UniqueConstraint(columnNames = {"updateClass", "updateMethod"}))
public class DatabaseUpdate implements Serializable
{
	private static final long serialVersionUID = 4756405562373614407L;

	@Id
    private Long id;

    @Version
    private Long version;

    private String updateClass;
    private String updateMethod;

    /* Data de l'última actualització del registre a BDD
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getVersion()
    {
        return version;
    }

    public void setVersion(Long version)
    {
        this.version = version;
    }

    public String getUpdateClass()
    {
        return updateClass;
    }

    public void setUpdateClass(String updateClass)
    {
        this.updateClass = updateClass;
    }

    public String getUpdateMethod()
    {
        return updateMethod;
    }

    public void setUpdateMethod(String updateMethod)
    {
        this.updateMethod = updateMethod;
    }

    public Date getLastUpdated()
    {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated)
    {
        this.lastUpdated = lastUpdated;
    }

    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre.
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((updateClass == null) ? 0 : updateClass.hashCode());
        result = prime * result + ((updateMethod == null) ? 0 : updateMethod.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }

        DatabaseUpdate other = (DatabaseUpdate)obj;
        /* update Class */
        if(updateClass==null){
            if(other.updateClass!=null){
                return false;
            }
        }
        else if(!updateClass.equals(other.updateClass)){
            return false;
        }
        /* update Method */
        if(updateMethod==null){
            if(other.updateMethod!=null){
                return false;
            }
        }
        else if(!updateMethod.equals(other.updateMethod)){
            return false;
        }

        return true;
    }
}
