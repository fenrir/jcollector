package org.fenrir.jcollector.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.4.20140311
 */
@TableGenerator(
    name="GROUP_ID_GENERATOR", 
    table="SEQUENCES", 
    pkColumnName="SEQUENCE_NAME", 
    valueColumnName="NEXT_VAL", 
    pkColumnValue="org.fenrir.jcollector.core.entity.PictureGroup", 
    allocationSize=1)
@Entity
public class PictureGroup implements Serializable
{
	private static final long serialVersionUID = 3782690486755923105L;

	@Id
    @GeneratedValue(strategy= GenerationType.TABLE, generator="GROUP_ID_GENERATOR")
	private Long id;
    
    @Version
    protected Long version;
    
	private String name;    
    @ManyToOne
	private Collection collection;    
    @ManyToOne
	private PictureGroup parent;    
	private String folderPath;
	private String pattern;	
    @Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
    @Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdated;
	
	public PictureGroup()
	{
		
	}
	
	public PictureGroup(PictureGroup group)
	{
		setId(group.getId());
		setName(group.getName());
		setCollection(group.getCollection());
		setParent(group.getParent());
		setFolderPath(group.getFolderPath());
		setPattern(group.getPattern());
		setCreationDate(group.getCreationDate());
		setLastUpdated(group.getLastUpdated());
	}
	
	public Long getId()
    {
    	return id;
    }
    
    public void setId(Long id)
    {
    	this.id = id;
    }

    public Long getVersion()
    {
        return version;
    }
    
    public void setVersion(Long version)
    {
        this.version = version;
    }
    
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public Collection getCollection() 
	{
		return collection;
	}
	
	public void setCollection(Collection collection) 
	{
		this.collection = collection;
	}
	
	public PictureGroup getParent() 
	{
		return parent;
	}
	
	public void setParent(PictureGroup parent) 
	{
		this.parent = parent;
	}
	
	public String getFolderPath() 
	{
		return folderPath;
	}
	
	public void setFolderPath(String folderPath) 
	{
		this.folderPath = folderPath;
	}
	
	public String getPattern() 
	{
		return pattern;
	}
	
	public void setPattern(String pattern) 
	{
		this.pattern = pattern;
	}
	
	public Date getCreationDate() 
	{
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate) 
	{
		this.creationDate = new Date(creationDate.getTime());
	}
	
	public Date getLastUpdated()
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = new Date(lastUpdated.getTime());
	}
	
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        if(creationDate==null){
            creationDate = new Date();
        }
        lastUpdated = new Date();        
    }
    
    @Override
	public String toString()
	{
		return name;
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		
		int result = 1;		
        result = prime * result	+ ((collection == null) ? 0 : collection.hashCode());
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(!(obj instanceof PictureGroup)){
			return false;
		}
		
		PictureGroup other = (PictureGroup)obj;		
		/* Collection */
		if(collection==null){
			if(other.collection!=null){
				return false;
			}
		} 
		else if(!collection.equals(other.collection)){
			return false;
		}
		/* Parent */
		if(parent==null){
			if(other.parent!=null) {
				return false;
			}
		} 
		else if(!parent.equals(other.parent)){
			return false;
		}
		
		return true;
	}
}
