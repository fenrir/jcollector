package org.fenrir.jcollector.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.4.20140311
 */
@TableGenerator(
    name="COLLECTION_ID_GENERATOR", 
    table="SEQUENCES", 
    pkColumnName="SEQUENCE_NAME", 
    valueColumnName="NEXT_VAL", 
    pkColumnValue="org.fenrir.jcollector.core.entity.Collection", 
    allocationSize=1)
@Entity
@Table(uniqueConstraints=@UniqueConstraint(columnNames = {"name"}))
public class Collection implements Serializable
{
	private static final long serialVersionUID = 5610691209365924987L;

	@Id
    @GeneratedValue(strategy= GenerationType.TABLE, generator="COLLECTION_ID_GENERATOR")
	private Long id;
    
    @Version
    protected Long version;

    // No pot existir més d'una col.lecció amb el mateix nom
    @Column(unique=true)
    private String name;
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;
    
    public Long getId()
    {
    	return id;
    }
    
    public void setId(Long id)
    {
    	this.id = id;
    }
    
    public Long getVersion()
    {
        return version;
    }
    
    public void setVersion(Long version)
    {
        this.version = version;
    }
    
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public Date getCreationDate() 
	{
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate) 
	{
		this.creationDate = creationDate;
	}
	
	public Date getLastUpdated()
	{
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}
	
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        if(creationDate==null){
            creationDate = new Date();
        }
        lastUpdated = new Date();        
    }
    
	@Override
	public String toString() 
    {
		return name;
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		
		int result = 1;		
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(!(obj instanceof Collection)){
			return false;
		}
		
		Collection other = (Collection)obj;
		/* Name */
		if(name==null){
			if(other.name!=null){
				return false;
			}
		} 
		else if(!name.equals(other.name)){
			return false;
		}		
		
		return true;
	}
}
