package org.fenrir.jcollector.core.dao;

import java.util.Comparator;
import java.util.List;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.entity.Tag;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140311
 */
public interface IPictureDAO 
{
	public List<Picture> findOutstandingPicturesByRepository(Repository repository);
	public List<Picture> findOutstandingPicturesByRepository(Repository repository, Comparator<Picture> comparator);
	public long countOutstandingPicturesByRepository(Repository repository);
	public List<Picture> findPicturesByGroup(PictureGroup group, boolean collected);
	public List<Picture> findPicturesByGroup(PictureGroup group, boolean collected, Comparator<Picture> comparator);
	public long countPicturesByGroup(PictureGroup group, boolean collected);
	public List<Picture> findTaggedPictures(Tag tag);
	public List<Picture> findTaggedPictures(Tag tag, Comparator<Picture> comparator);
	public long countTaggedPictures(Tag tag);
	public Picture findPictureById(Long id);
	public Picture findPictureByRepositoryRelativePath(Repository repository, String relativePath);
	
	public Picture createPicture(Picture picture);
	public Picture updatePicture(Picture picture);
	public void deletePicture(Picture picture);
}
