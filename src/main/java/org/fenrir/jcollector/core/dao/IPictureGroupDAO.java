package org.fenrir.jcollector.core.dao;

import java.util.List;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.PictureGroup;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140311
 */
public interface IPictureGroupDAO 
{
	public List<PictureGroup> findAllPictureGroups();
	public List<PictureGroup> findCollectionPictureGroups(Collection collection, PictureGroup parent);
	public List<PictureGroup> findPictureGroupsByParent(PictureGroup parent);
	public List<PictureGroup> findPictureGroupsNameLike(String namePrefix);
	public List<PictureGroup> findPictureGroupsByFolderPath(String folderPath);
	
	public PictureGroup findPictureGroupById(Long id);
	
	public PictureGroup createPictureGroup(PictureGroup group);
	public PictureGroup updatePictureGroup(PictureGroup group);
	public void deletePictureGroup(PictureGroup group);
}
