package org.fenrir.jcollector.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.jcollector.core.dao.ICollectionDAO;
import org.fenrir.jcollector.core.entity.Collection;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140311
 */
public class CollectionDAOImpl implements ICollectionDAO
{
	private final Logger log = LoggerFactory.getLogger(CollectionDAOImpl.class);

    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Transactional
	@Override
	public List<Collection> findAllCollections()
	{
		Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        List<Collection> results = em.createQuery("from Collection", Collection.class)
                .getResultList();
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
		return results;
	}
	
    @Transactional
	@Override
	public Collection findCollectionById(Long id)
	{
        Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        Collection result = em.find(Collection.class, id);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return result;
	}		
	
    @Transactional
	@Override
	public Collection createCollection(Collection collection)
	{
        Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        em.persist(collection);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return collection;
	}
	
    @Transactional
	@Override
	public Collection updateCollection(Collection collection)
	{
        Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        Collection updatedCollection = em.merge(collection);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }		
        return updatedCollection;
	}
	
    @Transactional
	@Override
	public void deleteCollection(Collection collection)
	{
        Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        em.remove(collection);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
	}
}
