package org.fenrir.jcollector.core.dao;

import java.util.List;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.Tag;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20131117
 */
public interface ITagDAO 
{
    public List<Tag> findAllTags();
    public List<Tag> findTagsByName(String name);
    public List<Tag> findPreferredTags(boolean preferred);
    public List<Tag> findPreferredTagsNameLike(String namePrefix);
    public List<Tag> findTagsNameLike(String namePrefix); 
    public List<Tag> findPictureTags(Picture picture); 
    public Tag findTagById(Long id);
	
    public Tag createTag(Tag tag);
    public Tag updateTag(Tag tag);
    public void deleteTag(Tag tag);    
}
