package org.fenrir.jcollector.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.jcollector.core.dao.IDatabaseUpdateDAO;
import org.fenrir.jcollector.core.entity.DatabaseUpdate;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.4.20140311
 */
public class DatabaseUpdateDAOImpl implements IDatabaseUpdateDAO
{
	private final Logger log = LoggerFactory.getLogger(DatabaseUpdateDAOImpl.class);
	
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject
    private Provider<EntityManager> entityManagerProvider;

    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }

    @Override
    @Transactional
    public List<DatabaseUpdate> findAllDatabaseUpdates()
    {
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
        List<DatabaseUpdate> records = em.createQuery("from DatabaseUpdate order by id asc", DatabaseUpdate.class).getResultList();
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return records;
    }
    
    @Override
    @Transactional
    public long countAllDatabaseUpdates()
    {
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
        Long count = em.createQuery("select count(o) from DatabaseUpdate o", Long.class).getSingleResult();
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return count;
    }

    @Override
    @Transactional
    public DatabaseUpdate findDatabaseUpdateById(Long id) 
    {
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
        DatabaseUpdate record = em.find(DatabaseUpdate.class, id);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return record;
    }

    @Override
    @Transactional
    public DatabaseUpdate createDatabaseUpdate(DatabaseUpdate databaseUpdate)
    {
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
        em.persist(databaseUpdate);

        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return databaseUpdate;
    }

    @Override
    @Transactional
    public DatabaseUpdate updateDatabaseUpdate(DatabaseUpdate databaseUpdate)
    {
    	Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        DatabaseUpdate updatedDatabaseUpdate = em.merge(databaseUpdate);

        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return updatedDatabaseUpdate;
    }

    @Override
    @Transactional
    public void deleteDatabaseUpdate(DatabaseUpdate databaseUpdate)
    {
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
        em.remove(databaseUpdate);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
    }
}
