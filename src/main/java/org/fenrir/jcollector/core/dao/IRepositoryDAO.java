package org.fenrir.jcollector.core.dao;

import java.util.List;
import org.fenrir.jcollector.core.entity.Repository;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20110428
 */
public interface IRepositoryDAO 
{
	public List<Repository> findAllRepositories();
	public List<Repository> findAllActiveRepositories();
	public List<Repository> findRepositoriesNameLike(String namePrefix);
	public Repository findRepositoryById(Long id);
	public Repository findRepositoryBySource(String source);
	
	public Repository createRepository(Repository repository);
	public Repository updateRepository(Repository repository);
	public void deleteRepository(Repository repository);
}	
