package org.fenrir.jcollector.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.jcollector.core.dao.IRepositoryDAO;
import org.fenrir.jcollector.core.entity.Repository;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140311
 */
public class RepositoryDAOImpl implements IRepositoryDAO
{
	private final Logger log = LoggerFactory.getLogger(CollectionDAOImpl.class);

    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
	
    @Transactional
	@Override
	public List<Repository> findAllRepositories()
	{
		Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        List<Repository> results = em.createQuery("from Repository", Repository.class)
                .getResultList();
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
		return results;
	}
	
    @Transactional
	@Override
	public List<Repository> findAllActiveRepositories()
	{
        Long startMseg = System.currentTimeMillis();
        
        EntityManager em = entityManagerProvider.get();
        List<Repository> results = em.createQuery("from Respository where active=:active", Repository.class)
            .setParameter("active", Boolean.TRUE)
            .getResultList();
        
		if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return results;
	}
	
    @Transactional
	@Override
	public List<Repository> findRepositoriesNameLike(String namePrefix)
	{
		Long startMseg = System.currentTimeMillis();
        
        EntityManager em = entityManagerProvider.get();
        List<Repository> results = em.createQuery("from Respository where upper(name) like :namePrefix", Repository.class)
            .setParameter("namePrefix", namePrefix.toUpperCase() + "%")
            .getResultList();
        
		if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return results;
	}

    @Transactional
	@Override
	public Repository findRepositoryById(Long id)
	{
		Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        Repository result = em.find(Repository.class, id);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return result;
	}

    @Transactional
    @Override
	public Repository findRepositoryBySource(String source)
	{
        Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        List<Repository> resultList = em.createQuery("FROM Repository WHERE source=:source", Repository.class)
                .setParameter("source", source)
                .getResultList();

        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        
        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
	}		
	
    @Transactional
	@Override
	public Repository createRepository(Repository repository)
	{
		Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        em.persist(repository);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return repository;
	}
	
    @Transactional
	@Override
	public Repository updateRepository(Repository repository)
	{
		Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        Repository updatedRepository = em.merge(repository);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }		
        return updatedRepository;
	}

    @Transactional
    @Override
	public void deleteRepository(Repository repository)
	{
		Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        em.remove(repository);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
	}
}
