package org.fenrir.jcollector.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.jcollector.core.dao.ITagDAO;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.Tag;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140311
 */
public class TagDAOImpl implements ITagDAO
{
    private final Logger log = LoggerFactory.getLogger(CollectionDAOImpl.class);
    
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }

    @Override
    @Transactional
    public List<Tag> findAllTags()
    {
        Long startMseg = System.currentTimeMillis();
        
        EntityManager em = entityManagerProvider.get();
        List<Tag> tags = em.createQuery("from Tag", Tag.class).getResultList();        
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return tags;
    }

    @Override
    @Transactional
    public List<Tag> findTagsByName(String name)
    {
        Long startMseg = System.currentTimeMillis();
        
        EntityManager em = entityManagerProvider.get();
        List<Tag> tags = em.createQuery("from Tag where upper(name)=:name", Tag.class)
                .setParameter("name", name.toUpperCase())
                .getResultList();        
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return tags;
    }
    
    @Override
    @Transactional
    public List<Tag> findPreferredTags(boolean preferred)
    {
        Long startMseg = System.currentTimeMillis();
        
        EntityManager em = entityManagerProvider.get();
        List<Tag> tags = em.createQuery("from Tag where preferred=:preferred", Tag.class)
                .setParameter("preferred", preferred)
                .getResultList();        
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return tags;
    }
    
    @Override
    @Transactional
    public List<Tag> findPreferredTagsNameLike(String namePrefix)
    {
        Long startMseg = System.currentTimeMillis();
        
        EntityManager em = entityManagerProvider.get();
        List<Tag> tags = em.createQuery("FROM Tag WHERE preferred=:preferred AND name LIKE :namePrefix", Tag.class)
                .setParameter("preferred", Boolean.TRUE)
                .setParameter("namePrefix", namePrefix + "%")
                .getResultList();
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return tags;
    }
    
    @Override
    @Transactional
    public List<Tag> findTagsNameLike(String namePrefix)
    {
        Long startMseg = System.currentTimeMillis();
        
        EntityManager em = entityManagerProvider.get();
        List<Tag> tags = em.createQuery("FROM Tag WHERE upper(name) LIKE :namePrefix", Tag.class)
                .setParameter("namePrefix", namePrefix.toUpperCase() + "%")
                .getResultList();
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return tags;
    }
    
    @Override
    @Transactional
    public List<Tag> findPictureTags(Picture picture)
    {
        Long startMseg = System.currentTimeMillis();
        
        EntityManager em = entityManagerProvider.get();
        List<Tag> tags = em.createQuery("SELECT DISTINCT t FROM Tag t JOIN t.pictures p WHERE p=:picture", Tag.class)
                .setParameter("picture", picture)
                .getResultList();       
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return tags;
    }
    
    @Override
    @Transactional
    public Tag findTagById(Long id) 
    {
        Long startMseg = System.currentTimeMillis();
        
        EntityManager em = entityManagerProvider.get();
        Tag tag = em.find(Tag.class, id);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return tag;        
    }

    @Override
    @Transactional
    public Tag createTag(Tag tag) 
    {
        Long startMseg = System.currentTimeMillis();
        
        EntityManager em = entityManagerProvider.get();
        em.persist(tag);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
		
        return tag;
    }

    @Override
    @Transactional
    public Tag updateTag(Tag tag) 
    {
        Long startMseg = System.currentTimeMillis();
        
        EntityManager em = entityManagerProvider.get();
        Tag updatedTag = em.merge(tag);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
		
        return updatedTag;
    }

    @Override
    @Transactional
    public void deleteTag(Tag tag) 
    {
        Long startMseg = System.currentTimeMillis();
        
        EntityManager em = entityManagerProvider.get();
        em.remove(tag);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
    }    
}
