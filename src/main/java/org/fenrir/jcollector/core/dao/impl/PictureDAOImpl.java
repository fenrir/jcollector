package org.fenrir.jcollector.core.dao.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.jcollector.core.dao.IPictureDAO;
import org.fenrir.jcollector.core.entity.Picture;
import org.fenrir.jcollector.core.entity.PictureGroup;
import org.fenrir.jcollector.core.entity.Repository;
import org.fenrir.jcollector.core.entity.Tag;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140311
 */
public class PictureDAOImpl implements IPictureDAO
{
	private final Logger log = LoggerFactory.getLogger(PictureDAOImpl.class);
	
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
	
    @Transactional
    @Override
    public List<Picture> findOutstandingPicturesByRepository(Repository repository)
    {
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
        List<Picture> records = em.createQuery("SELECT p FROM Picture p WHERE p.repository=:repository and (COLLECTED is null or COLLECTED=:collected)", Picture.class)
                .setParameter("repository", repository)
                .setParameter("collected", Boolean.FALSE)
                .getResultList();  
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return records;
    }
	
    @Transactional
    @Override
    public List<Picture> findOutstandingPicturesByRepository(Repository repository, Comparator<Picture> comparator)
    {		
    	Long startMseg = System.currentTimeMillis();
    	
        List<Picture> records = findOutstandingPicturesByRepository(repository);
        Collections.sort(records, comparator);

        if(log.isDebugEnabled()){
            log.debug("Resultat ordenat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return records;
    }
	
    @Transactional
    @Override
    public long countOutstandingPicturesByRepository(Repository repository)
    {
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
        Long count = em.createQuery("select count(p) FROM Picture p WHERE p.repository=:repository and (COLLECTED is null or COLLECTED=:collected)", Long.class)
                .setParameter("repository", repository)
                .setParameter("collected", Boolean.FALSE)
                .getSingleResult();
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return count;
    }
	
    @Transactional
    @Override
    public List<Picture> findPicturesByGroup(PictureGroup group, boolean collected)
    {
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
        List<Picture> records = em.createQuery("SELECT p FROM Picture p WHERE p.pictureGroup=:group and p.collected=:collected", Picture.class)
                .setParameter("group", group)
                .setParameter("collected", collected)
                .getResultList();
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return records;
    }
	
    @Transactional
    @Override
    public List<Picture> findPicturesByGroup(PictureGroup group, boolean collected, Comparator<Picture> comparator)
    {
        List<Picture> records = findPicturesByGroup(group, collected);
        Long startMseg = System.currentTimeMillis();
        Collections.sort(records, comparator);

        if(log.isDebugEnabled()){
            log.debug("Resultat ordenat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return records;
    }
	
    @Transactional
    @Override
    public long countPicturesByGroup(PictureGroup group, boolean collected)
    {
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
        Long count = em.createQuery("SELECT count(p) FROM Picture p WHERE p.pictureGroup=:group and p.collected=:collected", Long.class)
                .setParameter("group", group)
                .setParameter("collected", collected)
                .getSingleResult();
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return count;
    }
	
    @Transactional
	@Override
	public List<Picture> findTaggedPictures(Tag tag)
	{
		Long startMseg = System.currentTimeMillis();
		
		EntityManager em = entityManagerProvider.get();
        List<Picture> records = em.createQuery("select i from Picture i join i.tags t where t=:tag", Picture.class)
            .setParameter("tag", tag)
            .getResultList();

        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return records;
	}
	
	@Transactional
    @Override
    public List<Picture> findTaggedPictures(Tag tag, Comparator<Picture> comparator)
    {        
        List<Picture> records = findTaggedPictures(tag);
        Long startMseg = System.currentTimeMillis();
        Collections.sort(records, comparator);

        if(log.isDebugEnabled()){
            log.debug("Resultat ordenat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return records;
    }
	
    @Transactional
	@Override
	public long countTaggedPictures(Tag tag)
	{
    	Long startMseg = System.currentTimeMillis();
		
		EntityManager em = entityManagerProvider.get();
        Long count = em.createQuery("select count(i) from Picture i join i.tags t where t=:tag", Long.class)
            .setParameter("tag", tag)
            .getSingleResult();

        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return count;
	}
	
    @Transactional
    @Override
    public Picture findPictureById(Long id)
    {
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
        Picture picture = em.find(Picture.class, id);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return picture;
    }	
	
    @Transactional
	@Override
	public Picture findPictureByRepositoryRelativePath(Repository repository, String relativePath)
	{
    	Long startMseg = System.currentTimeMillis();
		
		EntityManager em = entityManagerProvider.get();
        List<Picture> records = em.createQuery("select i from Picture i where i.repository=:repository and i.relativePath=:relativePath", Picture.class)
            .setParameter("repository", repository)
            .setParameter("relativePath", relativePath)
            .getResultList();

        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
         */
        if(records.isEmpty()){
            return null;
        }
        else{
            return records.get(0);
        }
	}		
	
    @Transactional
    @Override
    public Picture createPicture(Picture picture)
    {
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
        em.persist(picture);
		
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return picture;
    }
	
    @Transactional
    @Override
    public Picture updatePicture(Picture picture)
    {
    	Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        Picture updatedPicture = em.merge(picture);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }		
        return updatedPicture;
    }
	
    @Transactional
    @Override
    public void deletePicture(Picture picture)
    {
    	Long startMseg = System.currentTimeMillis();
    	
    	EntityManager em = entityManagerProvider.get();
        em.remove(picture);	
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
    }
}
