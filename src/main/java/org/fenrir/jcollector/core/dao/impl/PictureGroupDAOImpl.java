package org.fenrir.jcollector.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.jcollector.core.dao.IPictureGroupDAO;
import org.fenrir.jcollector.core.entity.Collection;
import org.fenrir.jcollector.core.entity.PictureGroup;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.4.20140311
 */
public class PictureGroupDAOImpl implements IPictureGroupDAO
{
	private final Logger log = LoggerFactory.getLogger(PictureGroupDAOImpl.class);
	
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
	
    @Transactional
    @Override
	public List<PictureGroup> findAllPictureGroups()
	{
    	Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        List<PictureGroup> results = em.createQuery("from PictureGroup order by name", PictureGroup.class)
                .getResultList();
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
		return results;
	}
	
    @Transactional
	@Override
	public List<PictureGroup> findCollectionPictureGroups(Collection collection, PictureGroup parent)
	{
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
        TypedQuery<PictureGroup> query = null;
        if(parent==null){
        	query = em.createQuery("SELECT p FROM PictureGroup p WHERE p.collection=:collection and parent is null order by name", PictureGroup.class)
        			.setParameter("collection", collection);
        }
        else{
        	query = em.createQuery("SELECT p FROM PictureGroup p WHERE p.collection=:collection and p.parent=:parent order by name", PictureGroup.class)
        			.setParameter("collection", collection)
        			.setParameter("parent", parent);
        }
        List<PictureGroup> records = query.getResultList();
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return records;
	}
	
    @Transactional
	@Override
	public List<PictureGroup> findPictureGroupsByParent(PictureGroup parent)
	{
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
        List<PictureGroup> records = em.createQuery("SELECT p FROM PictureGroup p WHERE p.parent=:parent order by name", PictureGroup.class)
                .setParameter("parent", parent)
                .getResultList();
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return records;
	}
	
    @Transactional
	@Override
	public List<PictureGroup> findPictureGroupsNameLike(String namePrefix)
	{
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
		List<PictureGroup> records = em.createQuery("from PictureGroup where UPPER(name) like :name", PictureGroup.class)
	            .setParameter("name", "%" + namePrefix.toUpperCase() + "%")
	            .getResultList();
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return records;
	}
	
    @Transactional
	@Override
	public List<PictureGroup> findPictureGroupsByFolderPath(String folderPath)
	{
    	Long startMseg = System.currentTimeMillis();
    	
        EntityManager em = entityManagerProvider.get();
        List<PictureGroup> records = em.createQuery("SELECT p FROM PictureGroup p WHERE p.folderPath=:folderPath order by name", PictureGroup.class)
                .setParameter("folderPath", folderPath)
                .getResultList();
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return records;
	}
	
    @Transactional
	@Override
	public PictureGroup findPictureGroupById(Long id)
	{
    	Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
		PictureGroup result = em.find(PictureGroup.class, id);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return result;
	}
	
    @Transactional
	@Override
	public PictureGroup createPictureGroup(PictureGroup group)
	{
    	Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        em.persist(group);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
        return group;
	}
	
    @Transactional
	@Override
	public PictureGroup updatePictureGroup(PictureGroup group)
	{
    	Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
		PictureGroup updatedGroup = em.merge(group);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }		
        return updatedGroup;
	}
	
    @Transactional
	@Override
	public void deletePictureGroup(PictureGroup group)
	{
    	Long startMseg = System.currentTimeMillis();
        
		EntityManager em = entityManagerProvider.get();
        em.remove(group);
        
        if(log.isDebugEnabled()){
            log.debug("Executat en: {}ms.", (System.currentTimeMillis()-startMseg));
        }
	}
}
