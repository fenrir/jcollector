package org.fenrir.jcollector.core.dao;

import java.util.List;
import org.fenrir.jcollector.core.entity.Collection;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20130428
 */
public interface ICollectionDAO 
{
	public List<Collection> findAllCollections();
	public Collection findCollectionById(Long id);
	
	public Collection createCollection(Collection collection);
	public Collection updateCollection(Collection collection);
	public void deleteCollection(Collection collection);
}
