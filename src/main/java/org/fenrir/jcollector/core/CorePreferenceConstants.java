package org.fenrir.jcollector.core;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20130415
 */
public class CorePreferenceConstants 
{
    public static final String WORKSPACE_IS_DEFAULT = "//workspaces/workspace[contains(folder, '${folder}')]/@default";
    public static final String WORKSPACE_DEFAULT_FOLDER = "//workspaces/workspace[@default]/folder";
    public static final String WORKSPACE_RESTART_FOLDER = "//workspaces/workspace[@restart]/folder";
    public static final String WORKSPACE_CURRENT = "//transient/workspace/folder";
    public static final String PREFERENCES_IS_DEFAULT = "//preferences/@default";
    public static final String PREFERENCES_VERSION = "//preferences/@version";
    
    public static final String UPDATE_SITE = "//preferences/update/site";
	
    public static final String CONNECTION_PROVIDER = "//preferences/connection/provider";
    public static final String CONNECTION_PROVIDER_DEFAULT_VALUE = "org.apache.derby.jdbc.EmbeddedDriver";
    public static final String CONNECTION_URL = "//preferences/connection/url";
    public static final String CONNECTION_USER = "//preferences/connection/user";
    public static final String CONNECTION_PASSWORD = "//preferences/connection/password";
    
    public static final String PROXY_HOST = "//preferences/proxy/host";
    public static final String PROXY_PORT = "//preferences/proxy/port";
    public static final String HTTP_PROXY_PORT_DEFAULT = "80";
	
    public static final String THUMBS_FETCH_COUNT = "//preferences/views/thumbs-view/fetch-limit";
	
    public static final String TASKS_NAMES = "//preferences/tasks/task/@name";
    public static final String TASK_CLASS = "//preferences/tasks/task[contains(@name, '${id}')]/class";
    public static final String TASK_INITIAL_DELAY = "//preferences/tasks/task[contains(@name, '${id}')]/initialDelay";
    public static final String TASK_DELAY = "//preferences/tasks/task[contains(@name, '${id}')]/delay";
	
    public static final String EXTERNAL_PROGRAM = "//preferences/application/path";		
}
