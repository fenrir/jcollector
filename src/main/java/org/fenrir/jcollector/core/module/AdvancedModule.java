package org.fenrir.jcollector.core.module;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import org.fenrir.jcollector.core.service.IContentAdministrationService;
import org.fenrir.jcollector.core.service.IContentSearchService;
import org.fenrir.jcollector.core.service.IDatabaseUpdaterService;
import org.fenrir.jcollector.core.service.impl.ContentAdministrationServiceImpl;
import org.fenrir.jcollector.core.service.impl.ContentSearchServiceImpl;
import org.fenrir.jcollector.core.service.impl.DatabaseUpdaterServiceImpl;
import org.fenrir.jcollector.core.dao.ICollectionDAO;
import org.fenrir.jcollector.core.dao.IDatabaseUpdateDAO;
import org.fenrir.jcollector.core.dao.IPictureDAO;
import org.fenrir.jcollector.core.dao.IRepositoryDAO;
import org.fenrir.jcollector.core.dao.ITagDAO;
import org.fenrir.jcollector.core.dao.impl.CollectionDAOImpl;
import org.fenrir.jcollector.core.dao.impl.DatabaseUpdateDAOImpl;
import org.fenrir.jcollector.core.dao.impl.PictureDAOImpl;
import org.fenrir.jcollector.core.dao.impl.RepositoryDAOImpl;
import org.fenrir.jcollector.core.dao.impl.TagDAOImpl;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20130721
 */
public class AdvancedModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        /* DAOs */
        bind(IDatabaseUpdateDAO.class).to(DatabaseUpdateDAOImpl.class).in(Singleton.class);
        bind(ICollectionDAO.class).to(CollectionDAOImpl.class).in(Singleton.class);
        bind(IRepositoryDAO.class).to(RepositoryDAOImpl.class).in(Singleton.class);
        bind(IPictureDAO.class).to(PictureDAOImpl.class).in(Singleton.class);
        bind(ITagDAO.class).to(TagDAOImpl.class).in(Singleton.class);
        
        /* Services */
        bind(IDatabaseUpdaterService.class).to(DatabaseUpdaterServiceImpl.class).in(Singleton.class);
        bind(IContentSearchService.class).to(ContentSearchServiceImpl.class).in(Singleton.class);
        bind(IContentAdministrationService.class).to(ContentAdministrationServiceImpl.class).in(Singleton.class);
    }
}
