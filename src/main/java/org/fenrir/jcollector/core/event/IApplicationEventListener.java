package org.fenrir.jcollector.core.event;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.0.20101226
 */
public interface IApplicationEventListener
{
	public void applicationEventPerformed(ApplicationEvent event);
}
