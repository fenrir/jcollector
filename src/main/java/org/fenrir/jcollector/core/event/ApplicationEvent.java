package org.fenrir.jcollector.core.event;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.1.20110925
 */
public class ApplicationEvent 
{
	/* Events del workspace */
	public static final String EVENT_WORKSPACE_LOADED = "org.fenrir.jcollector.event.workspace.loaded";
	
	/* Events de col.lecció */
	public static final String EVENT_COLLECTION_CREATED = "org.fenrir.jcollector.event.collection.created";
	public static final String EVENT_COLLECTION_MODIFIED = "org.fenrir.jcollector.event.collection.modified";
	public static final String EVENT_COLLECTION_DELETED = "org.fenrir.jcollector.event.collection.deleted";		
	
	/* Events de grup */
	public static final String EVENT_GROUP_CREATED = "org.fenrir.jcollector.event.group.created";	
	public static final String EVENT_GROUP_MODIFIED = "org.fenrir.jcollector.event.group.modified";
	public static final String EVENT_GROUP_DELETED = "org.fenrir.jcollector.event.group.deleted";
	/**
	 * Event d'actualització del contingut d'un grup. Llançat quan les dades pròpies del grup no han
	 * estat modificades però si el seu contingut, com per exemple quan s'importen imatges.
	 * Com que només es necessari comunicar quin és el grup actualitzat, es fa servir el camp oldValue 
	 * de l'event, ignorant el camp newValue que serà sempre null
	 */
	public static final String EVENT_GROUP_CONTENT_UPDATED = "org.fenrir.jcollector.event.group.content.updated";
	
	/* Events de repositori */	
	public static final String EVENT_REPOSITORY_CREATED = "org.fenrir.jcollector.event.repository.created";	
	public static final String EVENT_REPOSITORY_MODIFIED = "org.fenrir.jcollector.event.repository.modified";
	public static final String EVENT_REPOSITORY_DELETED = "org.fenrir.jcollector.event.repository.deleted";
	/**
	 * Event d'actualització del contingut d'un repositori. Llançat quan les dades pròpies del repositori no han
	 * estat modificades però si el seu contingut, com per exemple quan s'importen imatges.
	 * Com que només es necessari comunicar quin és el repositori actualitzat, es fa servir el camp oldValue 
	 * de l'event, ignorant el camp newValue que serà sempre null
	 */
	public static final String EVENT_REPOSITORY_CONTENT_UPDATED = "org.fenrir.jcollector.event.repository.content.updated";
	
	/* Events d'imatge */
	public static final String EVENT_PICTURE_MODIFIED = "org.fenrir.jcollector.event.picture.mofified";
	public static final String EVENT_PICTURE_DELETED = "org.fenrir.jcollector.event.picture.deleted";
	
	/* Event de Tags */
	public static final String EVENT_TAG_CREATED = "org.fenrir.jcollector.event.tag.created";	
	public static final String EVENT_TAG_MODIFIED = "org.fenrir.jcollector.event.tag.modified";
	public static final String EVENT_TAG_DELETED = "org.fenrir.jcollector.event.tag.deleted";	
	/**
	 * Event d'actualització del contingut d'un tag. Llançat quan les dades pròpies del tag no han
	 * estat modificades però si el seu contingut, com per exemple quan s'etiqueta una imatge.
	 * Com que només es necessari comunicar quin és el tag actualitzat, es fa servir el camp oldValue 
	 * de l'event, ignorant el camp newValue que serà sempre null
	 */
	public static final String EVENT_TAG_CONTENT_UPDATED = "org.fenrir.jcollector.event.tag.content.updated";
	
	private String type;
	private Object oldValue;
	private Object newValue;
	
	public ApplicationEvent(String type)
	{
		this.type = type;
	}
	
	public String getType()
	{
		return type;
	}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public Object getOldValue()
	{
		return oldValue;
	}
	
	public void setOldValue(Object oldValue)
	{
		this.oldValue = oldValue;
	}
	
	public Object getNewValue()
	{
		return newValue;
	}
	
	public void setNewValue(Object newValue)
	{
		this.newValue = newValue;
	}
}
